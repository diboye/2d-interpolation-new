##These packages is created by Diallo Boye and Simon Connell to perform an interpolation using the thin plane splin method:
Please knowledge the authors in case you are using it outside the AS analysis.



#first step:


```
Run on the reconstructed samples and construct <mll> 1D histogram.
root -l -q -b runonsimSignalNominalPlusWeightSys_AS.C
root -l -q -b runonsimSignalKinSys_AS.C
```


#2nd step:
Do the fit by doing:

```
make
./interpolMorph
```


These will produce the txt files containing the fit parameter.



#third step:


```
make
./2D_Smooth FitParameterAvgMll_sr1_4e.txt settings.ini FitParameterAvgMll_4e.root
```


These takes as inputs the txt files conatining the fit parameter and created the root file.



#fourthtep:

```
make
./interPol
```

These uses the root files created in the third step to make the interpolation.