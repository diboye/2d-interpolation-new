
using namespace std;
TH1D* hist_avgM_nom[34][4];
TH1D* hist_avgM_kSyst[34][4];
// Globals
TObject *MyObj = 0;

//TH1F* hist_avgM_nom[34][4];
TH1D* hist_m4l_nom[34][4];
double leftRangeFit[34];
double rightRangeFit[34];
double leftRangeFitM4l[34];
double rightRangeFitM4l[34];
double leftRangeHistXM4l[34];
double rightRangeHistXM4l[34];
double leftRangeHistX[34];
double rightRangeHistX[34];
double mZd[34];
double mS[34];
double sigma_el = 0.035;
double Mean[34][4];
double Sigma[34][4];
double Integral[34][4];
double IntegralReco[34][4];
double mean_m4l[34][4];
double sigma_m4l[34][4];
double IntegralM4l[34][4];
double IntegralM4lReco[34][4];
TFile* fOut;
double error_integral[34][4];
double error_integral_m4l[34][4];

TF1 *rand_gaus_mS = new TF1("rand_gaus_mS","gaus", 120, 1500);
TF1 *rand_gaus_mZd = new TF1("rand_gaus_mZd","gaus", 0, 300);

double events_all[34];
int NumberOfFileToProcess;

double err_Mean[34][4];
double err_Sigma[34][4];
double err_mean_m4l[34][4];
double err_sigma_m4l[34][4];
const char *l_name[34] = {
  "signal_30_15",
  "signal_40_15",
  "signal_50_20",
  "signal_60_25",
  "signal_70_30",
  "signal_80_35",
  "signal_90_40",
  "signal_100_45",
  "signal_56_18",
  "signal_60_22", 
  "signal_64_16", 
  "signal_66_29", 
  "signal_68_32", 
  "signal_73_20",
  "signal_76_34", 
  "signal_80_27",
  "signal_82_36",
  "signal_84_19",
  "signal_85_33", 
  "signal_86_24",
  "signal_92_37",
  "signal_93_28", 
  "signal_95_17",
  "signal_96_44",
  "signal_98_42",
  "signal_102_23",
  "signal_104_48",
  "signal_105_35",
  "signal_105_38",
  "signal_108_46",
  "signal_109_26",
  "signal_110_30",
  "signal_111_50",
  "signal_112_52"
};




TFile* file[34];
TFile* file_m4l[34];
TFile* file_kSyst[34][2];
double maxFitResult=-111;
double maxHistY=-111;
const char *i_name[] = {"4e", "2e2m", "4m", "all"};
 string line;
//TString files[]={"./inputListFile.dat", "./inputListFileSyst.dat"};
//ifstream myfile;// (files[0]);
//ifstream myfile ("inputListFile.dat");//for reco
 ifstream myfile ("inputListFileFull.dat");//for reco
ifstream myfileKinSyst ("inputListFileFullSyst.dat");//for reco
 //ifstream myfile ("inputListFileTruth.dat");//for truth
vector<TString> lineCointainerAvgM;
vector<TString> lineCointainerAvgMkSyst[7][2];
vector<TString> lineCointainerM4l;
vector <vector<string>> vectorContainerkSyst;
string m4l;
string EG_RESOLUTION_ALL1up;
  Int_t count_gen_mZd[18];
  double mean_gen_mZd[18];
  double sigma_gen_mZd[18];

  
  Int_t  count_gen_mS[18];
  double mean_gen_mS[18];
  double sigma_gen_mS[18];
double sigma_gen_mS_new[36];
  
  double mS_gen[18];
double mS_gen_new[36];
  double mZd_gen[18];
  double QCD_scale_up[18][3];
  double QCD_scale_down[18][18];
  
  double PDF_scale_up[18][3];
  double PDF_scale_down[18][3];

  double QCD_uncert_up[18];
  double QCD_uncert_down[18];
  
  double PDF_uncert_up[18];
  double PDF_uncert_down[18];
    TFile* file_saveFitHist;
     TObjArray *MyHistArrayGen = new TObjArray(0);
TObjArray *MyHistArrayHistAll = new TObjArray(0);
    //TFile* file_saveGenHist =  new TFile("SavingGenHist.root", "recreate");
    //file_saveGenHist->Close();
    TFile *file_saveGenHist;
    TH1F *sig;
    TH1F *sig_up;
    TH1F *sig_down;
    TDirectory *myDirect;
    TH1D *hGen_mZd[18][4];
    TH1D *hGen_mS[18][4];
    int channel;
 TCanvas *c1 = new TCanvas("c1","c1",700,500);
      TPad* thePad = (TPad*)c1->cd();

Double_t M_Z_d_i, M_S_j;
int i_Nwsyst = -1;
int i_var = -1;
int i_kSyst ;
//gROOT->ForceStyle();
//gStyle->SetOptStat(0);
//    gStyle->SetOptFit();


 const char *c_mZdmS[] =
      {
	"mZd70_mH250GeV",
	"mZd125_mH500GeV",
	"mZd130_mH600GeV",
	"mZd200_mH550GeV",
	"mZd135_mH300GeV",
	"mZd230_mH560GeV",
	"mZd260_mH650GeV",
	"mZd75_mH620GeV",
	"mZd110_mH700GeV",
	"mZd180_mH440GeV",
	"mZd160_mH380GeV",
	"mZd170_mH520GeV",
	"mZd60_mH510GeV",
	"mZd65_mH655GeV",
	"mZd165_mH615GeV",
	"mZd235_mH720GeV",
	"mZd185_mH680GeV",
	"mZd210_mH780GeV"
      };
    
    const char *c_mZd[] =
      {
	"mZd70",
	"mZd125",
	"mZd130",
	"mZd200",
	"mZd135",
	"mZd230",
	"mZd260",
	"mZd75",
	"mZd110",
	"mZd180",
	"mZd160",
	"mZd170",
	"mZd60",
	"mZd65",
	"mZd165",
	"mZd235",
	"mZd185",
	"mZd210"
      };

 const char *c_mSPrime[] =
    {
      "30",	 
      "40", 
      "50", 
      "60", 
      "70", 
      "80", 
      "90", 
      "100", 
      "56",
      "60", 
      "64", 
      "66", 
      "68", 
      "73", 
      "76", 
      "80", 
      "82", 
      "84", 
      "85", 
      "86", 
      "92", 
      "93", 
      "95", 
      "96", 
      "98", 
      "102", 
      "104", 
      "105", 
      "105", 
      "108", 
      "109", 
      "110", 
      "111", 
      "112"
    };
  
  const char *c_mZdPrime[] =
      {
"15", 
"15", 
"20", 
"25", 
"30", 
"35", 
"40", 
"45", 
"18", 
"22", 
"16", 
"29", 
"32", 
"20", 
"34", 
"27", 
"36", 
"19", 
"33", 
"24", 
"37", 
"28", 
"17", 
"44", 
"42", 
"23", 
"48", 
"35", 
"38", 
"46", 
"26", 	
"30",      
"50", 
"52" 
 };
    const char *c_mS[] =
      {
	"mH250GeV",
	"mH500GeV",
	"mH600GeV",
	"mH550GeV",
	"mH300GeV",
	"mH560GeV",
	"mH650GeV",
	"mH620GeV",
	"mH700GeV",
	"mH440GeV",
	"mH380GeV",
	"mH520GeV",
	"mH510GeV",
	"mH655GeV",
	"mH615GeV",
	"mH720GeV",
	"mH680GeV",
	"mH780GeV"
      };
  
    const char *c_wSyst[] =
      {
	"EL_EFF_ID_TOTAL",
	"EL_EFF_ISO_TOTAL",
	"EL_EFF_RECO_TOTAL",
	"MUON_EFF_ISO_STAT",
	"MUON_EFF_ISO_SYS",
	"MUON_EFF_RECO_STAT",
	"MUON_EFF_RECO_STAT_LOWPT",
	"MUON_EFF_RECO_SYS",
	"MUON_EFF_RECO_SYS_LOWPT",
	"MUON_EFF_TTVA_STAT",
	"MUON_EFF_TTVA_SYS",
	"Pileup_weight"
      };

const char *c_kSyst[] =
      {
	"EG_RESOLUTION_ALL1",
	"EG_SCALE_ALL1",
	"MUONS_ID1",
	"MUONS_MS1",
	"MUONS_SAGITTA_RESBIAS1",
	"MUONS_SAGITTA_RHO1",
	"MUONS_SCALE1"

      };
    
    const char *c_vSyst[] =
      {
	"up",
	"down"
      };
   


void  initializeVar()
{

  mZd[0]  = 15; 
  mZd[1]  = 15; 
  mZd[2]  = 20; 
  mZd[3]  = 25; 
  mZd[4]  = 30; 
  mZd[5]  = 35; 
  mZd[6]  = 40; 
  mZd[7]  = 45; 
  mZd[8]  = 18; 
  mZd[9]  = 22; 
  mZd[10] = 16; 
  mZd[11] = 29; 
  mZd[12] = 32; 
  mZd[13] = 20; 
  mZd[14] = 34; 
  mZd[15] = 27; 
  mZd[16] = 36; 
  mZd[17] = 19; 
  mZd[18] = 33; 
  mZd[19] = 24; 
  mZd[20] = 37; 
  mZd[21] = 28; 
  mZd[22] = 17; 	
  mZd[23] = 44; 	
  mZd[24] = 42; 	
  mZd[25] = 23; 	
  mZd[26] = 48; 
  mZd[27] = 35; 	
  mZd[28] = 38; 	
  mZd[29] = 46; 	
  mZd[30] = 26; 
  mZd[31] = 30; 	
  mZd[32] = 50; 	
  mZd[33] = 52; 	
	
 
  
  



  mZd_gen[0]  = 70;
  mZd_gen[1]  = 125;
  mZd_gen[2]  = 130;
  mZd_gen[3]  = 200;
  mZd_gen[4]  = 135;
  mZd_gen[5]  = 230;
  mZd_gen[6]  = 260;
  mZd_gen[7]  = 75;
  mZd_gen[8]  = 110;
  mZd_gen[9]  = 180;
  mZd_gen[10] = 160;
  mZd_gen[11] = 170;
  mZd_gen[12] = 60;
  mZd_gen[13] = 65;
  mZd_gen[14] = 165;
  mZd_gen[15] = 235;
  mZd_gen[16] = 185;
  mZd_gen[17] = 210;
  
  mS_gen[0]  = 250;
  mS_gen[1]  = 500;
  mS_gen[2]  = 600;
  mS_gen[3]  = 550;
  mS_gen[4]  = 300;
  mS_gen[5]  = 560;
  mS_gen[6]  = 650;
  mS_gen[7]  = 620;
  mS_gen[8]  = 700;
  mS_gen[9]  = 440;
  mS_gen[10] = 380;
  mS_gen[11] = 520;
  mS_gen[12] = 510;
  mS_gen[13] = 655;
  mS_gen[14] = 615;
  mS_gen[15] = 720;
  mS_gen[16] = 680;
  mS_gen[17] = 780;

  mS_gen_new[0]  = 175;
  mS_gen_new[1]	 = 175;
  mS_gen_new[2]	 = 250;
  mS_gen_new[3]	 = 250;
  mS_gen_new[4]	 = 325;
  mS_gen_new[5]	 = 400;
  mS_gen_new[6]	 = 400;
  mS_gen_new[7]	 = 750;
  mS_gen_new[8]	 = 750;
  mS_gen_new[9]	 = 800;
  mS_gen_new[10] = 150;
  mS_gen_new[11] = 200;
  mS_gen_new[12] = 225;
  mS_gen_new[13] = 300;
  mS_gen_new[14] = 320;
  mS_gen_new[15] = 350;
  mS_gen_new[16] = 375;
  mS_gen_new[17] = 380;
  mS_gen_new[18] = 440;
  mS_gen_new[19] = 450;
  mS_gen_new[20] = 500;
  mS_gen_new[21] = 500;
  mS_gen_new[22] = 510;
  mS_gen_new[23] = 520;
  mS_gen_new[24] = 550;
  mS_gen_new[25] = 560;
  mS_gen_new[26] = 600;
  mS_gen_new[27] = 625;
  mS_gen_new[28] = 650;
  mS_gen_new[29] = 660;
  mS_gen_new[30] = 680;
  mS_gen_new[31] = 690;
  mS_gen_new[32] = 700;
  mS_gen_new[33] = 730;
  mS_gen_new[34] = 760;
  mS_gen_new[35] = 770;
 
 




  
  QCD_uncert_up[0]  = 5.9/100;   //mH = 250
  QCD_uncert_up[1]  = 5.9/100;   //mH = 500
  QCD_uncert_up[2]  = 5.5/100;   //mH = 600
  QCD_uncert_up[3]  = 5.5/100;   //mH = 550
  QCD_uncert_up[4]  = 5.8/100;   //mH = 300
  QCD_uncert_up[5]  = 5.5/100;   //mH = 560
  QCD_uncert_up[6]  = 5.5/100;   //mH = 650
  QCD_uncert_up[7]  = 5.5/100;   //mH = 620
  QCD_uncert_up[8]  = 5.4/100;   //mH = 700
  QCD_uncert_up[9]  = 5.7/100;   //mH = 440
  QCD_uncert_up[10] = 5.7/100;   //mH = 380
  QCD_uncert_up[11] = 5.6/100;   //mH = 520
  QCD_uncert_up[12] = 5.6/100;   //mH = 510
  QCD_uncert_up[13] = 5.5/100;   //mH = 655
  QCD_uncert_up[14] = 5.5/100;   //mH = 615
  QCD_uncert_up[15] = 5.4/100;   //mH = 720
  QCD_uncert_up[16] = 5.4/100;   //mH = 680
  QCD_uncert_up[17] = 5.5/100;   //mH = 780
  
  
  QCD_uncert_down[0]  = 6.5/100;   //mH = 250
  QCD_uncert_down[1]  = 5.1/100;   //mH = 500
  QCD_uncert_down[2]  = 4.9/100;   //mH = 600
  QCD_uncert_down[3]  = 5.0/100;   //mH = 550
  QCD_uncert_down[4]  = 6.2/100;   //mH = 300
  QCD_uncert_down[5]  = 4.9/100;   //mH = 560
  QCD_uncert_down[6]  = 4.9/100;   //mH = 650
  QCD_uncert_down[7]  = 4.9/100;   //mH = 620
  QCD_uncert_down[8]  = 4.8/100;   //mH = 700
  QCD_uncert_down[9]  = 5.3/100;   //mH = 440
  QCD_uncert_down[10] = 5.7/100;   //mH = 380
  QCD_uncert_down[11] = 5.1/100;   //mH = 520
  QCD_uncert_down[12] = 5.1/100;   //mH = 510
  QCD_uncert_down[13] = 4.9/100;   //mH = 655
  QCD_uncert_down[14] = 5.5/100;   //mH = 615
  QCD_uncert_down[15] = 4.8/100;   //mH = 720
  QCD_uncert_down[16] = 4.8/100;   //mH = 680
  QCD_uncert_down[17] = 4.7/100;   //mH = 780
  
  
  PDF_uncert_up[0]  = 2.9/100;   //mH = 250
  PDF_uncert_up[1]  = 3.1/100;   //mH = 500
  PDF_uncert_up[2]  = 3.4/100;   //mH = 600
  PDF_uncert_up[3]  = 3.2/100;   //mH = 550
  PDF_uncert_up[4]  = 2.9/100;   //mH = 300
  PDF_uncert_up[5]  = 3.2/100;   //mH = 560
  PDF_uncert_up[6]  = 3.5/100;   //mH = 650
  PDF_uncert_up[7]  = 3.4/100;   //mH = 620
  PDF_uncert_up[8]  = 3.7/100;   //mH = 700
  PDF_uncert_up[9]  = 3.0/100;   //mH = 440
  PDF_uncert_up[10] = 2.9/100;   //mH = 380
  PDF_uncert_up[11] = 3.1/100;   //mH = 520
  PDF_uncert_up[12] = 3.1/100;   //mH = 510
  PDF_uncert_up[13] = 3.5/100;   //mH = 655
  PDF_uncert_up[14] = 3.4/100;   //mH = 615
  PDF_uncert_up[15] = 3.7/100;   //mH = 720
  PDF_uncert_up[16] = 3.7/100;   //mH = 680
  PDF_uncert_up[17] = 4.0/100;   //mH = 780
  
  
  PDF_uncert_down[0]  = 2.9/100;   //mH = 250
  PDF_uncert_down[1]  = 3.1/100;   //mH = 500
  PDF_uncert_down[2]  = 3.4/100;   //mH = 600
  PDF_uncert_down[3]  = 3.2/100;   //mH = 550
  PDF_uncert_down[4]  = 2.9/100;   //mH = 300
  PDF_uncert_down[5]  = 3.2/100;   //mH = 560
  PDF_uncert_down[6]  = 3.5/100;   //mH = 650
  PDF_uncert_down[7]  = 3.4/100;   //mH = 620
  PDF_uncert_down[8]  = 3.7/100;   //mH = 700
  PDF_uncert_down[9]  = 3.0/100;   //mH = 440
  PDF_uncert_down[10] = 2.9/100;   //mH = 380
  PDF_uncert_down[11] = 3.1/100;   //mH = 520
  PDF_uncert_down[12] = 3.1/100;   //mH = 510
  PDF_uncert_down[13] = 3.5/100;   //mH = 655
  PDF_uncert_down[14] = 3.4/100;   //mH = 615
  PDF_uncert_down[15] = 3.7/100;   //mH = 720
  PDF_uncert_down[16] = 3.7/100;   //mH = 680
  PDF_uncert_down[17] = 4.0/100;   //mH = 780
  
  
  mS[0]  = 30; 
  mS[1]  = 40; 
  mS[2]  = 50; 
  mS[3]  = 60; 
  mS[4]  = 70; 
  mS[5]  = 80; 
  mS[6]  = 90; 
  mS[7]  = 100; 
  mS[8]  = 56; 
  mS[9]  = 60; 
  mS[10] = 64; 
  mS[11] = 66; 
  mS[12] = 68; 
  mS[13] = 73; 
  mS[14] = 76; 
  mS[15] = 80; 
  mS[16] = 82; 
  mS[17] = 84; 
  mS[18] = 85; 
  mS[19] = 86; 
  mS[20] = 92; 
  mS[21] = 93; 
  mS[22] = 95; 
  mS[23] = 96; 
  mS[24] = 98; 
  mS[25] = 102; 
  mS[26] = 104; 
  mS[27] = 105; 
  mS[28] = 105; 
  mS[29] = 108; 
  mS[30] = 109; 
  mS[31] = 110; 
  mS[32] = 111; 
  mS[33] = 112; 
 

  //TF1 *fit = new TF1("MyCrystalBall","crystalball",-5.,5.);

   if (channel == 0)
    {
  leftRangeFit[0] = mZd[0]*(1-1.2*sigma_el);
  leftRangeFit[1] = mZd[1]*(1-1.2*sigma_el);
  leftRangeFit[2] = mZd[2]*(1-1.2*sigma_el);
  leftRangeFit[3] = mZd[3]*(1-1.2*sigma_el);
  leftRangeFit[4] = mZd[4]*(1-1.2*sigma_el);
  leftRangeFit[5] = mZd[5]*(1-1.2*sigma_el);
  leftRangeFit[6] = mZd[6]*(1-0.5*sigma_el);
  leftRangeFit[7] = mZd[7]*(1-.6*sigma_el);
  leftRangeFit[8] = mZd[8]*(1-.5*sigma_el);

  rightRangeFit[0] = mZd[0]*(1+1.2*sigma_el);
  rightRangeFit[1] = mZd[1]*(1+1.2*sigma_el);
  rightRangeFit[2] = mZd[2]*(1+1.2*sigma_el);
  rightRangeFit[3] = mZd[3]*(1+1.2*sigma_el);
  rightRangeFit[4] = mZd[4]*(1+1.2*sigma_el);
  rightRangeFit[5] = mZd[5]*(1+1.2*sigma_el);
  rightRangeFit[6] = mZd[6]*(1+0.5*sigma_el);
  rightRangeFit[7] = mZd[7]*(1+.6*sigma_el);
  rightRangeFit[8] = mZd[8]*(1+.5*sigma_el);

  leftRangeFitM4l[0] = mS[0]*(1-1.*sigma_el);
  leftRangeFitM4l[1] = mS[1]*(1-1.*sigma_el);
  leftRangeFitM4l[2] = mS[2]*(1-2.*sigma_el);
  leftRangeFitM4l[3] = mS[3]*(1-7.5*sigma_el);
  leftRangeFitM4l[4] = mS[4]*(1-1.*sigma_el);
  leftRangeFitM4l[5] = mS[5]*(1-1.5*sigma_el);
  leftRangeFitM4l[6] = mS[6]*(1-1.5*sigma_el);
  leftRangeFitM4l[7] = mS[7]*(1-7.5*sigma_el);
  leftRangeFitM4l[8] = mS[8]*(1-9.5*sigma_el);

  rightRangeFitM4l[0] = mS[0]*(1+1.*sigma_el);
  rightRangeFitM4l[1] = mS[1]*(1+1.*sigma_el);
  rightRangeFitM4l[2] = mS[2]*(1+1.8*sigma_el);
  rightRangeFitM4l[3] = mS[3]*(1+7.5*sigma_el);
  rightRangeFitM4l[4] = mS[4]*(1+.8*sigma_el);
  rightRangeFitM4l[5] = mS[5]*(1+1.*sigma_el);
  rightRangeFitM4l[6] = mS[6]*(1+2.*sigma_el);
  rightRangeFitM4l[7] = mS[7]*(1+7.5*sigma_el);
  rightRangeFitM4l[8] = mS[8]*(1+9.5*sigma_el);

  leftRangeHistX[0] = mZd[0]-5;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = mZd[3]-10;
  leftRangeHistX[4] = mZd[4]-10;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = mZd[7]-10;
  leftRangeHistX[8] = mZd[8]-10;

  rightRangeHistX[0] = mZd[0]+5;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = mZd[3]+10;
  rightRangeHistX[4] = mZd[4]+10;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = mZd[7]+10;
  rightRangeHistX[8] = mZd[8]+10;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = mS[2]-80;
  leftRangeHistXM4l[3] = mS[3]-300;
  leftRangeHistXM4l[4] = mS[4]-50;
  leftRangeHistXM4l[5] = mS[5]-100;
  leftRangeHistXM4l[6] = mS[6]-100;
  leftRangeHistXM4l[7] = mS[7]-300;
  leftRangeHistXM4l[8] = mS[8]-300;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = mS[2]+80;
  rightRangeHistXM4l[3] = mS[3]+300;
  rightRangeHistXM4l[4] = mS[4]+50;
  rightRangeHistXM4l[5] = mS[5]+100;
  rightRangeHistXM4l[6] = mS[6]+100;
  rightRangeHistXM4l[7] = mS[7]+300;
  rightRangeHistXM4l[8] = mS[8]+300;
    }

   else if (channel == 1)
    {
  leftRangeFit[0] = mZd[0]*(1-2.5*sigma_el);
  leftRangeFit[1] = mZd[1]*(1-1.2*sigma_el);
  leftRangeFit[2] = mZd[2]*(1-1.2*sigma_el);
  leftRangeFit[3] = mZd[3]*(1-1.2*sigma_el);
  leftRangeFit[4] = mZd[4]*(1-1.2*sigma_el);
  leftRangeFit[5] = mZd[5]*(1-1.1*sigma_el);
  leftRangeFit[6] = mZd[6]*(1-0.5*sigma_el);
  leftRangeFit[7] = mZd[7]*(1-1.1*sigma_el);
  leftRangeFit[8] = mZd[8]*(1-1.1*sigma_el);

  rightRangeFit[0] = mZd[0]*(1+2.5*sigma_el);
  rightRangeFit[1] = mZd[1]*(1+1.2*sigma_el);
  rightRangeFit[2] = mZd[2]*(1+1.2*sigma_el);
  rightRangeFit[3] = mZd[3]*(1+1.2*sigma_el);
  rightRangeFit[4] = mZd[4]*(1+1.2*sigma_el);
  rightRangeFit[5] = mZd[5]*(1+1.1*sigma_el);
  rightRangeFit[6] = mZd[6]*(1+0.5*sigma_el);
  rightRangeFit[7] = mZd[7]*(1+1.1*sigma_el);
  rightRangeFit[8] = mZd[8]*(1+1.1*sigma_el);

  leftRangeFitM4l[0] = mS[0]*(1-1.*sigma_el);
  leftRangeFitM4l[1] = mS[1]*(1-1.*sigma_el);
  leftRangeFitM4l[2] = mS[2]*(1-2.*sigma_el);
  leftRangeFitM4l[3] = mS[3]*(1-7.5*sigma_el);
  leftRangeFitM4l[4] = mS[4]*(1-1.*sigma_el);
  leftRangeFitM4l[5] = mS[5]*(1-1.5*sigma_el);
  leftRangeFitM4l[6] = mS[6]*(1-1.5*sigma_el);
  leftRangeFitM4l[7] = mS[7]*(1-7.5*sigma_el);
  leftRangeFitM4l[8] = mS[8]*(1-9.5*sigma_el);

  rightRangeFitM4l[0] = mS[0]*(1+1.*sigma_el);
  rightRangeFitM4l[1] = mS[1]*(1+1.*sigma_el);
  rightRangeFitM4l[2] = mS[2]*(1+1.8*sigma_el);
  rightRangeFitM4l[3] = mS[3]*(1+7.5*sigma_el);
  rightRangeFitM4l[4] = mS[4]*(1+.8*sigma_el);
  rightRangeFitM4l[5] = mS[5]*(1+1.*sigma_el);
  rightRangeFitM4l[6] = mS[6]*(1+2.*sigma_el);
  rightRangeFitM4l[7] = mS[7]*(1+7.5*sigma_el);
  rightRangeFitM4l[8] = mS[8]*(1+9.5*sigma_el);

  leftRangeHistX[0] = mZd[0]-5;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = mZd[3]-10;
  leftRangeHistX[4] = mZd[4]-10;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = mZd[7]-10;
  leftRangeHistX[8] = mZd[8]-20;

  rightRangeHistX[0] = mZd[0]+5;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = mZd[3]+10;
  rightRangeHistX[4] = mZd[4]+10;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = mZd[7]+10;
  rightRangeHistX[8] = mZd[8]+20;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = mS[2]-80;
  leftRangeHistXM4l[3] = mS[3]-300;
  leftRangeHistXM4l[4] = mS[4]-50;
  leftRangeHistXM4l[5] = mS[5]-100;
  leftRangeHistXM4l[6] = mS[6]-100;
  leftRangeHistXM4l[7] = mS[7]-300;
  leftRangeHistXM4l[8] = mS[8]-300;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = mS[2]+80;
  rightRangeHistXM4l[3] = mS[3]+300;
  rightRangeHistXM4l[4] = mS[4]+50;
  rightRangeHistXM4l[5] = mS[5]+100;
  rightRangeHistXM4l[6] = mS[6]+100;
  rightRangeHistXM4l[7] = mS[7]+300;
  rightRangeHistXM4l[8] = mS[8]+300;
    }

    else if (channel == 2)
    {
  leftRangeFit[0] = mZd[0]*(1-2.5*sigma_el);
  leftRangeFit[1] = mZd[1]*(1-1.2*sigma_el);
  leftRangeFit[2] = mZd[2]*(1-1.2*sigma_el);
  leftRangeFit[3] = mZd[3]*(1-1.2*sigma_el);
  leftRangeFit[4] = mZd[4]*(1-1.2*sigma_el);
  leftRangeFit[5] = mZd[5]*(1-1.2*sigma_el);
  leftRangeFit[6] = mZd[6]*(1-0.5*sigma_el);
  leftRangeFit[7] = mZd[7]*(1-.6*sigma_el);
  leftRangeFit[8] = mZd[8]*(1-.5*sigma_el);

  rightRangeFit[0] = mZd[0]*(1+2.5*sigma_el);
  rightRangeFit[1] = mZd[1]*(1+1.2*sigma_el);
  rightRangeFit[2] = mZd[2]*(1+1.2*sigma_el);
  rightRangeFit[3] = mZd[3]*(1+1.2*sigma_el);
  rightRangeFit[4] = mZd[4]*(1+1.2*sigma_el);
  rightRangeFit[5] = mZd[5]*(1+1.2*sigma_el);
  rightRangeFit[6] = mZd[6]*(1+0.5*sigma_el);
  rightRangeFit[7] = mZd[7]*(1+.6*sigma_el);
  rightRangeFit[8] = mZd[8]*(1+.5*sigma_el);

  leftRangeFitM4l[0] = mS[0]*(1-1.*sigma_el);
  leftRangeFitM4l[1] = mS[1]*(1-1.*sigma_el);
  leftRangeFitM4l[2] = mS[2]*(1-2.*sigma_el);
  leftRangeFitM4l[3] = mS[3]*(1-7.5*sigma_el);
  leftRangeFitM4l[4] = mS[4]*(1-1.*sigma_el);
  leftRangeFitM4l[5] = mS[5]*(1-1.5*sigma_el);
  leftRangeFitM4l[6] = mS[6]*(1-1.5*sigma_el);
  leftRangeFitM4l[7] = mS[7]*(1-7.5*sigma_el);
  leftRangeFitM4l[8] = mS[8]*(1-9.5*sigma_el);

  rightRangeFitM4l[0] = mS[0]*(1+1.*sigma_el);
  rightRangeFitM4l[1] = mS[1]*(1+1.*sigma_el);
  rightRangeFitM4l[2] = mS[2]*(1+1.8*sigma_el);
  rightRangeFitM4l[3] = mS[3]*(1+7.5*sigma_el);
  rightRangeFitM4l[4] = mS[4]*(1+.8*sigma_el);
  rightRangeFitM4l[5] = mS[5]*(1+1.*sigma_el);
  rightRangeFitM4l[6] = mS[6]*(1+2.*sigma_el);
  rightRangeFitM4l[7] = mS[7]*(1+7.5*sigma_el);
  rightRangeFitM4l[8] = mS[8]*(1+9.5*sigma_el);

  leftRangeHistX[0] = mZd[0]-5;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = mZd[3]-10;
  leftRangeHistX[4] = mZd[4]-10;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = mZd[7]-10;
  leftRangeHistX[8] = mZd[8]-10;

  rightRangeHistX[0] = mZd[0]+5;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = mZd[3]+10;
  rightRangeHistX[4] = mZd[4]+10;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = mZd[7]+10;
  rightRangeHistX[8] = mZd[8]+10;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = mS[2]-80;
  leftRangeHistXM4l[3] = mS[3]-300;
  leftRangeHistXM4l[4] = mS[4]-50;
  leftRangeHistXM4l[5] = mS[5]-100;
  leftRangeHistXM4l[6] = mS[6]-100;
  leftRangeHistXM4l[7] = mS[7]-300;
  leftRangeHistXM4l[8] = mS[8]-300;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = mS[2]+80;
  rightRangeHistXM4l[3] = mS[3]+300;
  rightRangeHistXM4l[4] = mS[4]+50;
  rightRangeHistXM4l[5] = mS[5]+100;
  rightRangeHistXM4l[6] = mS[6]+100;
  rightRangeHistXM4l[7] = mS[7]+300;
  rightRangeHistXM4l[8] = mS[8]+300;
    }


   else if (channel == 3)
    {
  leftRangeFit[0] = mZd[0]*(1-3.5*sigma_el);
  leftRangeFit[1] = mZd[1]*(1-1.7*sigma_el);
  leftRangeFit[2] = mZd[2]*(1-1.7*sigma_el);
  leftRangeFit[3] = mZd[3]*(1-1.*sigma_el);
  leftRangeFit[4] = mZd[4]*(1-1.*sigma_el);
  leftRangeFit[5] = mZd[5]*(1-1.2*sigma_el);
  leftRangeFit[6] = mZd[6]*(1-1.*sigma_el);
  leftRangeFit[7] = mZd[7]*(1-.6*sigma_el);
  leftRangeFit[8] = mZd[8]*(1-.5*sigma_el);
  leftRangeFit[9] = mZd[9]*(1-.5*sigma_el);
  leftRangeFit[10] = mZd[10]*(1-.5*sigma_el);
  leftRangeFit[11] = mZd[11]*(1-.5*sigma_el);
  leftRangeFit[12] = mZd[12]*(1-.5*sigma_el);
  leftRangeFit[13] = mZd[13]*(1-.5*sigma_el);
  leftRangeFit[14] = mZd[14]*(1-.5*sigma_el);
  leftRangeFit[15] = mZd[15]*(1-.5*sigma_el);
  leftRangeFit[16] = mZd[16]*(1-.5*sigma_el);
  leftRangeFit[17] = mZd[17]*(1-.5*sigma_el);
  leftRangeFit[18] = mZd[18]*(1-.5*sigma_el);
  leftRangeFit[19] = mZd[19]*(1-.5*sigma_el);
  leftRangeFit[20] = mZd[20]*(1-.5*sigma_el);
  leftRangeFit[21] = mZd[21]*(1-.5*sigma_el);
  leftRangeFit[22] = mZd[22]*(1-.5*sigma_el);
  leftRangeFit[23] = mZd[23]*(1-.5*sigma_el);
  leftRangeFit[24] = mZd[24]*(1-.5*sigma_el);
  leftRangeFit[25] = mZd[25]*(1-.5*sigma_el);
  leftRangeFit[26] = mZd[26]*(1-.5*sigma_el);
  leftRangeFit[27] = mZd[27]*(1-.5*sigma_el);
  leftRangeFit[28] = mZd[28]*(1-.5*sigma_el);
  leftRangeFit[29] = mZd[29]*(1-.5*sigma_el);
  leftRangeFit[30] = mZd[30]*(1-.5*sigma_el);
  leftRangeFit[31] = mZd[31]*(1-.5*sigma_el);
  leftRangeFit[32] = mZd[32]*(1-.5*sigma_el);
  leftRangeFit[33] = mZd[33]*(1-.5*sigma_el);
 

  

  rightRangeFit[0] = mZd[0]*(1+3.5*sigma_el);
  rightRangeFit[1] = mZd[1]*(1+1.7*sigma_el);
  rightRangeFit[2] = mZd[2]*(1+1.7*sigma_el);
  rightRangeFit[3] = mZd[3]*(1+1.*sigma_el);
  rightRangeFit[4] = mZd[4]*(1+1.*sigma_el);
  rightRangeFit[5] = mZd[5]*(1+1.1*sigma_el);
  rightRangeFit[6] = mZd[6]*(1+1.*sigma_el);
  rightRangeFit[7] = mZd[7]*(1+.6*sigma_el);
  rightRangeFit[8] = mZd[8]*(1+.5*sigma_el);
  rightRangeFit[9] = mZd[9]*(1+.5*sigma_el);
  rightRangeFit[10] = mZd[10]*(1+.5*sigma_el);
  rightRangeFit[11] = mZd[11]*(1+.5*sigma_el);
  rightRangeFit[12] = mZd[12]*(1+.5*sigma_el);
  rightRangeFit[13] = mZd[13]*(1+.5*sigma_el);
  rightRangeFit[14] = mZd[14]*(1+.5*sigma_el);
  rightRangeFit[15] = mZd[15]*(1+.5*sigma_el);
  rightRangeFit[16] = mZd[16]*(1+.5*sigma_el);
  rightRangeFit[17] = mZd[17]*(1+.5*sigma_el);
  rightRangeFit[18] = mZd[18]*(1+.5*sigma_el);
  rightRangeFit[19] = mZd[19]*(1+.5*sigma_el);
  rightRangeFit[20] = mZd[20]*(1+.5*sigma_el);
  rightRangeFit[21] = mZd[21]*(1+.5*sigma_el);
  rightRangeFit[22] = mZd[22]*(1+.5*sigma_el);
  rightRangeFit[23] = mZd[23]*(1+.5*sigma_el);
  rightRangeFit[24] = mZd[24]*(1+.5*sigma_el);
  rightRangeFit[25] = mZd[25]*(1+.5*sigma_el);
  rightRangeFit[26] = mZd[26]*(1+.5*sigma_el);
  rightRangeFit[27] = mZd[27]*(1+.5*sigma_el);
  rightRangeFit[28] = mZd[28]*(1+.5*sigma_el);
  rightRangeFit[29] = mZd[29]*(1+.5*sigma_el);
  rightRangeFit[30] = mZd[30]*(1+.5*sigma_el);
  rightRangeFit[31] = mZd[31]*(1+.5*sigma_el);
  rightRangeFit[32] = mZd[32]*(1+.5*sigma_el);
  rightRangeFit[33] = mZd[33]*(1+.5*sigma_el);
 

  leftRangeFitM4l[0] = mS[0]*(1-1.*sigma_el);
  leftRangeFitM4l[1] = mS[1]*(1-1.*sigma_el);
  leftRangeFitM4l[2] = mS[2]*(1-2.*sigma_el);
  leftRangeFitM4l[3] = mS[3]*(1-7.5*sigma_el);
  leftRangeFitM4l[4] = mS[4]*(1-1.*sigma_el);
  leftRangeFitM4l[5] = mS[5]*(1-1.5*sigma_el);
  leftRangeFitM4l[6] = mS[6]*(1-1.5*sigma_el);
  leftRangeFitM4l[7] = mS[7]*(1-7.5*sigma_el);
  leftRangeFitM4l[8] = mS[8]*(1-9.5*sigma_el);
  leftRangeFitM4l[9] = mS[9]*(1-1.5*sigma_el);
  leftRangeFitM4l[10] = mS[10]*(1-1.*sigma_el);
  leftRangeFitM4l[11] = mS[11]*(1-1.*sigma_el);
  leftRangeFitM4l[12] = mS[12]*(1-1.5*sigma_el);
  leftRangeFitM4l[13] = mS[13]*(1-1.5*sigma_el);
  leftRangeFitM4l[14] = mS[14]*(1-1.5*sigma_el);
  leftRangeFitM4l[15] = mS[15]*(1-1.5*sigma_el);
  leftRangeFitM4l[16] = mS[16]*(1-1.5*sigma_el);
  leftRangeFitM4l[17] = mS[17]*(1-1.5*sigma_el);
  leftRangeFitM4l[18] = mS[18]*(1-1.5*sigma_el);
  leftRangeFitM4l[19] = mS[19]*(1-1.5*sigma_el);
  leftRangeFitM4l[20] = mS[20]*(1-1.5*sigma_el);
  leftRangeFitM4l[21] = mS[21]*(1-1.5*sigma_el);
  leftRangeFitM4l[22] = mS[22]*(1-1.5*sigma_el);
  leftRangeFitM4l[23] = mS[23]*(1-1.5*sigma_el);
  leftRangeFitM4l[24] = mS[24]*(1-1.5*sigma_el);
  leftRangeFitM4l[25] = mS[25]*(1-1.5*sigma_el);
  leftRangeFitM4l[26] = mS[26]*(1-1.5*sigma_el);
  leftRangeFitM4l[27] = mS[27]*(1-1.5*sigma_el);
  leftRangeFitM4l[28] = mS[28]*(1-1.5*sigma_el);
  leftRangeFitM4l[29] = mS[29]*(1-1.5*sigma_el);
  leftRangeFitM4l[30] = mS[30]*(1-1.5*sigma_el);
  leftRangeFitM4l[31] = mS[31]*(1-1.5*sigma_el);
  leftRangeFitM4l[32] = mS[32]*(1-1.5*sigma_el);
  leftRangeFitM4l[33] = mS[33]*(1-1.5*sigma_el);
  

  rightRangeFitM4l[0] = mS[0]*(1+1.*sigma_el);
  rightRangeFitM4l[1] = mS[1]*(1+1.*sigma_el);
  rightRangeFitM4l[2] = mS[2]*(1+1.8*sigma_el);
  rightRangeFitM4l[3] = mS[3]*(1+7.5*sigma_el);
  rightRangeFitM4l[4] = mS[4]*(1+.8*sigma_el);
  rightRangeFitM4l[5] = mS[5]*(1+1.*sigma_el);
  rightRangeFitM4l[6] = mS[6]*(1+2.*sigma_el);
  rightRangeFitM4l[7] = mS[7]*(1+7.5*sigma_el);
  rightRangeFitM4l[8] = mS[8]*(1+9.5*sigma_el);
  rightRangeFitM4l[9] = mS[9]*(1+1.5*sigma_el);
  rightRangeFitM4l[10] = mS[10]*(1+1.*sigma_el);
  rightRangeFitM4l[11] = mS[11]*(1+1.*sigma_el);
  rightRangeFitM4l[12] = mS[12]*(1+1.5*sigma_el);
  rightRangeFitM4l[13] = mS[13]*(1+1.5*sigma_el);
  rightRangeFitM4l[14] = mS[14]*(1+1.5*sigma_el);
  rightRangeFitM4l[15] = mS[15]*(1+1.5*sigma_el);
  rightRangeFitM4l[16] = mS[16]*(1+1.5*sigma_el);
  rightRangeFitM4l[17] = mS[17]*(1+1.5*sigma_el);
  rightRangeFitM4l[18] = mS[18]*(1+1.5*sigma_el);
  rightRangeFitM4l[19] = mS[19]*(1+1.5*sigma_el);
  rightRangeFitM4l[20] = mS[20]*(1+1.5*sigma_el);
  rightRangeFitM4l[21] = mS[21]*(1+1.5*sigma_el);
  rightRangeFitM4l[22] = mS[22]*(1+1.5*sigma_el);
  rightRangeFitM4l[23] = mS[23]*(1+1.5*sigma_el);
  rightRangeFitM4l[24] = mS[24]*(1+1.5*sigma_el);
  rightRangeFitM4l[25] = mS[25]*(1+1.5*sigma_el);
  rightRangeFitM4l[26] = mS[26]*(1+1.5*sigma_el);
  rightRangeFitM4l[27] = mS[27]*(1+1.5*sigma_el);
  rightRangeFitM4l[28] = mS[28]*(1+1.5*sigma_el);
  rightRangeFitM4l[29] = mS[29]*(1+1.5*sigma_el);
  rightRangeFitM4l[30] = mS[30]*(1+1.5*sigma_el);
  rightRangeFitM4l[31] = mS[31]*(1+1.5*sigma_el);
  rightRangeFitM4l[32] = mS[32]*(1+1.5*sigma_el);
  rightRangeFitM4l[33] = mS[33]*(1+1.5*sigma_el);
 

  leftRangeHistX[0] = mZd[0]-5;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = mZd[3]-10;
  leftRangeHistX[4] = mZd[4]-10;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = mZd[7]-10;
  leftRangeHistX[8] = mZd[8]-10;
  leftRangeHistX[9] = mZd[9]-10;
  leftRangeHistX[10] = mZd[10]-10;
  leftRangeHistX[11] = mZd[11]-10;
  leftRangeHistX[12] = mZd[12]-10;
  leftRangeHistX[13] = mZd[13]-10;
  leftRangeHistX[14] = mZd[14]-10;
  leftRangeHistX[15] = mZd[15]-10;
  leftRangeHistX[16] = mZd[16]-10;
  leftRangeHistX[17] = mZd[17]-10;
  leftRangeHistX[18] = mZd[18]-10;
  leftRangeHistX[19] = mZd[19]-10;
  leftRangeHistX[20] = mZd[20]-10;
  leftRangeHistX[21] = mZd[21]-10;
  leftRangeHistX[22] = mZd[22]-10;
  leftRangeHistX[23] = mZd[23]-10;
  leftRangeHistX[24] = mZd[24]-10;
  leftRangeHistX[25] = mZd[25]-10;
  leftRangeHistX[26] = mZd[26]-10;
  leftRangeHistX[27] = mZd[27]-10;
  leftRangeHistX[28] = mZd[28]-10;
  leftRangeHistX[29] = mZd[29]-10;
  leftRangeHistX[30] = mZd[30]-10;
  leftRangeHistX[31] = mZd[31]-10;
  leftRangeHistX[32] = mZd[32]-10;
  leftRangeHistX[33] = mZd[33]-10;
 
  

  rightRangeHistX[0] = mZd[0]+5;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = mZd[3]+10;
  rightRangeHistX[4] = mZd[4]+10;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = mZd[7]+10;
  rightRangeHistX[8] = mZd[8]+10;
  rightRangeHistX[9] = mZd[9]+10;
  rightRangeHistX[10] = mZd[10]+10;
  rightRangeHistX[11] = mZd[11]+10;
  rightRangeHistX[12] = mZd[12]+10;
  rightRangeHistX[13] = mZd[13]+10;
  rightRangeHistX[14] = mZd[14]+10;
  rightRangeHistX[15] = mZd[15]+10;
  rightRangeHistX[16] = mZd[16]+10;
  rightRangeHistX[17] = mZd[17]+10;
  rightRangeHistX[18] = mZd[18]+10;
  rightRangeHistX[19] = mZd[19]+10;
  rightRangeHistX[20] = mZd[20]+10;
  rightRangeHistX[21] = mZd[21]+10;
  rightRangeHistX[22] = mZd[22]+10;
  rightRangeHistX[23] = mZd[23]+10;
  rightRangeHistX[24] = mZd[24]+10;
  rightRangeHistX[25] = mZd[25]+10;
  rightRangeHistX[26] = mZd[26]+10;
  rightRangeHistX[27] = mZd[27]+10;
  rightRangeHistX[28] = mZd[28]+10;
  rightRangeHistX[29] = mZd[29]+10;
  rightRangeHistX[30] = mZd[30]+10;
  rightRangeHistX[31] = mZd[31]+10;
  rightRangeHistX[32] = mZd[32]+10;
  rightRangeHistX[33] = mZd[33]+10;
 

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = mS[2]-80;
  leftRangeHistXM4l[3] = mS[3]-300;
  leftRangeHistXM4l[4] = mS[4]-50;
  leftRangeHistXM4l[5] = mS[5]-100;
  leftRangeHistXM4l[6] = mS[6]-100;
  leftRangeHistXM4l[7] = mS[7]-300;
  leftRangeHistXM4l[8] = mS[8]-300;
  leftRangeHistXM4l[9] = mS[9]-40;
  leftRangeHistXM4l[10] = mS[10]-40;
  leftRangeHistXM4l[11] = mS[11]-40;
  leftRangeHistXM4l[12] = mS[12]-100;
  leftRangeHistXM4l[13] = mS[13]-100;
  leftRangeHistXM4l[14] = mS[14]-100;
  leftRangeHistXM4l[15] = mS[15]-100;
  leftRangeHistXM4l[16] = mS[16]-100;
  leftRangeHistXM4l[17] = mS[17]-100;
  leftRangeHistXM4l[18] = mS[18]-100;
  leftRangeHistXM4l[19] = mS[19]-100;
  leftRangeHistXM4l[20] = mS[20]-100;
  leftRangeHistXM4l[21] = mS[21]-100;
  leftRangeHistXM4l[22] = mS[22]-100;
  leftRangeHistXM4l[23] = mS[23]-100;
  leftRangeHistXM4l[24] = mS[24]-100;
  leftRangeHistXM4l[25] = mS[25]-100;
  leftRangeHistXM4l[26] = mS[26]-100;
  leftRangeHistXM4l[27] = mS[27]-100;
  leftRangeHistXM4l[28] = mS[28]-100;
  leftRangeHistXM4l[29] = mS[29]-100;
  leftRangeHistXM4l[30] = mS[30]-100;
  leftRangeHistXM4l[31] = mS[31]-100;
  leftRangeHistXM4l[32] = mS[32]-100;
  leftRangeHistXM4l[33] = mS[33]-100;
 
		    
  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = mS[2]+80;
  rightRangeHistXM4l[3] = mS[3]+300;
  rightRangeHistXM4l[4] = mS[4]+50;
  rightRangeHistXM4l[5] = mS[5]+100;
  rightRangeHistXM4l[6] = mS[6]+100;
  rightRangeHistXM4l[7] = mS[7]+300;
  rightRangeHistXM4l[8] = mS[8]+300;
  rightRangeHistXM4l[9] = mS[9]+40;
  rightRangeHistXM4l[10] = mS[10]+40;
  rightRangeHistXM4l[11] = mS[11]+40;
  rightRangeHistXM4l[12] = mS[12]+100;
  rightRangeHistXM4l[13] = mS[13]+100;
  rightRangeHistXM4l[14] = mS[14]+100;
  rightRangeHistXM4l[15] = mS[15]+100;
  rightRangeHistXM4l[16] = mS[16]+100;
  rightRangeHistXM4l[17] = mS[17]+100;
  rightRangeHistXM4l[18] = mS[18]+100;
  rightRangeHistXM4l[19] = mS[19]+100;
  rightRangeHistXM4l[20] = mS[20]+100;
  rightRangeHistXM4l[21] = mS[21]+100;
  rightRangeHistXM4l[22] = mS[22]+100;
  rightRangeHistXM4l[23] = mS[23]+100;
  rightRangeHistXM4l[24] = mS[24]+100;
  rightRangeHistXM4l[25] = mS[25]+100;
  rightRangeHistXM4l[26] = mS[26]+100;
  rightRangeHistXM4l[27] = mS[27]+100;
  rightRangeHistXM4l[28] = mS[28]+100;
  rightRangeHistXM4l[29] = mS[29]+100;
  rightRangeHistXM4l[30] = mS[30]+100;
  rightRangeHistXM4l[31] = mS[31]+100;
  rightRangeHistXM4l[32] = mS[32]+100;
  rightRangeHistXM4l[33] = mS[33]+100;
 
    }


}
