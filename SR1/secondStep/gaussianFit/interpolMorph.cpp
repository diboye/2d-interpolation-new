#include <TStyle.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TSystem.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TFitResult.h>
#include <TObject.h>
#include <TTree.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TBox.h>
#include <TSpectrum.h>
#include <TLatex.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string.h>
#include <TGraph2DErrors.h>
#include <cmath>
#include <iomanip>
#include "RooWorkspace.h"
#include <RooRealVar.h>
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooAbsArg.h"
#include "RooMomentMorph.h"
#include "RooFit.h"
#include "TRandom3.h"
#include "TGraphErrors.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TSpline.h"
#include "TPaveStats.h"
#include "TExec.h"
#include "TPaletteAxis.h"
#include "declarationFile.h"
#include "functionForMain.h"
using namespace std;
using namespace RooFit;



int main(int argc, char **argv)
{
  //TString option = GetOption();
   //TString file = GetOption();
  TString option;
    TString newfile;
if (newfile.IsNull()) newfile="signal.root";
  else {
    Ssiz_t pos = newfile.Last('.');
    if (pos<0) pos=newfile.Length();
    newfile.Replace(pos,20,".root");
  }
   fOut = TFile::Open(newfile, "RECREATE");
   // TCanvas cv("cv","",800,800);
   //cv.Print(signal+"[", "pdf");

   NumberOfFileToProcess =32;

  /*********************************intialized the generated histograms with their different channels *************************/
  for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 18; j++)
	{
	  hGen_mZd[j][i] = new TH1D(TString::Format("hGen_mZd_%s_%s", i_name[i], c_mZd[j]), ";m_{Zd} GeV;events/[1.0 GeV]", 300, 0, 300);
	  hGen_mS[j][i] = new TH1D(TString::Format("hGen_mS_%s_%s", i_name[i], c_mS[j]),";m_{S} GeV;events/[2.5 GeV]", 552,120,1500);
	  
	}
    }
  /* //if (i_Nwsyst == 0)
  //{
   for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 9; j++)
	{
	  hist_avgM_nom[j][i] = new TH1D(TString::Format("hist_avgM_nom_%s_%s", i_name[i], l_name[j]), ";m_{Zd} GeV;events/[1.0 GeV]", 300, 0, 300);
	}
    }
   //  }*/
  

  /*************************************** vizialized a 2D plot of ms vs mZd***********************/
  Hist2D_gen_reco();
  c1->Clear();

  /******************************************** Reading the inputs********************************/
  //for reco
  /* if (myfile.is_open())
    {
      while ( getline (myfile,line) )
	{
	  if (line.find("m4l") != string::npos)
	    {
	      lineCointainerM4l.push_back(line);
	    }
	  //else lineCointainerAvgM.push_back(line);
	  
	}
      myfile.close();
    }*/

  for (int i = 0; i < NumberOfFileToProcess; i++)
    {
      
	  lineCointainerM4l.push_back(TString::Format("../../../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_Nominal/signal_%s_%s_Nominal_reco_NotApplying_slice_m4l_output_combined.root",c_mSPrime[i],c_mZdPrime[i]));
	  lineCointainerAvgM.push_back(TString::Format("../../../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_Nominal/signal_%s_%s_Nominal_reco_NoSliceCutNoXS_output_combined.root", c_mSPrime[i],c_mZdPrime[i]));

    }
  
  for (int i = 0; i < 7; i++)
    
    {
      for (int j = 0; j < NumberOfFileToProcess; j++)
	
	{
	  
	      lineCointainerAvgMkSyst[i][0].push_back(TString::Format("../../../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_%s/signal_%s_%s_%sup_reco_output_combined.root",c_kSyst[i],c_mSPrime[i],c_mZdPrime[i],  c_kSyst[i]));
	      lineCointainerAvgMkSyst[i][1].push_back(TString::Format("../../../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_%s/signal_%s_%s_%sdown_reco_output_combined.root",c_kSyst[i],c_mSPrime[i],c_mZdPrime[i],c_kSyst[i]));
	   
	  
	}
    }
  
  // for (int i =0; i < lineCointainerAvgMkSyst[0][0].size(); i++)
  // {
  //cout << " lines : " << lineCointainerAvgMkSyst[0][0][0] << '\n';
  //  cout << " lines : " << lineCointainerAvgMkSyst[0][0][1] << '\n';
  //  cout << " lines : " << lineCointainerAvgMkSyst[0][0][0] << '\n';
      //cout << " lines : " << lineCointainerAvgMkSyst[1][0][3] << '\n';
      // }
  //vectorContainerkSyst.push_back(lineCointainerMUONS_SCALE1[0]);
  // cout << "first line " << lineCointainerAvgMKynSystEG_RESup.size() << '\n';
      //cout << lineCointainer.size() << '\n';
  
  /********************************** extracting the m4l histograms then fit, graph the integral, the mean and sigma******************/
  
  for (channel = 0; channel < 4; channel++)
    {
      /***/  initializeVar(); /**/ //initializing the variables used in the main
      /*for (int j = 0; j < NumberOfFileToProcess; j++)
	{
	   cout << " lines : " << lineCointainerM4l[j]<< '\n';
	  file_m4l[j] = new TFile(lineCointainerM4l[j]);
	  hist_m4l_nom[j][channel]  = (TH1D*)file_m4l[j]->Get(TString::Format("havgM_%s", i_name[channel]));
	 
	  
	}
       FitM4lHisto();
       c1->Clear();
       //graph_integral_mS();
       //c1->Clear();
       graph_sigma_mS();
       c1->Clear();
       graph_mean_mS();*/
       //c1->Clear();
      /****************************** Generate the mS mass points with a gaussian*/
       TRandom3 *rand_gauss = new TRandom3();
       /* for (int i_gen = 0; i_gen < 18; i_gen++)
	{
	  for (int j = 0; j < count_gen_mS[i_gen]; j++)
	  //for (int j = 0; j < 319; j++)
	    {
	      M_S_j   = rand_gauss->Gaus(mean_gen_mS[i_gen], sigma_gen_mS[i_gen]);
	      hGen_mS[i_gen][channel]->Fill(M_S_j);
	    }
	}*/
      
       //compare_msGen_msReco();
       /*for (int j = 0; j < NumberOfFileToProcess; j++)
	    {
	      file_m4l[j]->Close();
	    }
	    c1->Clear();*/
	     for (i_Nwsyst = 0; i_Nwsyst < 1; i_Nwsyst++ )
	{
	  if (i_Nwsyst==0)
	    {
	      for (int j = 0; j < NumberOfFileToProcess; j++)
		{
		  file[j] = new TFile(lineCointainerAvgM[j]);
		  hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s",i_name[channel]));
		  // hist_avgM_nom[j][channel]->SetName(TString::Format("havgM_nominal_%s",i_name[channel]));
		  //MyHistArrayHistAll->AddLast(hist_avgM_nom[j][channel]);
		}
	      FitAvgMllHisto();
	      c1->Clear();
	      /* //c1.Print(signal, "pdf");
	      graph_integral_mZd();
	      c1->Clear();
	      graph_sigma_mZd();
	      c1->Clear();
	      graph_mean_mZd();
	      c1->Clear();
	      // c1->Divide(2,1);
	      plot_integral_mZd_vs_mS();
	      c1->Clear();*/
		
	      
	      for (int i_gen = 0; i_gen < 18; i_gen++)
		{
		  for (int j = 0; j < count_gen_mZd[i_gen]; j++)
		  //for (int j = 0; j < 319; j++)
		    {
		      M_Z_d_i = rand_gauss->Gaus(mean_gen_mZd[i_gen], sigma_gen_mZd[i_gen]);
		      hGen_mZd[i_gen][channel]->Fill(M_Z_d_i);
		    }
		  // hGen_mZd[i_gen][channel]->Scale(0.5);
		  
		}
	      
	     
	      //compare_mZdGen_mZdReco();
	      //SaveInNomDirect(); 
	      
	    }
	  
	  else
	    { 
	      for (i_var = 0; i_var < 2; i_var++)
		{
		  for (int i_gen = 0; i_gen < 18; i_gen++)//reset the histograms to avoid double counting events
		    {
		      hGen_mZd[i_gen][channel]->Reset();
		    }
		  for (int j = 0; j < NumberOfFileToProcess; j++)
		    {
		       file[j] = new TFile(lineCointainerAvgM[j]);
		      hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s_%s_%s",c_wSyst[i_Nwsyst], c_vSyst[i_var], i_name[channel]));
		      //hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s",i_name[channel])); //use nominal temporiraly
		    }
		    FitAvgMllHisto();
		    c1->Clear();
		    graph_integral_mZd();
		    c1->Clear();
		  graph_sigma_mZd();
		  c1->Clear();
		  graph_mean_mZd();
		  c1->Clear();
		  // c1->Divide(2,1);
		  for (int i_gen = 0; i_gen < 18; i_gen++)
		    {
		      
		      for (int j = 0; j < count_gen_mZd[i_gen]; j++)
		      //for (int j = 0; j < 319; j++)
			{
			  M_Z_d_i = rand_gauss->Gaus(mean_gen_mZd[i_gen], sigma_gen_mZd[i_gen]);
			  hGen_mZd[i_gen][channel]->Fill(M_Z_d_i);
			}
		      
		    }
		  
		   SaveOtherSystDirect();
		    for (int j = 0; j < NumberOfFileToProcess; j++)
			{
			  file[j]->Close();
			}
		}
	    }
	}
      
      
       /* for (int j = 0; j < NumberOfFileToProcess; j++)
	{
	  hist_avgM_nom[j][channel]->Reset();
	}*/
      

       /*  for (i_kSyst=0; i_kSyst< 7; i_kSyst++)
	{
		  for (i_var =0; i_var<2; i_var++)
		    {	  
		      for (int i_gen = 0; i_gen < 18; i_gen++)//reset the histograms to avoid double counting events
			{
			  hGen_mZd[i_gen][channel]->Reset();
			}
		     
		      for (int j = 0; j < NumberOfFileToProcess; j++)
			{
			  file_kSyst[j][i_var] = new TFile(lineCointainerAvgMkSyst[i_kSyst][i_var][j]);
			  //file[j] = new TFile(lineCointainerAvgM[j].c_str()); //use nominal temporarily
			  //hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s",i_name[channel])); //use nominal temporarily
			   hist_avgM_nom[j][channel]  = (TH1D*)file_kSyst[j][i_var]->Get(TString::Format("havgM_%s",i_name[channel]));
			  
			}
		      FitAvgMllHisto();
		      c1->Clear();
		      graph_integral_mZd();
		      c1->Clear();
		      graph_sigma_mZd();
		      c1->Clear();
		      graph_mean_mZd();
		      c1->Clear();
		      
		      for (int i_gen = 0; i_gen < 18; i_gen++)
			{
			  for (int j = 0; j < count_gen_mZd[i_gen]; j++)
			  //for (int j = 0; j < 9; j++)
			    {
			      M_Z_d_i = rand_gauss->Gaus(mean_gen_mZd[i_gen], sigma_gen_mZd[i_gen]);
			      hGen_mZd[i_gen][channel]->Fill(M_Z_d_i);
			    }
			}
		      for (int i_gen = 0; i_gen < 18; i_gen++)
			{
			  file_saveFitHist =  new TFile(TString::Format("BRscaled_gaussiansignal_%schannel_%s.root",i_name[channel], c_mZdmS[i_gen]),"update");
			  if (i_var ==0)  myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_kSyst[i_kSyst]));
			  else file_saveFitHist->cd(TString::Format("%s",c_kSyst[i_kSyst])); 
			  // for (int i_var = 0; i_var < 2; i_var++)
			  // {
			  //myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_kSyst[0]));
			  sig = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
			  sig->SetName(TString::Format("sig_%s",c_vSyst[i_var]));
			  file_saveFitHist->cd(TString::Format("%s",c_kSyst[i_kSyst]));
			  sig->Write();
			  sig->Reset();
	  // }
			  file_saveFitHist->Close();
			}
		      for (int j = 0; j < NumberOfFileToProcess; j++)
		    {
		      file_kSyst[j][i_var]->Close();
		      //file[j]->Close();//use nominal temporarily
		    }
		    }
	}*/

      
      
      
      //  c1->Clear(); 
      
       for (int i_gen = 0; i_gen < 18; i_gen++)//reset the histograms to avoid double counting events
			{
			  hGen_mZd[i_gen][channel]->Reset();
			}
    }
  cout << " i_kSyst "  << i_kSyst << endl;
  
  
  //TString signal = GetOption();
  TString signal;
  if (signal.IsNull()) {
    if (fOut && fOut->IsOpen()) {
      fOut->Write();
      fOut->Close();
    }
  } else {
    Ssiz_t pos = signal.Last('.');
    if (pos<0) pos=signal.Length();
    signal.Replace(pos,20,".pdf");
  }
 

  // cv.Print(signal+"]", "pdf");

   
  if (fOut && fOut->IsOpen()) {
    fOut->Write();
    fOut->Close();
  } 
  
  
  
  return 0;
}
