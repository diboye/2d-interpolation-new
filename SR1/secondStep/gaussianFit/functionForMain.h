//#include "declarationFile.h"

void Hist2D_gen_reco()

{

 TH2D *H2plane = new TH2D("H2plane","",20,0,300,20,0,1000);
  H2plane->Fill(20,175);
  H2plane->Fill(50,175);
  H2plane->Fill(50,400);
  H2plane->Fill(65,750);
  H2plane->Fill(70,250);
  H2plane->Fill(80,325);
  H2plane->Fill(100,400);
  H2plane->Fill(150,750);
  H2plane->Fill(250,800);
  H2plane->GetXaxis()->SetTitle("m_{Zd}");
  H2plane->GetZaxis()->SetTitle("ms");
  TH2D *H2planeGen = new TH2D("H2planeGen","",20,0,300,20,0,1000);
  H2planeGen->Fill(125,500);
  H2planeGen->Fill(130,600);
  H2planeGen->Fill(200,550);
  H2planeGen->Fill(135,300);
  H2planeGen->Fill(230,560);
  H2planeGen->Fill(260,650);
  H2planeGen->Fill(90,620);
  H2planeGen->Fill(95,700);
  H2planeGen->Fill(180,440);
  H2planeGen->Fill(160,380);
  H2planeGen->Fill(170,520);
  H2planeGen->Fill(60,510);
  H2planeGen->Fill(65,655);
  H2planeGen->Fill(165,615);
  H2planeGen->Fill(235,720);
  H2planeGen->Fill(185,680);
  H2planeGen->Fill(210,780);
  H2planeGen->GetXaxis()->SetTitle("m_{Zd}");
  H2planeGen->GetYaxis()->SetTitle("m_{S}");
  // TExec *ex_bkg = new TExec("ex_bkg","gStyle->SetPalette(kFruitPunch);");
  TExec *ex_sng = new TExec("ex_sng","gStyle->SetPalette(55);");
  gStyle->SetOptStat(0);
  H2plane->Draw("colz");
  gPad->Update();
  TPaletteAxis *palette = (TPaletteAxis*)H2plane->GetListOfFunctions()->FindObject("palette"); 
  palette->SetX1NDC(0.03);
  palette->SetX2NDC(-0.016);
  ex_sng->Draw();  
  H2planeGen->Draw("colz same");
  H2plane->GetZaxis()->SetLabelSize(0);
  gPad->Print("plots/mS_vs_mZd.pdf");
  
 
}
 std::ofstream outfile;

void FitM4lHisto()

{
  c1->Divide(6,6);
 gStyle->SetOptFit(1);
  gStyle->SetOptStat(1);

   
  //outfile = TFile::Open("FitParameterM4l.txt");

  outfile.open(TString::Format("FitParameterM4l_sr1_%s.txt", i_name[channel])); // append instead of overwrite
  outfile << "mS" << '\t' <<"mZd"<< '\t' << "mean" << '\t'<< "erMean" << '\t'<<"sigma"<< '\t'<< "erSigma" << '\t'<<"const"<< '\t'<< "erConst" << endl;

  //outfile << "mS" << '\t' <<"mZd"<< '\t' << "mean" << '\t'<<"sigma"<< '\t'<<"const" << endl;

   for (int j = 0; j < NumberOfFileToProcess; j++)
    {
      //if (j==6) continue; // to remove the mass point mZd = 100; mS= 400 GeV
      c1->cd(j+1);
      // hist_m4l_nom[j][channel]->Fit("gaus","","",leftRangeFitM4l[j],rightRangeFitM4l[j]);
     
      hist_m4l_nom[j][channel]->SetTitle(TString::Format("%s_%s",l_name[j], i_name[channel]));
      //hist_m4l_nom[j][channel]->GetXaxis()->SetRangeUser(leftRangeHistXM4l[j], rightRangeHistXM4l[j]);
      
      hist_m4l_nom[j][channel]->Fit("gaus", "E");
      hist_m4l_nom[j][channel]->GetXaxis()->SetRangeUser(mS[j]-20*hist_m4l_nom[j][channel]->GetRMS(), mS[j]+20*hist_m4l_nom[j][channel]->GetRMS());
     
      mean_m4l[j][channel] = hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParameter(1);
      sigma_m4l[j][channel]= hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParameter(2);
      //IntegralM4l[j][channel] = hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParameter(0)*sqrt(2*3.14)*SigmaM4l[j][channel];
      err_mean_m4l[j][channel] = hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParError(1);
      err_sigma_m4l[j][channel] = hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParError(2);
      // cout << "************count_gen_mS[0]***************" << IntegralM4l[j][channel] << endl;
      IntegralM4l[j][channel] = hist_m4l_nom[j][channel]->GetFunction("gaus")->Integral(leftRangeFitM4l[j], rightRangeFitM4l[j]);
      //IntegralM4lReco[j][channel] = hist_m4l_nom[j][channel]->IntegralAndError(120,1500,error_integral_m4l[j][channel],"");
      IntegralM4lReco[j][channel] = hist_m4l_nom[j][channel]->Integral(0, 1500);
      // IntegralM4lReco[j][channel] = hist_m4l_nom[j][channel]->GetEntries();
     
      maxFitResult = hist_m4l_nom[j][channel]->GetFunction("gaus")->GetMaximum();
      maxHistY = hist_m4l_nom[j][channel]->GetMaximum();
       if (channel ==0)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+100); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+100);
	}
	}

      else if (channel ==1)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+100); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+100);
	}
	}
       else if (channel ==2)
	{
      if (maxFitResult >= maxHistY)
		{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+100); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+100);
	}
	}

      if (channel ==3)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+100); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+100);
	}
	}
      
      hist_m4l_nom[j][channel]->Draw("func");
      //hist_m4l_nom[j][channel]->Draw();
      //hist_m4l_nom[j][channel]->SetStats(0);
     thePad->Print(TString::Format("plots/hist_m4l_nominal_%s_%s.pdf",l_name[32], i_name[channel]));

     outfile << std::setprecision(3) <<mS[j]<< '\t' <<mZd[j] <<'\t' << mean_m4l[j][channel] << '\t' <<err_mean_m4l[j][channel]<< '\t' << sigma_m4l[j][channel] << '\t' << err_sigma_m4l[j][channel] <<  '\t' << hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParameter(0) << '\t' << hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParError(0) << endl;

     //outfile << std::setprecision(3) <<mS[j]<< '\t' <<mZd[j] <<'\t' << mean_m4l[j][channel] << '\t' << sigma_m4l[j][channel] << '\t' << hist_m4l_nom[j][channel]->GetFunction("gaus")->GetParameter(0) << endl;
      
    }


   outfile.close();	 

}

void graph_integral_mS()
{
  TMultiGraph  *mg_mS  = new TMultiGraph();
  Double_t mass_mS[5]  = {175,325,400,750,800};
  Double_t integral_mS[5]  = {IntegralM4l[0][channel],IntegralM4l[5][channel],IntegralM4l[2][channel],IntegralM4l[7][channel],IntegralM4l[8][channel]};
  TGraphErrors *gr_integral_mS = new TGraphErrors(5,mass_mS,integral_mS,0,0);
  gr_integral_mS->SetLineColor(kRed);
  gr_integral_mS->Draw("acp");
  // mg_mS->Add(gr_integral_mS);
  Double_t integral_reco_mS[5]  = {IntegralM4lReco[0][channel],IntegralM4lReco[5][channel],IntegralM4lReco[2][channel],IntegralM4lReco[7][channel],IntegralM4lReco[8][channel]};
  TGraphErrors *gr_integral_reco_mS = new TGraphErrors(5,mass_mS,integral_reco_mS,0,0);
  gr_integral_reco_mS->SetTitle("");
  gr_integral_reco_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  gr_integral_reco_mS->GetYaxis()->SetTitle("integral");
  gr_integral_reco_mS->SetLineColor(kBlue);
 // mg_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
 // mg_mS->GetYaxis()->SetTitle("integral");
 // mg_mS->Add(gr_integral_reco_mS);
  /* int n = gr_integral_reco_mS->GetN();
  double* y = gr_integral_reco_mS->GetY();
  int locmax = TMath::LocMax(n,y);
  double tmax = y[locmax]*10000;  
  mg_mS->GetYaxis()->SetRangeUser(0.1, tmax);
  c1->SetLogy();*/
  gr_integral_reco_mS->SetMarkerSize(1);
  gr_integral_reco_mS->SetMarkerStyle(8);
  //gr_integral_reco_mS->GetYaxis()->SetRangeUser(150, 500);
  gr_integral_reco_mS->Draw("ap");
  auto legend1 = new TLegend(0.2,0.7,0.3,0.8);
  legend1->SetTextFont(42);
  legend1->SetTextSize(0.03);
  legend1->SetBorderSize(0);
  legend1->AddEntry(gr_integral_reco_mS,"Integral from reco","lf");
  legend1->AddEntry(gr_integral_mS,"Integral from fit","lf");
  //legend1->Draw();
  thePad->Print(TString::Format("plots/hist_integral_nominal_mS_%s.pdf", i_name[channel]));
     for (int i =0; i<18; i++)
    {
      
      //count_gen_mS[i] = std::round(gr_integral_mS->Eval(mS_gen[i]));
      count_gen_mS[i] = std::round(gr_integral_reco_mS->Eval(mS_gen[i]));
    
    }
       //cout << "count_gen_mZd[0]" << gr_integral_mZd->Eval(70) << endl;
     /*  cout << "count_reco_mZd[0] = " << IntegralM4lReco[0][channel] << endl;
    cout << "count_reco_mZd[2] = " << IntegralM4lReco[2][channel] << endl;
    cout << "count_reco_mZd[3] = " << IntegralM4lReco[3][channel] << endl;
    cout << "count_reco_mZd[5] = " << IntegralM4lReco[5][channel] << endl;
    cout << "count_reco_mZd[6] = " << IntegralM4lReco[6][channel] << endl;
    cout << "count_reco_mZd[7] = " << IntegralM4lReco[7][channel] << endl;
    cout << "count_reco_mZd[8] = " << IntegralM4lReco[8][channel] << endl;

    //cout << "count_gen_mZd[0]" << gr_integral_reco_mZd->Eval(70) << endl;
    //cout << "count_gen_mZd[0]" << count_gen_mZd[0] << endl;

    cout << "count_fit_mZd[0] = " << IntegralM4l[0][channel] << endl;
    cout << "count_fit_mZd[2] = " << IntegralM4l[2][channel] << endl;
    cout << "count_fit_mZd[3] = " << IntegralM4l[3][channel] << endl;
    cout << "count_fit_mZd[5] = " << IntegralM4l[5][channel] << endl;
    cout << "count_fit_mZd[6] = " << IntegralM4l[6][channel] << endl;
    cout << "count_fit_mZd[7] = " << IntegralM4l[7][channel] << endl;
    cout << "count_fit_mZd[8] = " << IntegralM4l[8][channel] << endl;*/

  }

void graph_sigma_mS()

{
  c1->SetLogy(0);
  Double_t mass_mS[34]  = {
    30, 
    40, 
    50, 
    60, 
    70, 
    80, 
    90, 
    56, 
    60, 
    64, 
    66, 
    68, 
    73, 
    76, 
    80, 
    82, 
    84, 
    85, 
    86, 
    92, 
    93, 
    95, 
    96, 
    98,
    100,
    102,	
    104,	
    105,	
    105,	
    108,	
    109,	
    110,	
    111,	
    112,
  };
 
  Double_t sigma_mS[32]  =
    {
sigma_m4l[0][channel], 
sigma_m4l[1][channel], 
sigma_m4l[2][channel], 
sigma_m4l[3][channel], 
sigma_m4l[4][channel], 
sigma_m4l[5][channel], 
sigma_m4l[6][channel], 
sigma_m4l[8][channel], 
sigma_m4l[9][channel], 
sigma_m4l[10][channel], 
sigma_m4l[11][channel], 
sigma_m4l[12][channel], 
sigma_m4l[13][channel], 
sigma_m4l[14][channel], 
sigma_m4l[15][channel], 
sigma_m4l[16][channel], 
sigma_m4l[17][channel], 
sigma_m4l[18][channel], 
sigma_m4l[19][channel], 
sigma_m4l[20][channel], 
sigma_m4l[21][channel], 
sigma_m4l[22][channel], 
sigma_m4l[23][channel], 
sigma_m4l[24][channel], 
sigma_m4l[7][channel], 
sigma_m4l[25][channel], 
sigma_m4l[26][channel],
sigma_m4l[27][channel],
sigma_m4l[28][channel],
sigma_m4l[29][channel],
sigma_m4l[30][channel],
sigma_m4l[31][channel],
//sigma_m4l[32][channel],
//sigma_m4l[33][channel],

  };
  Double_t sigma_error_mS[32] = {
    err_sigma_m4l[0][channel], 
    err_sigma_m4l[1][channel], 
    err_sigma_m4l[2][channel], 
    err_sigma_m4l[3][channel], 
    err_sigma_m4l[4][channel], 
    err_sigma_m4l[5][channel], 
    err_sigma_m4l[6][channel], 
    err_sigma_m4l[8][channel], 
    err_sigma_m4l[9][channel], 
    err_sigma_m4l[10][channel], 
    err_sigma_m4l[11][channel], 
    err_sigma_m4l[12][channel], 
    err_sigma_m4l[13][channel], 
    err_sigma_m4l[14][channel], 
    err_sigma_m4l[15][channel], 
    err_sigma_m4l[16][channel], 
    err_sigma_m4l[17][channel], 
    err_sigma_m4l[18][channel], 
    err_sigma_m4l[19][channel], 
    err_sigma_m4l[20][channel], 
    err_sigma_m4l[21][channel], 
    err_sigma_m4l[22][channel], 
    err_sigma_m4l[23][channel], 
    err_sigma_m4l[24][channel], 
    err_sigma_m4l[7][channel], 
    err_sigma_m4l[25][channel], 
    err_sigma_m4l[26][channel],
    err_sigma_m4l[27][channel],
    err_sigma_m4l[28][channel],
    err_sigma_m4l[29][channel],
    err_sigma_m4l[30][channel],
    err_sigma_m4l[31][channel],
    //err_sigma_m4l[32][channel],
    //err_sigma_m4l[33][channel],
   
  };
  TGraphErrors *gr_sigma_mS = new TGraphErrors(32, mass_mS, sigma_mS,0,sigma_error_mS);
  TSpline3 *s_mS = new TSpline3("grmS",gr_sigma_mS);
  gr_sigma_mS->SetTitle("");
  gr_sigma_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  gr_sigma_mS->GetYaxis()->SetTitle("Fit param #sigma");
  // gr_sigma_mS->Fit("expo","","",175,800);
   gr_sigma_mS->Fit("pol2","","",30,115);
   //gr_sigma_mS->GetXaxis()->SetRangeUser(0, 900);
   gr_sigma_mS->GetYaxis()->SetRangeUser(0, 5);
  gr_sigma_mS->Draw("APE");
  gr_sigma_mS->SetMarkerSize(1);
  gr_sigma_mS->SetMarkerStyle(8);
  s_mS->SetLineColor(kRed);
   thePad->Print(TString::Format("plots/hist_sigma_nominal_mS_%s.pdf", i_name[channel]));
   /*    for (int i =0; i<18; i++)
    {
      
      //sigma_gen_mS[i] = gr_sigma_mS->GetFunction("expo")->Eval(mS_gen[i]);
      sigma_gen_mS[i] = gr_sigma_mS->GetFunction("pol2")->Eval(mS_gen[i]);
      cout << "*******sigma_mS[0]****** = " << sigma_gen_mS[i] << endl;
      
    }*/

       for (int i =0; i<32; i++)
    {
      
      //sigma_gen_mS[i] = gr_sigma_mS->GetFunction("expo")->Eval(mS_gen[i]);
      //sigma_gen_mS_new[i] = gr_sigma_mS->GetFunction("pol2")->Eval(mS_gen_new[i]);
      // cout << "mS " <<mS_gen_new[i] << " *******sigma_mS_new[0]****** = " << sigma_gen_mS_new[i] << endl;

      cout << "mS = " <<mS[i]  << " signal = "  << sigma_mS[i] << endl;

    }
      //cout << "*******sigma_mS[0]****** = " << sigma_gen_mS[0] << endl;
}
void graph_mean_mS()
{
Double_t mass_mS[34]  =
  {
    30, 
    40, 
    50, 
    60, 
    70, 
    80, 
    90, 
    56, 
    60, 
    64, 
    66, 
    68, 
    73, 
    76, 
    80, 
    82, 
    84, 
    85, 
    86, 
    92, 
    93, 
    95, 
    96, 
    98,
    100,
    102,	
    104,	
    105,	
    105,	
    108,	
    109,	
    110,	
    111,	
    112
};
   Double_t mean_mS[32]  =
     {
     mean_m4l[0][channel], 
     mean_m4l[1][channel], 
     mean_m4l[2][channel], 
     mean_m4l[3][channel], 
     mean_m4l[4][channel], 
     mean_m4l[5][channel], 
     mean_m4l[6][channel], 
     mean_m4l[8][channel], 
     mean_m4l[9][channel], 
     mean_m4l[10][channel], 
     mean_m4l[11][channel], 
     mean_m4l[12][channel], 
     mean_m4l[13][channel], 
     mean_m4l[14][channel], 
     mean_m4l[15][channel], 
     mean_m4l[16][channel], 
     mean_m4l[17][channel], 
     mean_m4l[18][channel], 
     mean_m4l[19][channel], 
     mean_m4l[20][channel], 
     mean_m4l[21][channel], 
     mean_m4l[22][channel], 
     mean_m4l[23][channel], 
     mean_m4l[24][channel], 
     mean_m4l[7][channel], 
     mean_m4l[25][channel], 
     mean_m4l[26][channel],
     mean_m4l[27][channel],
     mean_m4l[28][channel],
     mean_m4l[29][channel],
     mean_m4l[30][channel],
     mean_m4l[31][channel],
     //mean_m4l[32][channel],
     //mean_m4l[33][channel],
     };
  Double_t mean_error_mS[32] =
    {
     err_mean_m4l[0][channel], 
     err_mean_m4l[1][channel], 
     err_mean_m4l[2][channel], 
     err_mean_m4l[3][channel], 
     err_mean_m4l[4][channel], 
     err_mean_m4l[5][channel], 
     err_mean_m4l[6][channel], 
     err_mean_m4l[8][channel], 
     err_mean_m4l[9][channel], 
     err_mean_m4l[10][channel], 
     err_mean_m4l[11][channel], 
     err_mean_m4l[12][channel], 
     err_mean_m4l[13][channel], 
     err_mean_m4l[14][channel], 
     err_mean_m4l[15][channel], 
     err_mean_m4l[16][channel], 
     err_mean_m4l[17][channel], 
     err_mean_m4l[18][channel], 
     err_mean_m4l[19][channel], 
     err_mean_m4l[20][channel], 
     err_mean_m4l[21][channel], 
     err_mean_m4l[22][channel], 
     err_mean_m4l[23][channel], 
     err_mean_m4l[24][channel], 
     err_mean_m4l[7][channel], 
     err_mean_m4l[25][channel], 
     err_mean_m4l[26][channel],
     err_mean_m4l[27][channel],
     err_mean_m4l[28][channel],
     err_mean_m4l[29][channel],
     err_mean_m4l[30][channel],
     err_mean_m4l[31][channel],
     //err_mean_m4l[32][channel],
     //err_mean_m4l[33][channel],
    };
  TGraphErrors *gr_mean_mS = new TGraphErrors(32,mass_mS,mean_mS,0,mean_error_mS);
  gr_mean_mS->SetTitle("");
  gr_mean_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  gr_mean_mS->GetYaxis()->SetTitle("Fit param #mu");
  // c1->cd(2);
  gr_mean_mS->Fit("pol2","","",30,115);
  //gr_mean_mS->GetXaxis()->SetRangeUser(0, 9000);
  gr_mean_mS->GetYaxis()->SetRangeUser(0, 200);
  gr_mean_mS->Draw("APE");
  gr_mean_mS->SetMarkerSize(1);
  gr_mean_mS->SetMarkerStyle(8);
  //  s_mS->SetLineColor(kRed);
  thePad->Print(TString::Format("plots/hist_mean_nominal_mS_%s.pdf", i_name[channel]));
   for (int i =0; i<32; i++)
    {
      
      //sigma_gen_mS[i] = gr_sigma_mS->GetFunction("expo")->Eval(mS_gen[i]);
      //sigma_gen_mS_new[i] = gr_sigma_mS->GetFunction("pol2")->Eval(mS_gen_new[i]);
      // cout << "mS " <<mS_gen_new[i] << " *******sigma_mS_new[0]****** = " << sigma_gen_mS_new[i] << endl;

      cout << "mS = " <<mS[i]  << " signal = "  << mean_mS[i] << endl;

    }
  /*   for (int i =0; i<18; i++)
    {
      
           mean_gen_mS[i]  = gr_mean_mS->GetFunction("pol2")->Eval(mS_gen[i]);
	   cout << "*********mean_gen_mS[0]******** =  " << mean_gen_mS[0] << endl;
     
    }*/
}

void compare_msGen_msReco()

{
  c1->SetLogy(0);
  // cout << "integral_nom__mS" <<" " << hist_m4l_nom[4][channel]->Integral() <<endl;
  /* double scale250 = 1/hist_m4l_nom[4][channel]->Integral();
  hist_m4l_nom[4][channel]->Scale(scale250);
  double scale1 = 1/hGen_mS[0][channel]->Integral();
  hGen_mS[0][channel]->Scale(scale1);*/
  //hGen_mS[0][channel]->GetXaxis()->SetRangeUser(0, 500);
  //hist_m4l_nom[4][channel]->GetXaxis()->SetRangeUser(0, 500);
  hGen_mS[0][channel]->SetLineColor(2);
  hist_m4l_nom[4][channel]->SetLineColor(4);
  hGen_mS[0][channel]->GetYaxis()->SetTitle("events/[2.5 GeV]");
  hist_m4l_nom[4][channel]->Draw("hist");
  hGen_mS[0][channel]->Draw("hist same");
 
 
  gStyle->SetOptFit(0);
  gStyle->SetOptStat(0);
  auto legend = new TLegend(0.4,0.5,0.5,0.8);
  legend->SetTextFont(42);
  legend->SetTextSize(0.03);
  legend->SetBorderSize(0);
  legend->AddEntry(hist_m4l_nom[4][channel],"reco signal","lf");
  legend->AddEntry(hGen_mS[0][channel],"Generated signal","lf");
  //legend->SetX1NDC(.7); //new x start position
  //legend->SetX2NDC(.5);
  legend->Draw();
  thePad->Print(TString::Format("plots/hGen_mS_nominal_250_%s.pdf", i_name[channel]));
}
void FitAvgMllHisto()

{
 c1->Divide(6,6);
  gStyle->SetOptFit(1);
  gStyle->SetOptStat(1);

   outfile.open(TString::Format("FitParameterAvgMll_sr1_%s.txt", i_name[channel])); // append instead of overwrite
   outfile << "mS" << '\t' <<"mZd"<< '\t' << "mean" << '\t'<< "erMean" << '\t'<<"sigma"<< '\t'<< "erSigma" << '\t'<<"const"<< '\t'<< "erConst" << endl;
   //outfile << "mS" << '\t' <<"mZd"<< '\t' << "mean" << '\t'<<"sigma" << '\t'<<"const"<< endl;
  
  for (int j = 0; j < NumberOfFileToProcess; j++)
    {
      //if (j==6) continue; // to remove the mass point mZd = 100; mS= 400 GeV
      c1->cd(j+1);
      hist_avgM_nom[j][channel]->SetTitle(TString::Format("%s_%s",l_name[j],i_name[channel]));
    
      //hist_avgM_nom[j][channel]->Fit("gaus","E","",leftRangeFit[j],rightRangeFit[j]);

      //hist_avgM_nom[j][channel]->Fit("gaus");

      hist_avgM_nom[j][channel]->Fit("gaus","E","",hist_avgM_nom[j][channel]->GetMean()-5*hist_avgM_nom[j][channel]->GetRMS(),hist_avgM_nom[j][channel]->GetMean()+5*hist_avgM_nom[j][channel]->GetRMS());

       hist_avgM_nom[j][channel]->GetXaxis()->SetRangeUser(mZd[j]-20*hist_avgM_nom[j][channel]->GetRMS(), mZd[j]+20*hist_avgM_nom[j][channel]->GetRMS());
     
      Mean[j][channel] = hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParameter(1);
      Sigma[j][channel]= hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParameter(2);
      //Integral[j][channel] = (hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParameter(0))*sqrt(2*3.14)*Sigma[j][channel];
      //Integral[j][channel] = (hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParameter(0));
      //cout << "************count_gen_mZd[0]***************" << Integral[j][channel] << endl;
      err_Mean[j][channel] = hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParError(1);
      err_Sigma[j][channel] = hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParError(2);
      Integral[j][channel] = hist_avgM_nom[j][channel]->GetFunction("gaus")->Integral(leftRangeFit[j], rightRangeFit[j]);
      //Integral[j][channel] = hist_avgM_nom[j][channel]->GetFunction("gaus")->GetEntries();
      //IntegralReco[j][channel] = hist_avgM_nom[j][channel]->IntegralAndError(0,300,error_integral[j][channel],"");
      IntegralReco[j][channel] = hist_avgM_nom[j][channel]->Integral(0,300);
      hist_avgM_nom[j][channel]->GetXaxis()->SetRangeUser(leftRangeHistX[j], rightRangeHistX[j]);
      maxFitResult = hist_avgM_nom[j][channel]->GetFunction("gaus")->GetMaximum();
      maxHistY = hist_avgM_nom[j][channel]->GetMaximum();
      if (channel ==0)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.0001); // for at truth level with all channel, add 0.1
	}
      else
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.0001);
	}
	}
      else if (channel ==1)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.0001); // for at truth level with all channel, add 0.1
	}
      else
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.0001);
	}
	}
       else if (channel ==2)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.0001); // for at truth level with all channel, add 0.1
	}
      else
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.0001);
	}
	}

      else if (channel ==3)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.0001); // for at truth level with all channel, add 0.1
	}
      else
	{
	  hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.0001);
	}
	}
      
      hist_avgM_nom[j][channel]->Draw("func");

      if (i_Nwsyst ==0)
	{
	  thePad->Print(TString::Format("plots/hist_avgMll_nominal_%s_%s.pdf",l_name[32], i_name[channel]));
	}
      else if ((i_Nwsyst !=0) && (i_kSyst==-1))
	{
	  thePad->Print(TString::Format("plots/hist_avgMll_%s_%s_%s_%s.pdf",l_name[32], i_name[channel],c_wSyst[i_Nwsyst],c_vSyst[i_var]));
	}
       else
	{
	  thePad->Print(TString::Format("plots/hist_avgMll_%s_%s_%s_%s.pdf",l_name[32], i_name[channel],c_kSyst[i_kSyst],c_vSyst[i_var]));
	}
      outfile << std::setprecision(3) <<mS[j]<< '\t' <<mZd[j] <<'\t' << Mean[j][channel] << '\t' <<err_Mean[j][channel]<< '\t' << Sigma[j][channel] << '\t' << err_Sigma[j][channel] << '\t' << hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParameter(0) << '\t' << hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParError(0) << endl;

      //outfile << std::setprecision(3) <<mS[j]<< '\t' <<mZd[j] <<'\t' << Mean[j][channel] << '\t' << Sigma[j][channel] <<'\t' << hist_avgM_nom[j][channel]->GetFunction("gaus")->GetParameter(0) << endl;
      
    }
  outfile.close();
}

void graph_integral_mZd()

{
  TMultiGraph  *mg  = new TMultiGraph();
  //Double_t mass_mZd[6]  = {20,50,65,80,150,250};
    Double_t mass_mZd[7]  = {20,50,65,70, 80,150,250};
  //Double_t integral_mZd[6]  = {Integral[0][channel],Integral[1][channel],Integral[3][channel],Integral[5][channel],Integral[7][channel],Integral[8][channel]};
  //TGraphErrors *gr_integral_mZd = new TGraphErrors(6,mass_mZd,integral_mZd,0,0);
  // TSpline3 *i_mZd = new TSpline3("grmZd",gr_integral_mZd);
  //gr_integral_mZd->SetLineColor(kRed);
  //gr_integral_mZd->Draw("apl");
  //mg->Add(gr_integral_mZd);
   /* Double_t integral_reco_mZd[6]  = {IntegralReco[0][channel],IntegralReco[2][channel],IntegralReco[3][channel],IntegralReco[5][channel],IntegralReco[7][channel],IntegralReco[8][channel]};
   Double_t integral_reco_error_mZd[6]  = {error_integral[0][channel],error_integral[2][channel],error_integral[3][channel],error_integral[5][channel],error_integral[7][channel],error_integral[8][channel]};
   //TGraphErrors *gr_integral_reco_mZd = new TGraphErrors(6,mass_mZd,integral_reco_mZd,0,integral_reco_error_mZd);
   TGraphErrors *gr_integral_reco_mZd = new TGraphErrors(6,mass_mZd,integral_reco_mZd,0,0);*/

    Double_t integral_reco_mZd[7]  = {IntegralReco[0][channel],IntegralReco[2][channel],IntegralReco[3][channel],IntegralReco[4][channel],IntegralReco[5][channel],IntegralReco[7][channel],IntegralReco[8][channel]};

  Double_t integral_reco_error_mZd[7]  = {error_integral[0][channel],error_integral[2][channel],error_integral[3][channel],error_integral[4][channel],error_integral[5][channel],error_integral[7][channel],error_integral[8][channel]};
  
  TGraphErrors *gr_integral_reco_mZd = new TGraphErrors(7,mass_mZd,integral_reco_mZd,0,integral_reco_error_mZd);
  
  // TSpline3 *r_mZd = new TSpline3("grmZd",gr_integral_reco_mZd);
  gr_integral_reco_mZd->SetTitle("");
  gr_integral_reco_mZd->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_integral_reco_mZd->GetYaxis()->SetTitle("integral");
  //gr_integral_reco_mZd->SetLineColor(kBlue);
  //mg->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  //mg->GetYaxis()->SetTitle("integral");
  //mg->Add(gr_integral_reco_mZd);
  //int n = gr_integral_reco_mZd->GetN();
  //double* y = gr_integral_reco_mZd->GetY();
  //int locmax = TMath::LocMax(n,y);
  //double tmax = y[locmax]*100;  
  //mg->GetYaxis()->SetRangeUser(0.1, tmax);
  gr_integral_reco_mZd->GetYaxis()->SetRangeUser(0, 1000);
   //c1->SetLogy();
  gr_integral_reco_mZd->Draw("ap");

  TBox *box3 = new TBox(89,0,99,1000);
  box3->SetLineColor(kBlack);
  box3->SetFillStyle(3354);
  box3->SetFillColor(kGray+2);
  box3->SetLineWidth(603);
  TBox *box2 = new TBox(89,0,99,1000);
  box2->SetFillColor(0);
  box2->SetLineWidth(603);
  box2->Draw("F");
  box3->Draw("F");
  thePad->SetTickx();
  thePad->SetTicky();

  TLatex *tex = new TLatex();
  tex->SetTextFont(72);
  tex->SetTextSize(0.03);
  tex->DrawLatex(150,900,"ATLAS");
  tex->SetTextFont(42);
  tex->SetTextSize(0.03);
  tex->DrawLatex(200,900,"Internal");
  tex->DrawLatex(150,850,"#sqrt{s} = 13 TeV, 139.0 fb^{-1}");
  tex->DrawLatex(150,800,"all channel combined");
  tex->DrawLatex(150,750,"m_{Zd} = 70 GeV Included");
  
  //gr_integral_reco_mZd->Draw("acp");
  gr_integral_reco_mZd->SetMarkerSize(1);
  gr_integral_reco_mZd->SetMarkerStyle(8);
  auto legend1 = new TLegend(0.2,0.7,0.3,0.8);
  legend1->SetTextFont(42);
  legend1->SetTextSize(0.03);
  legend1->SetBorderSize(0);
  legend1->AddEntry(gr_integral_reco_mZd,"Integral from reco","lf");
  thePad->RedrawAxis();
  //legend1->AddEntry(gr_integral_mZd,"Integral from fit","lf");
  //legend1->Draw();
 
 

  
      if (i_Nwsyst ==0)
	{
	  thePad->Print(TString::Format("plots/hist_integral_nominal_%s.pdf", i_name[channel]));
	}
      else if ((i_Nwsyst !=0) && (i_kSyst==-1))
	{
	  thePad->Print(TString::Format("plots/hist_integral_%s_%s_%s.pdf", i_name[channel],c_wSyst[i_Nwsyst],c_vSyst[i_var]));
	}
       else
	{
	  thePad->Print(TString::Format("plots/hist_integral_%s_%s_%s.pdf",i_name[channel],c_kSyst[i_kSyst],c_vSyst[i_var]));
	}
  
    for (int i =0; i<18; i++)
    {
      //count_gen_mZd[i] = std::round(gr_integral_mZd->Eval(mZd_gen[i]));
      count_gen_mZd[i] = std::round(gr_integral_reco_mZd->Eval(mZd_gen[i]));
    
    }
      //cout << "count_gen_mZd[0]" << gr_integral_mZd->Eval(70) << endl;
    cout << "count_reco_mZd[0] = " << IntegralReco[0][channel] << endl;
    cout << "count_reco_mZd[2] = " << IntegralReco[2][channel] << endl;
    cout << "count_reco_mZd[3] = " << IntegralReco[3][channel] << endl;
    cout << "count_reco_mZd[5] = " << IntegralReco[5][channel] << endl;
    cout << "count_reco_mZd[6] = " << IntegralReco[6][channel] << endl;
    cout << "count_reco_mZd[7] = " << IntegralReco[7][channel] << endl;
    cout << "count_reco_mZd[8] = " << IntegralReco[8][channel] << endl;

    cout << "interpolated integral (70) " << gr_integral_reco_mZd->Eval(70) << endl;
    cout << "count_gen_mZd[0]" << count_gen_mZd[0] << endl;
    cout << "reco inegral" << IntegralReco[4][channel]  << endl;

    //cout << "count_fit_mZd[0] = " << Integral[0][channel] << endl;
    /* cout << "count_fit_mZd[2] = " << Integral[2][channel] << endl;
    cout << "count_fit_mZd[3] = " << Integral[3][channel] << endl;
    cout << "count_fit_mZd[5] = " << Integral[5][channel] << endl;
    cout << "count_fit_mZd[6] = " << Integral[6][channel] << endl;
    cout << "count_fit_mZd[7] = " << Integral[7][channel] << endl;
    cout << "count_fit_mZd[8] = " << Integral[8][channel] << endl;*/
    

}



void plot_integral_mZd_vs_mS()

{
  //TMultiGraph  *mg  = new TMultiGraph();
  //Double_t mass_mZd[6]  = {20,50,65,80,150,250};
  TH2D* hist_empty = new TH2D();
  Double_t mass_mZd[9]  = {20, 50, 50, 65, 70, 80, 100, 150, 250};
  Double_t mass_mS[9]  = {175, 175, 400, 750, 250, 325, 400, 550, 800};

   Double_t integral_reco_mZd[9]  = {IntegralReco[0][channel],IntegralReco[1][channel],IntegralReco[2][channel],IntegralReco[3][channel],IntegralReco[4][channel],IntegralReco[5][channel],IntegralReco[6][channel],IntegralReco[7][channel],IntegralReco[8][channel]};

   Double_t integral_reco_error_mZd[9]  = {error_integral[0][channel],error_integral[1][channel],error_integral[2][channel],error_integral[3][channel],error_integral[4][channel],error_integral[5][channel],error_integral[6][channel],error_integral[7][channel],error_integral[8][channel]};

   // Double_t integral_reco_mS[9]  = {IntegralM4lReco[0][channel],IntegralM4lReco[1][channel],IntegralM4lReco[2][channel], IntegralM4lReco[3][channel], IntegralM4lReco[4][channel], IntegralM4lReco[5][channel],IntegralM4lReco[6][channel],IntegralM4lReco[7][channel],IntegralM4lReco[8][channel]};

  //Double_t integral_reco_error_mS[9]  = {error_integral_m4l[0][channel],error_integral_m4l[1][channel],error_integral_m4l[2][channel],error_integral_m4l[3][channel],error_integral_m4l[4][channel],error_integral_m4l[5][channel],error_integral_m4l[6][channel],error_integral_m4l[7][channel],error_integral_m4l[8][channel]};
  
  //TGraph2DErrors *gr_integral_reco_mZd_mS = new TGraph2DErrors(9, mass_mZd, mass_mS, integral_reco_mZd, 0, 0, integral_reco_error_mZd);
  TGraph2D *gr_integral_reco_mZd_mS = new TGraph2D(9, mass_mZd, mass_mS, integral_reco_mZd);
  
  // TSpline3 *r_mZd = new TSpline3("grmZd",gr_integral_reco_mZd);
  //gr_integral_reco_mZd_mS->SetTitle("");
  gr_integral_reco_mZd_mS->SetTitle("; m_{Zd}; m_{S}; integral");
  gr_integral_reco_mZd_mS->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_integral_reco_mZd_mS->GetYaxis()->SetTitle("m_{S}  [GeV]");
  gr_integral_reco_mZd_mS->GetZaxis()->SetTitle("integral");
  gr_integral_reco_mZd_mS->GetZaxis()->SetRangeUser(0, 1000);
  gr_integral_reco_mZd_mS->GetYaxis()->SetRangeUser(100, 900);
  gr_integral_reco_mZd_mS->GetXaxis()->SetTitleOffset(2);
  gr_integral_reco_mZd_mS->GetYaxis()->SetTitleOffset(2);
  gr_integral_reco_mZd_mS->GetZaxis()->SetTitleOffset(1.5);
  gr_integral_reco_mZd_mS->SetMarkerSize(1);
  gr_integral_reco_mZd_mS->SetMarkerStyle(8);
  // hist_empty->Draw("colz");
  // thePad->GetPadLeftMargin();
   gr_integral_reco_mZd_mS->Draw("colz");
   
    c1->SetRightMargin(0.15);
 
   //c1->SetLogz();

  cout << " (mZd, mS) = (20; 175)  GeV "<< IntegralReco[0][channel] << endl;
  cout << " (mZd, mS) = (50; 175)  GeV "  << IntegralReco[1][channel] << endl;
  cout << " (mZd, mS) = (50; 400)  GeV "  << IntegralReco[2][channel] << endl;
  cout << " (mZd, mS) = (65; 750)  GeV "  << IntegralReco[3][channel] << endl;
  cout << " (mZd, mS) = (70; 250)  GeV "  << IntegralReco[4][channel] << endl;
  cout << " (mZd, mS) = (80; 325)  GeV "  << IntegralReco[5][channel] << endl;
  cout << " (mZd, mS) = (100; 400) GeV " << IntegralReco[6][channel] << endl;
  cout << " (mZd, mS) = (150; 750) GeV " << IntegralReco[7][channel] << endl;
  cout << " (mZd, mS) = (250; 800) GeV " << IntegralReco[8][channel] << endl;
  
  cout << "*************************" << endl; 
  cout << " (mZd, mS) = (20; 175)  GeV "  << IntegralM4lReco[0][channel] << endl;
  cout << " (mZd, mS) = (50; 175)  GeV "  << IntegralM4lReco[1][channel] << endl;
  cout << " (mZd, mS) = (50; 400)  GeV "  << IntegralM4lReco[2][channel] << endl;
  cout << " (mZd, mS) = (65; 750)  GeV "  << IntegralM4lReco[3][channel] << endl;
  cout << " (mZd, mS) = (70; 250)  GeV "  << IntegralM4lReco[4][channel] << endl;
  cout << " (mZd, mS) = (80; 325)  GeV "  << IntegralM4lReco[5][channel] << endl;
  cout << " (mZd, mS) = (100; 400) GeV " << IntegralM4lReco[6][channel] << endl;
  cout << " (mZd, mS) = (150; 750) GeV " << IntegralM4lReco[7][channel] << endl;
  cout << " (mZd, mS) = (250; 800) GeV " << IntegralM4lReco[8][channel] << endl;
  //thePad->Print(TString::Format("plots/hist_integral_mZd_vs_mS_nominal_%s.pdf", i_name[channel]));

  /* TBox *box3 = new TBox(89,200,99,700);
  box3->SetLineColor(kBlack);
  box3->SetFillStyle(3354);
  box3->SetFillColor(kGray+2);
  box3->SetLineWidth(603);
  TBox *box2 = new TBox(89,200,99,700);
  box2->SetFillColor(0);
  box2->SetLineWidth(603);
  box2->Draw("F");
  box3->Draw("F");
  thePad->SetTickx();
  thePad->SetTicky();

  TLatex *tex = new TLatex();
  tex->SetTextFont(72);
  tex->SetTextSize(0.03);
  tex->DrawLatex(150,660,"ATLAS");
  tex->SetTextFont(42);
  tex->SetTextSize(0.03);
  tex->DrawLatex(200,660,"Internal");
  tex->DrawLatex(150,630,"#sqrt{s} = 13 TeV, 139.0 fb^{-1}");
  tex->DrawLatex(150,600,"all channel combined");
  tex->DrawLatex(150,570,"m_{Zd} = 70 GeV Excluded");
  
  //gr_integral_reco_mZd->Draw("acp");
  gr_integral_reco_mZd->SetMarkerSize(1);
  //gr_integral_reco_mZd->SetMarkerStyle(8);
  auto legend1 = new TLegend(0.2,0.7,0.3,0.8);
  legend1->SetTextFont(42);
  legend1->SetTextSize(0.03);
  legend1->SetBorderSize(0);
  legend1->AddEntry(gr_integral_reco_mZd,"Integral from reco","lf");
  thePad->RedrawAxis();*/
  //legend1->AddEntry(gr_integral_mZd,"Integral from fit","lf");
  //legend1->Draw();

 

  
   if (i_Nwsyst ==0)
	{
	  thePad->Print(TString::Format("plots/hist_integral_mZd_vs_mS_nominal_%s.pdf", i_name[channel]));
	}
      else if ((i_Nwsyst !=0) && (i_kSyst==-1))
	{
	  thePad->Print(TString::Format("plots/hist_integral_%s_%s_%s.pdf", i_name[channel],c_wSyst[i_Nwsyst],c_vSyst[i_var]));
	}
       else
	{
	  thePad->Print(TString::Format("plots/hist_integral_%s_%s_%s.pdf",i_name[channel],c_kSyst[i_kSyst],c_vSyst[i_var]));
	}
  

    

}

void graph_sigma_mZd()

{
  // c1->Divide(2,1);
  c1->SetLogy(0);
  Double_t mass_mZd[6]  = {20,50,65,80,150,250};
  Double_t sigma_mZd[6]  = {Sigma[0][channel],Sigma[2][channel],Sigma[3][channel],Sigma[5][channel],Sigma[7][channel],Sigma[8][channel]};
  Double_t sigma_error_mZd[6] = {err_Sigma[0][channel],err_Sigma[2][channel],err_Sigma[3][channel],err_Sigma[5][channel], err_Sigma[7][channel],err_Sigma[8][channel]};
  TGraphErrors *gr_sigma_mZd = new TGraphErrors(6,mass_mZd,sigma_mZd,0,sigma_error_mZd);
  TSpline3 *s_mZd = new TSpline3("grmZd",gr_sigma_mZd);
  
  gr_sigma_mZd->SetTitle("");
  gr_sigma_mZd->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_sigma_mZd->GetYaxis()->SetTitle("Fit param #sigma");
  // c1->cd(1);
  gr_sigma_mZd->Fit("pol2","","",20,255);
  gr_sigma_mZd->GetXaxis()->SetRangeUser(0, 300);
  gr_sigma_mZd->GetYaxis()->SetRangeUser(0, 5);
  gr_sigma_mZd->SetMarkerSize(5);
  gr_sigma_mZd->Draw("APE");
  gr_sigma_mZd->SetMarkerSize(1);
  gr_sigma_mZd->SetMarkerStyle(8);
  s_mZd->SetLineColor(kRed);
 
if (i_Nwsyst ==0)
	{
	  thePad->Print(TString::Format("plots/hist_sigma_nominal_%s.pdf", i_name[channel]));
	}
      else if ((i_Nwsyst !=0) && (i_kSyst==-1))
	{
	  thePad->Print(TString::Format("plots/hist_sigma_%s_%s_%s.pdf", i_name[channel],c_wSyst[i_Nwsyst],c_vSyst[i_var]));
	}
       else
	{
	  thePad->Print(TString::Format("plots/hist_sigma_%s_%s_%s.pdf", i_name[channel],c_kSyst[i_kSyst],c_vSyst[i_var]));
	}

  
    for (int i =0; i<18; i++)
    {
           sigma_gen_mZd[i] = gr_sigma_mZd->GetFunction("pol2")->Eval(mZd_gen[i]);
	   //cout << "*********sigma_gen_mZd[0]******* = " << sigma_gen_mZd[0] << endl;
    }
    //cout << "*********sigma_gen_mZd[0]******* = " << sigma_gen_mZd[0] << endl;
}
void graph_mean_mZd()
{

Double_t mass_mZd[6]  = {20,50,65,80,150,250};
  Double_t mean_mZd[6]  = {Mean[0][channel],Mean[2][channel],Mean[3][channel],Mean[5][channel],Mean[7][channel],Mean[8][channel]};
  Double_t mean_error_mZd[6] = {err_Mean[0][channel],err_Mean[2][channel],err_Mean[3][channel],err_Mean[5][channel],err_Mean[7][channel],err_Mean[8][channel]};
  TGraphErrors *gr_mean_mZd = new TGraphErrors(6,mass_mZd,mean_mZd,0,mean_error_mZd);
   
  gr_mean_mZd->SetTitle("");
  gr_mean_mZd->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_mean_mZd->GetYaxis()->SetTitle("Fit param #mu");
  // c1->cd(1);
  gr_mean_mZd->Fit("pol1","","",20,250);
  gr_mean_mZd->GetXaxis()->SetRangeUser(0, 300);
  gr_mean_mZd->GetYaxis()->SetRangeUser(0, 400);
  gr_mean_mZd->Draw("APE");
  gr_mean_mZd->SetMarkerSize(1);
  gr_mean_mZd->SetMarkerStyle(8);
 


if (i_Nwsyst ==0)
	{
	  thePad->Print(TString::Format("plots/hist_mean_nominal_%s.pdf", i_name[channel]));
	}
      else if ((i_Nwsyst !=0) && (i_kSyst==-1))
	{
	  thePad->Print(TString::Format("plots/hist_mean_%s_%s_%s.pdf", i_name[channel],c_wSyst[i_Nwsyst],c_vSyst[i_var]));
	}
       else
	{
	  thePad->Print(TString::Format("plots/hist_mean_%s_%s_%s.pdf", i_name[channel],c_kSyst[i_kSyst],c_vSyst[i_var]));
	}

  
    for (int i =0; i<18; i++)
    {
           mean_gen_mZd[i]  = gr_mean_mZd->GetFunction("pol1")->Eval(mZd_gen[i]);
	   cout << "*********mean_gen_mZd[0]******** =  " << mean_gen_mZd[0] << endl;
    }
   
}
void compare_mZdGen_mZdReco()
{
  c1->SetLogy(0);
  /* double scale = 1/hGen_mZd[0][channel]->Integral();
  hGen_mZd[0][channel]->Scale(scale);
  double scale70 = 1/hist_avgM_nom[4][channel]->Integral();
  hist_avgM_nom[4][channel]->Scale(scale70);*/
  hGen_mZd[0][channel]->GetXaxis()->SetRangeUser(0,140);
  hist_avgM_nom[4][channel]->GetXaxis()->SetRangeUser(0,140);
  hGen_mZd[0][channel]->SetLineColor(2);
  hist_avgM_nom[4][channel]->SetLineColor(4);
  c1->Clear();
  gStyle->SetOptFit(0);
  gStyle->SetOptStat(0);
  // put tick marks on top and RHS of plots
  thePad->SetTickx();
  thePad->SetTicky();
  hist_avgM_nom[4][channel]->SetTitle("");
 
  hGen_mZd[0][channel]->Draw("hist");
   hist_avgM_nom[4][channel]->Draw("hist same");
 
 
  gPad->Update();
  auto legend = new TLegend(0.2,0.5,0.3,0.8);
  legend->SetTextFont(42);
  legend->SetTextSize(0.03);
  legend->SetBorderSize(0);
  legend->AddEntry(hist_avgM_nom[4][channel],"reco signal","lf");
  legend->AddEntry(hGen_mZd[0][channel],"Generated signal","lf");
  legend->SetX1NDC(.5); //new x start position
  legend->SetX2NDC(.7);
  legend->Draw();


  /*  TLatex *tex = new TLatex();
  tex->SetTextFont(72);
  tex->SetTextSize(0.03);
  tex->DrawLatex(80,100,"ATLAS");
  tex->SetTextFont(42);
  tex->SetTextSize(0.03);
  tex->DrawLatex(100,100,"Internal");
  tex->DrawLatex(80,90,"#sqrt{s} = 13 TeV, 139.0 fb^{-1}");
  tex->DrawLatex(80,80,"all channel combined");*/

   TLatex *tex = new TLatex();
  tex->SetTextFont(72);
  tex->SetTextSize(0.03);
  tex->DrawLatex(80,240,"ATLAS");
  tex->SetTextFont(42);
  tex->SetTextSize(0.03);
  tex->DrawLatex(100,240,"Internal");
  tex->DrawLatex(80,220,"#sqrt{s} = 13 TeV, 139.0 fb^{-1}");
  tex->DrawLatex(80,195,"all channel combined");
  // tex->DrawLatex(80,70,"m_{Zd} = 70 GeV Included");
  thePad->RedrawAxis();


  if (i_Nwsyst ==0)
    {
      thePad->Print(TString::Format("plots/hist_hGen_mZd_70_nominal_%s.pdf", i_name[channel]));
    }
  else if ((i_Nwsyst !=0) && (i_kSyst==-1))
    {
      thePad->Print(TString::Format("plots/hist_hGen_mZd_70_%s_%s_%s.pdf", i_name[channel],c_wSyst[i_Nwsyst],c_vSyst[i_var]));
    }
  else
    {
      thePad->Print(TString::Format("plots/hist_hGen_mZd_70_%s_%s_%s.pdf", i_name[channel],c_kSyst[i_kSyst],c_vSyst[i_var]));
	}
  
}

void SaveInNomDirect()

{

 for (int i_gen = 0; i_gen < 18; i_gen++)
	{
	  QCD_scale_up[i_gen][channel]   = (count_gen_mZd[i_gen] + count_gen_mZd[i_gen]*QCD_uncert_up[i_gen])/count_gen_mZd[i_gen];
	  QCD_scale_down[i_gen][channel] = (count_gen_mZd[i_gen] + count_gen_mZd[i_gen]*QCD_uncert_down[i_gen])/count_gen_mZd[i_gen];
	  
	  PDF_scale_up[i_gen][channel]   = (count_gen_mZd[i_gen] + count_gen_mZd[i_gen]*PDF_uncert_up[i_gen])/count_gen_mZd[i_gen];
	  PDF_scale_down[i_gen][channel] = (count_gen_mZd[i_gen] + count_gen_mZd[i_gen]*PDF_uncert_down[i_gen])/count_gen_mZd[i_gen];
	  
	  file_saveFitHist =  new TFile(TString::Format("BRscaled_gaussiansignal_%schannel_%s.root",i_name[channel], c_mZdmS[i_gen]),"recreate");
	  sig = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig->SetName("sig");
	  sig_up = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig_up->SetName("sig_up");
	  sig_down = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig_down->SetName("sig_down");
	  myDirect = file_saveFitHist->mkdir("Nominal");
	  file_saveFitHist->cd("Nominal");
	  sig->Write();
	  myDirect = file_saveFitHist->mkdir("STAT");
	  file_saveFitHist->cd("STAT");
	  sig->Write();
	  myDirect = file_saveFitHist->mkdir("LUMI");
	  file_saveFitHist->cd("LUMI");
	  sig_up->Write();
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("QCDSCALE");
	  file_saveFitHist->cd("QCDSCALE");
	  sig_up->Scale(QCD_scale_up[i_gen][channel]);
	  sig_up->Write();
	  sig_down->Scale(QCD_scale_down[i_gen][channel]);
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("PDF");
	  file_saveFitHist->cd("PDF");
	  sig_up->Scale(PDF_scale_up[i_gen][channel]/QCD_scale_up[i_gen][channel]);
	  sig_up->Write();
	  sig_down->Scale(PDF_scale_down[i_gen][channel]/QCD_scale_up[i_gen][channel]);
	  sig_down->Write();
	  sig->Reset();
	  sig_up->Reset();
	  sig_down->Reset();
	  file_saveFitHist->Close();
	  
	}
     
}

void SaveOtherSystDirect()
{
for (int i_gen = 0; i_gen < 18; i_gen++)
	{

	  
	  file_saveFitHist =  new TFile(TString::Format("BRscaled_gaussiansignal_%schannel_%s.root",i_name[channel], c_mZdmS[i_gen]),"update");
	  if (i_var ==0)  myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_wSyst[i_Nwsyst]));
	  else file_saveFitHist->cd(TString::Format("%s",c_wSyst[i_Nwsyst])); 
	  // for (int i_var = 0; i_var < 2; i_var++)
	    // {
	  sig = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig->SetName(TString::Format("sig_%s",c_vSyst[i_var]));
	  file_saveFitHist->cd(TString::Format("%s",c_wSyst[i_Nwsyst]));
	  sig->Write();
	  sig->Reset();
	  // }
	  file_saveFitHist->Close();
	}
}
