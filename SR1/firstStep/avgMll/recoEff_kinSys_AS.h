//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun 18 16:24:01 2019 by ROOT version 5.34/25
// from TTree myTree/myTree
// found on file: file.root
//////////////////////////////////////////////////////////

#ifndef recoEff_kinSys_AS_h
#define recoEff_kinSys_AS_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TSelector.h>
#include <TLorentzVector.h>
#include <TCanvas.h>
// Header file for the classes stored in the TTree if any.
//#include "/var/build/72a/x86_64-slc6-gcc48-opt-build/projects/ROOT-5.34.25/src/ROOT/5.34.25/math/physics/inc/TLorentzVector.h"

// Fixed size dimensions of array or collections stored in the TTree if any.

class recoEff_kinSys_AS : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
   TFile* fOut;
   TH1F* havgM[4];
  TH1F* hCutflow[4];
  double DatasetID[36];
  double mS[36];
  double mZd[36];
  double widthS[36];
  double brSZZ[36];
  double XS[36];
  Int_t j_mc=-11111;


   TLorentzVector  *lep12;
   TLorentzVector  *lep34;
   TLorentzVector  *lep32;
   TLorentzVector  *lep14;
   TLorentzVector  *lep1234;
   TLorentzVector  *lep1;
   TLorentzVector  *lep2;
   TLorentzVector  *lep3;
   TLorentzVector  *lep4;
   Int_t           IdLep1;
   Int_t           IdLep2;
   Int_t           IdLep3;
   Int_t           IdLep4;
   Double_t        EvtWeight;
   Int_t           llll_pdgIdSum;
   ULong64_t       eventNumber;
   Int_t           RunNumber;
   Int_t           MC_channel_number;
   Double_t        Events_all;
   Bool_t          Q_Veto;
   Bool_t          LowMassVeto;
   Bool_t          HWinHM;
   Bool_t          ZVeto;
   Bool_t          LooseSR_HM;
   Bool_t          MediumSR;
   Bool_t          ZVR1HM;
   Bool_t          ZVR2HM;
   Double_t        min_SF_dR;
   Double_t        min_OF_dR;
   unsigned long long pre_trigger;
   Int_t           l_isIsolFixedCutLoose;
   Double_t        max_el_d0Sig;
   Double_t        max_mu_d0Sig;
   Bool_t          Electron_ID;
   bool Nominal;
  bool mc16a;
  bool mc16d;
  bool mc16e;
  double lumi;


    
   
    Double_t myFunc(Double_t q)
   {
     // local variable declaration
     Double_t result = -1111111111111;
     Double_t h = 3.72817e+00; 
     Double_t Y = 5.16317e+01; 
     Double_t S = 1.66099e+01 ; 
     Double_t B1 = -2.61777e+00 ; 
     Double_t B2 =  -2.66119e-02; 
     Double_t T = 6.39341e+00;
   
     
     if (q < Y-T)
       {
	 result = B1+B2*(q-Y)+h*exp((T*(2*q-2*Y+T))/(2*pow(S,2)));
       }
		   
     else if (q > Y-T) 
       result = B1+B2*(q-Y)+h*exp((-pow((q-Y),2))/(2*pow(S,2)));
     
     return result; 
   }
   void cutFlow(TH1F* hist0, TH1F* hist1, double weight, double Mll, bool cut1, bool cut2, bool cut3, bool cut4, bool cut5, bool cut6)
    {
      hist0->Fill(1,weight);
      if (cut1)
	{
	  hist0->Fill(2,weight);
	  if (cut2)
	    {
	      hist0->Fill(3,weight);
	      if (cut3)
		{
		  hist0->Fill(4,weight);
		  if (cut4)
		    {
		      hist0->Fill(5,weight);
		      if(cut5)
			{
			  hist0->Fill(6,weight);
			  {
			    if (cut6)
			      {
				hist0->Fill(7,weight);
				hist1->Fill(Mll,weight);
			      }
			  }
			}
		    }
		}
	    }
	}
    }

    
   // List of branches
   TBranch        *b_lep12;   //!
   TBranch        *b_lep34;   //!
   TBranch        *b_lep32;   //!
   TBranch        *b_lep14;   //!
   TBranch        *b_lep1234;   //!
   TBranch        *b_lep1;   //!
   TBranch        *b_lep2;   //!
   TBranch        *b_lep3;   //!
   TBranch        *b_lep4;   //!
   TBranch        *b_IdLep1;   //!
   TBranch        *b_IdLep2;   //!
   TBranch        *b_IdLep3;   //!
   TBranch        *b_IdLep4;   //!
   TBranch        *b_EvtWeight;   //!
   TBranch        *b_llll_pdgIdSum;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_MC_channel_number;   //!
   TBranch        *b_Events_all;   //!
   TBranch        *b_Q_Veto;   //!
   TBranch        *b_LowMassVeto;   //!
   TBranch        *b_HWinHM;   //!
   TBranch        *b_ZVeto;   //!
   TBranch        *b_LooseSR_HM;   //!
   TBranch        *b_MediumSR;   //!
   TBranch        *b_ZVR1HM;   //!
   TBranch        *b_ZVR2HM;   //!
   TBranch        *b_min_SF_dR;   //!
   TBranch        *b_min_OF_dR;   //!
   TBranch        *b_pre_trigger;   //!
   TBranch        *b_l_isIsolFixedCutLoose;   //!
   TBranch        *b_max_el_d0Sig;   //!
   TBranch        *b_max_mu_d0Sig;   //!
   TBranch        *b_Electron_ID;   //!
 
 
  
  

   recoEff_kinSys_AS(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~recoEff_kinSys_AS() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(recoEff_kinSys_AS,0);
};

#endif

#ifdef recoEff_kinSys_AS_cxx
void recoEff_kinSys_AS::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   lep12 = 0;
   lep34 = 0;
   lep32 = 0;
   lep14 = 0;
   lep1234 = 0;
   lep1 = 0;
   lep2 = 0;
   lep3 = 0;
   lep4 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("lep12", &lep12, &b_lep12);
   fChain->SetBranchAddress("lep34", &lep34, &b_lep34);
   fChain->SetBranchAddress("lep32", &lep32, &b_lep32);
   fChain->SetBranchAddress("lep14", &lep14, &b_lep14);
   fChain->SetBranchAddress("lep1234", &lep1234, &b_lep1234);
   fChain->SetBranchAddress("lep1", &lep1, &b_lep1);
   fChain->SetBranchAddress("lep2", &lep2, &b_lep2);
   fChain->SetBranchAddress("lep3", &lep3, &b_lep3);
   fChain->SetBranchAddress("lep4", &lep4, &b_lep4);
   fChain->SetBranchAddress("IdLep1", &IdLep1, &b_IdLep1);
   fChain->SetBranchAddress("IdLep2", &IdLep2, &b_IdLep2);
   fChain->SetBranchAddress("IdLep3", &IdLep3, &b_IdLep3);
   fChain->SetBranchAddress("IdLep4", &IdLep4, &b_IdLep4);
   fChain->SetBranchAddress("EvtWeight", &EvtWeight, &b_EvtWeight);
   fChain->SetBranchAddress("llll_pdgIdSum", &llll_pdgIdSum, &b_llll_pdgIdSum);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("MC_channel_number", &MC_channel_number, &b_MC_channel_number);
   fChain->SetBranchAddress("Events_all", &Events_all, &b_Events_all);
   fChain->SetBranchAddress("Q_Veto", &Q_Veto, &b_Q_Veto);
   fChain->SetBranchAddress("LowMassVeto", &LowMassVeto, &b_LowMassVeto);
   fChain->SetBranchAddress("HWinHM", &HWinHM, &b_HWinHM);
   fChain->SetBranchAddress("ZVeto", &ZVeto, &b_ZVeto);
   fChain->SetBranchAddress("LooseSR_HM", &LooseSR_HM, &b_LooseSR_HM);
   fChain->SetBranchAddress("MediumSR", &MediumSR, &b_MediumSR);
   fChain->SetBranchAddress("ZVR1HM", &ZVR1HM, &b_ZVR1HM);
   fChain->SetBranchAddress("ZVR2HM", &ZVR2HM, &b_ZVR2HM);
   fChain->SetBranchAddress("min_SF_dR", &min_SF_dR, &b_min_SF_dR);
   fChain->SetBranchAddress("min_OF_dR", &min_OF_dR, &b_min_OF_dR);
   fChain->SetBranchAddress("pre_trigger", &pre_trigger, &b_pre_trigger);
   fChain->SetBranchAddress("l_isIsolFixedCutLoose", &l_isIsolFixedCutLoose, &b_l_isIsolFixedCutLoose);
   fChain->SetBranchAddress("max_el_d0Sig", &max_el_d0Sig, &b_max_el_d0Sig);
   fChain->SetBranchAddress("max_mu_d0Sig", &max_mu_d0Sig, &b_max_mu_d0Sig);
   fChain->SetBranchAddress("Electron_ID", &Electron_ID, &b_Electron_ID);
  
  

}

Bool_t recoEff_kinSys_AS::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef recoEff_kinSys_AS_cxx
