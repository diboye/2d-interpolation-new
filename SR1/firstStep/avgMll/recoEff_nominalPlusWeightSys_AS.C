#define recoEff_nominalPlusWeightSys_AS_cxx
// The class definition in recoEff_nominalPlusWeightSys_AS.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("recoEff_nominalPlusWeightSys_AS.C")
// Root > T->Process("recoEff_nominalPlusWeightSys_AS.C","some options")
// Root > T->Process("recoEff_nominalPlusWeightSys_AS.C+")
//

#include "recoEff_nominalPlusWeightSys_AS.h"
#include <TH2.h>
#include <TH1.h>
#include <TStyle.h>
#include <iostream>
#include <fstream>
using namespace std;


void recoEff_nominalPlusWeightSys_AS::Begin(TTree * /*tree*/)
{
 

   TString option = GetOption();
    TString file = GetOption();
if (file.IsNull()) file="signal.root";
  else {
    Ssiz_t pos = file.Last('.');
    if (pos<0) pos=file.Length();
    file.Replace(pos,20,".root");
  }
   fOut = TFile::Open(file, "RECREATE");
     // style


  
   if (!file.SubString("mc16a").IsNull()) 
     {
       mc16a = true;
       mc16d = false;
       mc16e = false;
     }
   else if (!file.SubString("mc16d").IsNull())
     {
       mc16a = false;
       mc16d = true;
       mc16e = false;
     }

   else if (!file.SubString("mc16e").IsNull())
     {
       mc16a = false;
       mc16d = false;
       mc16e = true;
     }
   
   const int j_number[] =
     {
       451339,
       451340,
       451341,
       451342,
       451343,
       451344,
       451345,
       451346,
       451837,
       451838,
       451839,
       451840,
       451841,
       451842,
       451843,
       451844,
       451845,
       451846,
       451847,
       451848,
       451849,
       451850,
       451851,
       451852,
       451853,
       451854,
       451855,
       451856,
       451857,
       451858,
       451859,
       451860,
       451861,
       451862
     };
   
   
  
   TString file_input[]={"./signal_input.dat"};
   TString tmps;
   ifstream fin(file_input[0]);
   int  Npoints = 32;
   fin>>tmps;fin>>tmps;fin>>tmps;fin>>tmps;fin>>tmps;fin>>tmps;//read first line.
   
   for (Int_t i=0; i<Npoints; i++)
     {
       
       fin>>tmps;DatasetID[i]=tmps.IsFloat()?tmps.Atof():0;
       fin>>tmps; mS[i]=tmps.IsFloat()?tmps.Atof():0;
       fin>>tmps; mZd[i]=tmps.IsFloat()?tmps.Atof():0;
       fin>>tmps; widthS[i]=tmps.IsFloat()?tmps.Atof():0;
       fin>>tmps; brSZZ[i]=tmps.IsFloat()?tmps.Atof():0;
       fin>>tmps; XS[i]=tmps.IsFloat()?tmps.Atof():0;
     }

   cout <<mS[0] << " "  << mS[1] << " " << mS[2]  << " " << mS[3] << " XS "<< XS[0] << endl;
   
   cout << "mc16a " << mc16a << endl;
   cout << "mc16d " << mc16d << endl;
   cout << "mc16e " << mc16e << endl;
   const char *i_name[] = {"4e", "2e2m", "4m", "all"};
   const char *i_sysVar[] = {"up", "down"};

     for (int i = 0; i < 4; i++)
    {
      
      havgM[i] = new TH1F(TString::Format("havgM_%s", i_name[i]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
      MyHistArrayNom->AddLast(havgM[i]);
      
      hEventsAll[i] = new TH1F(TString::Format("hEventsAll_%s", i_name[i]), "4e channel;cut;events",1,0.5,1.5);
      TAxis* ax_EventsAll = hEventsAll[i]->GetXaxis();
      ax_EventsAll->SetBinLabel(1,"4e");
      MyHistArrayNom->AddLast(hEventsAll[i]);
      
      
      hCutflow[i] = new TH1F(TString::Format("hCutflow_%s", i_name[i]), "all channel;cut;events",7,0.5,7.5);
      TAxis* ax_Cutflow = hCutflow[i]->GetXaxis();
      ax_Cutflow->SetBinLabel(1,"all");
      ax_Cutflow->SetBinLabel(2,"LowMassVeto");
      ax_Cutflow->SetBinLabel(3,"ZVeto");
      ax_Cutflow->SetBinLabel(4,"LooseSR");
      ax_Cutflow->SetBinLabel(5,"HWinHS");
      ax_Cutflow->SetBinLabel(6,"MediumSR");
      ax_Cutflow->SetBinLabel(7,"TightSR");
      MyHistArrayNom->AddLast(hCutflow[i]);
      
      hCutflow_WeightedSyst[i] = new TH1F(TString::Format("hCutflow_WeightedSyst_%s", i_name[i]), "all channel;cut;events",25,0.5,25.5);
      TAxis *a = hCutflow_WeightedSyst[i]->GetXaxis();
      a->SetBinLabel(1, "Nominal");
      a->SetBinLabel(2, "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_up");
      a->SetBinLabel(3, "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_down");
      a->SetBinLabel(4, "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_up");
      a->SetBinLabel(5, "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_down");
      a->SetBinLabel(6, "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_up");
      a->SetBinLabel(7, "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_down");
      a->SetBinLabel(8, "MUON_EFF_ISO_STAT_up");
      a->SetBinLabel(9, "MUON_EFF_ISO_STAT_down");
      a->SetBinLabel(10, "MUON_EFF_ISO_SYS_up");
      a->SetBinLabel(11, "MUON_EFF_ISO_SYS_down");
      a->SetBinLabel(12, "MUON_EFF_RECO_STAT_up");
      a->SetBinLabel(13, "MUON_EFF_RECO_STAT_down");
      a->SetBinLabel(14, "MUON_EFF_RECO_STAT_LOWPT_up");
      a->SetBinLabel(15, "MUON_EFF_RECO_STAT_LOWPT_down");
      a->SetBinLabel(16, "MUON_EFF_RECO_SYS_up");
      a->SetBinLabel(17, "MUON_EFF_RECO_SYS_down");
      a->SetBinLabel(18, "MUON_EFF_RECO_SYS_LOWPT_up");
      a->SetBinLabel(19, "MUON_EFF_RECO_SYS_LOWPT_down");
      a->SetBinLabel(20, "MUON_EFF_TTVA_STAT_up");
      a->SetBinLabel(21, "MUON_EFF_TTVA_STAT_down");
      a->SetBinLabel(22, "MUON_EFF_TTVA_SYS_up");
      a->SetBinLabel(23, "MUON_EFF_TTVA_SYS_down");
      a->SetBinLabel(24, "PileUp_up");
      a->SetBinLabel(25, "PileUp_down");
      MyHistArrayNom->AddLast(hCutflow_WeightedSyst[i]);
     }
   
   
  for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < 4; j++)
	{
	  havgM_EL_EFF_ID_TOTAL[i][j] = new TH1F(TString::Format("havgM_EL_EFF_ID_TOTAL_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_EL_EFF_ID_TOTAL[i][j]);
	  havgM_EL_EFF_ISO_TOTAL[i][j] = new TH1F(TString::Format("havgM_EL_EFF_ISO_TOTAL_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_EL_EFF_ISO_TOTAL[i][j]);
	  havgM_EL_EFF_RECO_TOTAL[i][j] = new TH1F(TString::Format("havgM_EL_EFF_RECO_TOTAL_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_EL_EFF_RECO_TOTAL[i][j]);
	  havgM_MUON_EFF_ISO_STAT[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_ISO_STAT_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_ISO_STAT[i][j]);
	  havgM_MUON_EFF_ISO_SYS[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_ISO_SYS_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_ISO_SYS[i][j]);
	  havgM_MUON_EFF_RECO_STAT[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_RECO_STAT_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_RECO_STAT[i][j]);
	  havgM_MUON_EFF_RECO_STAT_LOWPT[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_RECO_STAT_LOWPT_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_RECO_STAT_LOWPT[i][j]);
	  havgM_MUON_EFF_RECO_SYS[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_RECO_SYS_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_RECO_SYS[i][j]);
	  havgM_MUON_EFF_RECO_SYS_LOWPT[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_RECO_SYS_LOWPT_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_RECO_SYS_LOWPT[i][j]);
	  havgM_MUON_EFF_TTVA_STAT[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_TTVA_STAT_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_TTVA_STAT[i][j]);
	  havgM_MUON_EFF_TTVA_SYS[i][j] = new TH1F(TString::Format("havgM_MUON_EFF_TTVA_SYS_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_MUON_EFF_TTVA_SYS[i][j]);
	  havgM_Pileup_weight[i][j] = new TH1F(TString::Format("havgM_Pileup_weight_%s_%s", i_sysVar[i], i_name[j]), "all;<M_{ll}> [GeV];entries / [1.0 GeV]",30,0,60);
	  MyHistArrayNom->AddLast(havgM_Pileup_weight[i][j]);
	
	}
    }
  
  

   
   
   
  
  
  
  
  //ax_Cutflow_4e->SetBinLabel(7,"TightSR");
  Double_t myFunc(Double_t q);
  void cutFlow(TH1F* hist0, TH1F* hist1, double weight, double Mll, bool cut1, bool cut2, bool cut3, bool cut4, bool cut5,bool cut6);

  void cutFlowSysWeight(TH1F*hist0,TH1F*hist1,TH1F* hist2,TH1F* hist3,TH1F* hist4,TH1F* hist5,TH1F* hist6,TH1F* hist7,TH1F* hist8,TH1F* hist9,TH1F* hist10,TH1F* hist11,TH1F* hist12,TH1F* hist13,TH1F* hist14,TH1F* hist15,TH1F* hist16,TH1F* hist17,TH1F* hist18,TH1F* hist19,TH1F* hist20,TH1F* hist21,TH1F* hist22,TH1F* hist23,TH1F* hist24,double Mll,double NomWeight,double SysWeight1,double SysWeight2,double SysWeight3,double SysWeight4,double SysWeight5,double SysWeight6,double SysWeight7,double SysWeight8,double SysWeight9,double SysWeight10,double SysWeight11,double SysWeight12,double SysWeight13,double SysWeight14,double SysWeight15,double SysWeight16,double SysWeight17,double SysWeight18,double SysWeight19,double SysWeight20,double SysWeight21,double SysWeight22,double SysWeight23,double SysWeight24,bool cut1,bool cut2,bool cut3,bool cut4,bool cut5,bool cut6);
}

  

void recoEff_nominalPlusWeightSys_AS::SlaveBegin(TTree * /*tree*/)
{

   TString option = GetOption();

}

Bool_t recoEff_nominalPlusWeightSys_AS::Process(Long64_t entry)
{
   fChain->GetEntry(entry);
   int Ncut = 1;
  
    TLorentzVector v4l = *lep1234;
   double M4l = lep1234->M() * 1e-3; // GeV
   TLorentzVector v12 = *lep12;
   TLorentzVector v34 = *lep34;
   TLorentzVector v14 = *lep14;
   TLorentzVector v32 = *lep32;

   double M12 = lep12->M() * 1e-3; // GeV
   double M34 = lep34->M() * 1e-3; // GeV
   double M14 = lep14->M() * 1e-3; // GeV
   double M32 = lep32->M() * 1e-3; // GeV
  

    double Mab, Mcd, Mcb, Mad;  // Mab > Mcd by definition
    TLorentzVector vAB, vCD, vAD, vCB;
   if (M12>M34) {
     Mab = M12;
     Mcd = M34;
     Mcb = M32;
     Mad = M14;
     vAB = v12;
     vCD = v34;
     vAD = v14;
     vCB = v32;
   } else {
     Mab = M34;
     Mcd = M12;
     Mcb = M14;
     Mad = M32;
     vAB = v34;
     vCD = v12;
     vAD = v32;
     vCB = v14;
   }
   

   if (MC_channel_number == 451339) j_mc = 0;
   else if (MC_channel_number == 451340) j_mc = 1;	 
   else if (MC_channel_number == 451341) j_mc = 2;	
   else if (MC_channel_number == 451342) j_mc = 3;	
   else if (MC_channel_number == 451343) j_mc = 4;	
   else if (MC_channel_number == 451344) j_mc = 5;	
   else if (MC_channel_number == 451345) j_mc = 6;	
   else if (MC_channel_number == 451346) j_mc = 7;
   else if (MC_channel_number == 451837) j_mc = 8;	 
   else if (MC_channel_number == 451838) j_mc = 9;	
   else if (MC_channel_number == 451839) j_mc = 10;	
   else if (MC_channel_number == 451840) j_mc = 11;	
   else if (MC_channel_number == 451841) j_mc = 12;	
   else if (MC_channel_number == 451842) j_mc = 13;	
   else if (MC_channel_number == 451843) j_mc = 14;
   else if (MC_channel_number == 451844) j_mc = 15;	 
   else if (MC_channel_number == 451845) j_mc = 16;	
   else if (MC_channel_number == 451846) j_mc = 17;	
   else if (MC_channel_number == 451847) j_mc = 18;	
   else if (MC_channel_number == 451848) j_mc = 19;	
   else if (MC_channel_number == 451849) j_mc = 20;	
   else if (MC_channel_number == 451850) j_mc = 21;
   else if (MC_channel_number == 451851) j_mc = 22;	 
   else if (MC_channel_number == 451852) j_mc = 23;	
   else if (MC_channel_number == 451853) j_mc = 24;	
   else if (MC_channel_number == 451854) j_mc = 25;	
   else if (MC_channel_number == 451855) j_mc = 26;	
   else if (MC_channel_number == 451856) j_mc = 27;	
   else if (MC_channel_number == 451857) j_mc = 28;
   else if (MC_channel_number == 451858) j_mc = 29;	
   else if (MC_channel_number == 451859) j_mc = 30;	
   else if (MC_channel_number == 451860) j_mc = 31;	
   else if (MC_channel_number == 451861) j_mc = 32;
   else if (MC_channel_number == 451862) j_mc = 33;
   
   double avgM = (Mab+Mcd)/2;
   TVector3 beta = v4l.BoostVector(); // (x/t, y/t, z/t)
   TLorentzVector vABcmf = vAB;
   TLorentzVector vADcmf = vAD;
   vABcmf.Boost(-beta);
   vADcmf.Boost(-beta);
   double Eab_cmf = vABcmf.E();
   double Ead_cmf = vADcmf.E();
   double EoM4l_ab = Eab_cmf/lep1234->M();
   double EoM4l_ad = Eab_cmf/lep1234->M();
   bool low_mass_veto = Mab > 5. && Mcd > 5. && Mcb > 5. && Mad > 5.;
   bool c_ZVetoSR1 = (Mab < 50  ||  Mab  > 106);
   bool loose_SR = Mab > 10. && Mcd > 10;
   bool Medium = (fabs(1.0 - Mcd/Mab) < 0.15);
   bool tight = (fabs(EoM4l_ab-0.5) < 0.008);
   bool HWinHS_SR1 = M4l < 115;
   double leftCut[32];
   double rightCut[32];
   
   bool slice_mS[32];

      for (Int_t i=0; i<32; i++)
     {
       leftCut[i]= mS[i]-5;
       rightCut[i]  = mS[i]+5;
       slice_mS[i] =((M4l > leftCut[i])  && (M4l < rightCut[i]));

     }
      /*for (Int_t i=0; i<32; i++)
     {
       if((j_mc == i) && (!slice_mS[i])) return kTRUE;
     }*/





   //if((j_mc == 3) && (!slice_mS[3])) return kTRUE;
   
     
     if (llll_pdgIdSum != 44 && llll_pdgIdSum != 48 && llll_pdgIdSum != 52 ) return kTRUE;
     if(!Q_Veto) return kTRUE;//Q veto
     if(l_isIsolFixedCutLoose!=15) return kTRUE;//isolation
     if (!Electron_ID) return kTRUE; //electron id
     if(!(max_el_d0Sig < 5 && max_mu_d0Sig < 3))return  kTRUE;//impact parameter
     
     double num = myFunc(Mab)-myFunc(10); 
     double denom = myFunc(49.6483)-myFunc(10);
     double modulFunc = 1-(num/denom);
     Double_t MassPointTotest = 0.0;
     if (Mab > 49.6483)  MassPointTotest = 0.85*Mab;
     else if (Mab <= 49.6483) MassPointTotest = 0.85*Mab - 0.1125*modulFunc*Mab;
     bool MediumSRWide = Mcd > MassPointTotest;
     if(l_isIsolFixedCutLoose!=15) return kTRUE;//isolation
     if (!Electron_ID) return kTRUE; //electron id
     if(!(max_el_d0Sig < 5 && max_mu_d0Sig < 3))return  kTRUE;//impact parameter
     if (llll_pdgIdSum ==44)
       
       {
	   cutFlow(hCutflow[0],
		   havgM[0],
		   EvtWeight,
		   avgM,
		   low_mass_veto,
		   c_ZVetoSR1,
		   loose_SR,
		   HWinHS_SR1,
		   MediumSRWide,
		   tight);

	   cutFlowSysWeight(hCutflow_WeightedSyst[0],
			    havgM_EL_EFF_ID_TOTAL[0][0],
			    havgM_EL_EFF_ID_TOTAL[1][0],
			    havgM_EL_EFF_ISO_TOTAL[0][0],
			    havgM_EL_EFF_ISO_TOTAL[1][0],
			    havgM_EL_EFF_RECO_TOTAL[0][0],
			    havgM_EL_EFF_RECO_TOTAL[1][0],
			    havgM_MUON_EFF_ISO_STAT[0][0],
			    havgM_MUON_EFF_ISO_STAT[1][0],
			    havgM_MUON_EFF_ISO_SYS[0][0],
			    havgM_MUON_EFF_ISO_SYS[1][0],
			    havgM_MUON_EFF_RECO_STAT[0][0],
			    havgM_MUON_EFF_RECO_STAT[1][0],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[0][0],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[1][0],
			    havgM_MUON_EFF_RECO_SYS[0][0],
			    havgM_MUON_EFF_RECO_SYS[1][0],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[0][0],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[1][0],
			    havgM_MUON_EFF_TTVA_STAT[0][0],
			    havgM_MUON_EFF_TTVA_STAT[1][0],
			    havgM_MUON_EFF_TTVA_SYS[0][0],
			    havgM_MUON_EFF_TTVA_SYS[1][0],
			    havgM_Pileup_weight[0][0],
			    havgM_Pileup_weight[1][0],
			    avgM,
			    EvtWeight,
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up,    
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down,  
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up,   
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down, 
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up,  
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down,
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1up,                    
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1down,                  
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1up,			   
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1down,		   
			    EvtWeight*Pileup_Weight_up/Pileup_Weight,		   
			    EvtWeight*Pileup_Weight_down/Pileup_Weight,		   
			    low_mass_veto,
			    c_ZVetoSR1,
			    loose_SR,
			    HWinHS_SR1,
			    MediumSRWide,
			    tight);
	   //cout << "EvtWeight*Pileup_Weight_up/Pileup_Weight" << EvtWeight*Pileup_Weight_up/Pileup_Weight << endl;
	   //cout << "evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up" << evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up << endl;
	 }
       
       
       if (llll_pdgIdSum ==48)
	 {
	   cutFlow(
		   hCutflow[1],
		   havgM[1],
		   EvtWeight,
		   avgM,
		   low_mass_veto,
		   c_ZVetoSR1,
		   loose_SR,
		   HWinHS_SR1,
		   MediumSRWide,
		   tight);
	   
	   cutFlowSysWeight(hCutflow_WeightedSyst[1],
			    havgM_EL_EFF_ID_TOTAL[0][1],
			    havgM_EL_EFF_ID_TOTAL[1][1],
			    havgM_EL_EFF_ISO_TOTAL[0][1],
			    havgM_EL_EFF_ISO_TOTAL[1][1],
			    havgM_EL_EFF_RECO_TOTAL[0][1],
			    havgM_EL_EFF_RECO_TOTAL[1][1],
			    havgM_MUON_EFF_ISO_STAT[0][1],
			    havgM_MUON_EFF_ISO_STAT[1][1],
			    havgM_MUON_EFF_ISO_SYS[0][1],
			    havgM_MUON_EFF_ISO_SYS[1][1],
			    havgM_MUON_EFF_RECO_STAT[0][1],
			    havgM_MUON_EFF_RECO_STAT[1][1],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[0][1],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[1][1],
			    havgM_MUON_EFF_RECO_SYS[0][1],
			    havgM_MUON_EFF_RECO_SYS[1][1],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[0][1],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[1][1],
			    havgM_MUON_EFF_TTVA_STAT[0][1],
			    havgM_MUON_EFF_TTVA_STAT[1][1],
			    havgM_MUON_EFF_TTVA_SYS[0][1],
			    havgM_MUON_EFF_TTVA_SYS[1][1],
			    havgM_Pileup_weight[0][1],
			    havgM_Pileup_weight[1][1],
			    avgM,
			    EvtWeight,
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up,    
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down,  
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up,   
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down, 
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up,  
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down,
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1up,                    
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1down,                  
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1up,			   
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1down,		   
			    EvtWeight*Pileup_Weight_up/Pileup_Weight,		   
			    EvtWeight*Pileup_Weight_down/Pileup_Weight,		   
			    low_mass_veto,
			    c_ZVetoSR1,
			    loose_SR,
			    HWinHS_SR1,
			    MediumSRWide,
			    tight);
	   
	   
	 }
       
       
       if (llll_pdgIdSum ==52)
	 {
	   cutFlow(hCutflow[2],
		   havgM[2],
		   EvtWeight,
		   avgM,
		   low_mass_veto,
		   c_ZVetoSR1,
		   loose_SR,
		   HWinHS_SR1,
		   MediumSRWide,
		   tight);
	   
	   cutFlowSysWeight(hCutflow_WeightedSyst[2],
			    havgM_EL_EFF_ID_TOTAL[0][2],
			    havgM_EL_EFF_ID_TOTAL[1][2],
			    havgM_EL_EFF_ISO_TOTAL[0][2],
			    havgM_EL_EFF_ISO_TOTAL[1][2],
			    havgM_EL_EFF_RECO_TOTAL[0][2],
			    havgM_EL_EFF_RECO_TOTAL[1][2],
			    havgM_MUON_EFF_ISO_STAT[0][2],
			    havgM_MUON_EFF_ISO_STAT[1][2],
			    havgM_MUON_EFF_ISO_SYS[0][2],
			    havgM_MUON_EFF_ISO_SYS[1][2],
			    havgM_MUON_EFF_RECO_STAT[0][2],
			    havgM_MUON_EFF_RECO_STAT[1][2],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[0][2],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[1][2],
			    havgM_MUON_EFF_RECO_SYS[0][2],
			    havgM_MUON_EFF_RECO_SYS[1][2],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[0][2],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[1][2],
			    havgM_MUON_EFF_TTVA_STAT[0][2],
			    havgM_MUON_EFF_TTVA_STAT[1][2],
			    havgM_MUON_EFF_TTVA_SYS[0][2],
			    havgM_MUON_EFF_TTVA_SYS[1][2],
			    havgM_Pileup_weight[0][2],
			    havgM_Pileup_weight[1][2],
			    avgM,
			    EvtWeight,
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up,    
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down,  
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up,   
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down, 
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up,  
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down,
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1up,                    
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1down,                  
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1up,			   
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1down,		   
			    EvtWeight*Pileup_Weight_up/Pileup_Weight,		   
			    EvtWeight*Pileup_Weight_down/Pileup_Weight,		   
			    low_mass_veto,
			    c_ZVetoSR1,
			    loose_SR,
			    HWinHS_SR1,
			    MediumSRWide,
			    tight);
	   
	 }
       
    cutFlow(hCutflow[3],
		   havgM[3],
		   EvtWeight,
		   avgM,
		   low_mass_veto,
		   c_ZVetoSR1,
		   loose_SR,
		   HWinHS_SR1,
		   MediumSRWide,
		   tight);

	   cutFlowSysWeight(hCutflow_WeightedSyst[3],
			    havgM_EL_EFF_ID_TOTAL[0][3],
			    havgM_EL_EFF_ID_TOTAL[1][3],
			    havgM_EL_EFF_ISO_TOTAL[0][3],
			    havgM_EL_EFF_ISO_TOTAL[1][3],
			    havgM_EL_EFF_RECO_TOTAL[0][3],
			    havgM_EL_EFF_RECO_TOTAL[1][3],
			    havgM_MUON_EFF_ISO_STAT[0][3],
			    havgM_MUON_EFF_ISO_STAT[1][3],
			    havgM_MUON_EFF_ISO_SYS[0][3],
			    havgM_MUON_EFF_ISO_SYS[1][3],
			    havgM_MUON_EFF_RECO_STAT[0][3],
			    havgM_MUON_EFF_RECO_STAT[1][3],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[0][3],
			    havgM_MUON_EFF_RECO_STAT_LOWPT[1][3],
			    havgM_MUON_EFF_RECO_SYS[0][3],
			    havgM_MUON_EFF_RECO_SYS[1][3],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[0][3],
			    havgM_MUON_EFF_RECO_SYS_LOWPT[1][3],
			    havgM_MUON_EFF_TTVA_STAT[0][3],
			    havgM_MUON_EFF_TTVA_STAT[1][3],
			    havgM_MUON_EFF_TTVA_SYS[0][3],
			    havgM_MUON_EFF_TTVA_SYS[1][3],
			    havgM_Pileup_weight[0][3],
			    havgM_Pileup_weight[1][3],
			    avgM,
			    EvtWeight,
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up,    
			    evtWeightNoSF*EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down,  
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up,   
			    evtWeightNoSF*EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down, 
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up,  
			    evtWeightNoSF*EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down,
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1up,                    
			    evtWeightNoSF*MUON_EFF_ISO_STAT_1down,                  
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1up,			   
			    evtWeightNoSF*MUON_EFF_ISO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_STAT_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_1down,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1up,		   
			    evtWeightNoSF*MUON_EFF_RECO_SYS_LOWPT_1down,	   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_STAT_1down,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1up,		   
			    evtWeightNoSF*MUON_EFF_TTVA_SYS_1down,		   
			    EvtWeight*Pileup_Weight_up/Pileup_Weight,		   
			    EvtWeight*Pileup_Weight_down/Pileup_Weight,		   
			    low_mass_veto,
			    c_ZVetoSR1,
			    loose_SR,
			    HWinHS_SR1,
			    MediumSRWide,
			    tight);
	  

   
    
   return kTRUE;
}

void recoEff_nominalPlusWeightSys_AS::SlaveTerminate()
{


}

void recoEff_nominalPlusWeightSys_AS::Terminate()

{
  if(mc16a) lumi  = 36.2077e03;//pb-1
  else if(mc16d) lumi  = 44.3074e03;//pb-1
  else if (mc16e) lumi  = 58.4e03;//pb-1
  
  TCanvas *c1 = new TCanvas("c1","c1");
  TPad* thePad = (TPad*)c1->cd();
  c1->SetRightMargin(0.18);
  // gStyle->SetPalette(55);
  // c1->SetLogz();
  // c1->SetLogy();
   cout << "Events_all" << Events_all << endl;
   double Cross_sect=-111111;
  double BrSZZ=-1111111;
  double BrZZ4l_all = 0.0044;


 
   double kfactor = 1.0;
  double genFiltEff = 1.0;
  
  double  Normar_all= (kfactor*genFiltEff*lumi)/Events_all;
  //double  Normar_all= (XS[j_mc]*kfactor*genFiltEff*lumi)/Events_all;
  //double Normar_all= (lumi)/Events_all;
  //cout << "Normar_all " << Normar_all << endl;
  //double Normar_all= 1;

  // for (int i = 0; i < MyHistArrayNom->GetEntriesFast(); i++)
  //	    {
  //MyHistArrayNom->Scale(Normar_all);;
	      //	    }

  /* TObject *obj;
  TKey *key;
  TIter next(MyHistArrayNom->GetListOfKeys());
  while ((key = (TKey *) next()))
    {
      obj = MyHistArrayNom->Get(key->GetName());
      //key->Delete();
    }*/
 for (int i = 0; i < 4; i++)
    {
      
      havgM[i]->Scale(Normar_all);
      hEventsAll[i]->Scale(Normar_all);
      hCutflow[i]->Scale(Normar_all);
      hCutflow_WeightedSyst[i]->Scale(Normar_all);
    }
 cout << "XS= " << XS[4] << endl;

 for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < 4; j++)
	{
	  havgM_EL_EFF_ID_TOTAL[i][j]->Scale(Normar_all);
	  havgM_EL_EFF_ISO_TOTAL[i][j]->Scale(Normar_all);
	  havgM_EL_EFF_RECO_TOTAL[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_ISO_STAT[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_ISO_SYS[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_RECO_STAT[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_RECO_STAT_LOWPT[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_RECO_SYS[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_RECO_SYS_LOWPT[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_TTVA_STAT[i][j]->Scale(Normar_all);
	  havgM_MUON_EFF_TTVA_SYS[i][j]->Scale(Normar_all);
	  havgM_Pileup_weight[i][j]->Scale(Normar_all);
	}
    }

 /* if(j_mc == 3)
   {
     for (int i = 0; i < 3; i++)
       {
	 
	 havgM[i]->Print("");
       }
   }*/
  
  TString signal = GetOption();
  if (signal.IsNull()) {
    if (fOut && fOut->IsOpen()) {
      fOut->Write();
      fOut->Close();
    }
  } else {
    Ssiz_t pos = signal.Last('.');
    if (pos<0) pos=signal.Length();
    signal.Replace(pos,20,".pdf");
  }
  
  if (fOut && fOut->IsOpen()) {
    fOut->Write();
    fOut->Close();
  } 
  
}
