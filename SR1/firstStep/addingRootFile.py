import os
my_array_mZd_mH=["30_15",
		 "40_15",
		 "50_20",
		 "60_25",
		 "70_30",
		 "80_35",
		 "90_40",
		 "100_45",
		 "56_18",
		 "60_22", 
		 "64_16", 
		 "66_29", 
		 "68_32", 
		 "73_20",
		 "76_34", 
		 "80_27",
		 "82_36",
		 "84_19",
		 "85_33", 
		 "86_24",
		 "92_37",
		 "93_28", 
		 "95_17",
		 "96_44",
		 "98_42",
		 "102_23",
		 "104_48",
		 "105_35",
		 "105_38",
		 "108_46",
		 "109_26",
		 "110_30",
		 ]
	
	

my_array_camp=["mc16a", "mc16d", "mc16e"]
my_array_chan=["4e", "2e2mu", "4mu"]

c_Syst =["EG_RESOLUTION_ALL1",
	"EG_SCALE_ALL1",
	"MUONS_ID1",
	"MUONS_MS1",
	"MUONS_SAGITTA_RESBIAS1",
	"MUONS_SAGITTA_RHO1",
	"MUONS_SCALE1"]
for k_mZd_mS in my_array_mZd_mH:
    os.system("hadd -f ../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_Nominal/signal_{}_Nominal_reco_NotApplying_slice_m4l_output_combined.root ../../../Ntuple/signal/mc16a/signal_reco/signal_reco_Nominal/signal_{}_mc16a_Nominal_reco_NotApplying_slice_m4l_output.root ../../../Ntuple/signal/mc16d/signal_reco/signal_reco_Nominal/signal_{}_mc16d_Nominal_reco_NotApplying_slice_m4l_output.root  ../../../Ntuple/signal/mc16e/signal_reco/signal_reco_Nominal/signal_{}_mc16e_Nominal_reco_NotApplying_slice_m4l_output.root".format(k_mZd_mS, k_mZd_mS, k_mZd_mS, k_mZd_mS))
    os.system("hadd -f ../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_Nominal/signal_{}_Nominal_reco_NoSliceCutNoXS_output_combined.root ../../../Ntuple/signal/mc16a/signal_reco/signal_reco_Nominal/signal_{}_mc16a_Nominal_reco_NoSliceCutNoXS_output.root ../../../Ntuple/signal/mc16d/signal_reco/signal_reco_Nominal/signal_{}_mc16d_Nominal_reco_NoSliceCutNoXS_output.root  ../../../Ntuple/signal/mc16e/signal_reco/signal_reco_Nominal/signal_{}_mc16e_Nominal_reco_NoSliceCutNoXS_output.root".format(k_mZd_mS, k_mZd_mS, k_mZd_mS, k_mZd_mS))
        
#for k_mZd_mS in my_array_mZd_mH:
#    for k_Syst in c_Syst:
#        os.system("hadd -f ../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_{}/signal_{}_{}up_reco_m4l_output_combined.root ../../../Ntuple/signal/mc16a/signal_reco/signal_reco_{}/signal_{}_mc16a_{}up_reco_m4l_output.root ../../../Ntuple/signal/mc16d/signal_reco/signal_reco_{}/signal_{}_mc16d_{}up_reco_m4l_output.root  ../../../Ntuple/signal/mc16e/signal_reco/signal_reco_{}/signal_{}_mc16e_{}up_reco_m4l_output.root".format(k_Syst, k_mZd_mS,k_Syst, k_Syst, k_mZd_mS, k_Syst,k_Syst, k_mZd_mS, k_Syst, k_Syst, k_mZd_mS, k_Syst))
#        os.system("hadd -f ../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_{}/signal_{}_{}down_reco_m4l_output_combined.root ../../../Ntuple/signal/mc16a/signal_reco/signal_reco_{}/signal_{}_mc16a_{}down_reco_m4l_output.root ../../../Ntuple/signal/mc16d/signal_reco/signal_reco_{}/signal_{}_mc16d_{}down_reco_m4l_output.root  ../../../Ntuple/signal/mc16e/signal_reco/signal_reco_{}/signal_{}_mc16e_{}down_reco_m4l_output.root".format(k_Syst, k_mZd_mS,k_Syst, k_Syst, k_mZd_mS, k_Syst,k_Syst, k_mZd_mS, k_Syst, k_Syst, k_mZd_mS, k_Syst))
# 
#        os.system("hadd -f ../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_{}/signal_{}_{}up_reco_output_combined.root ../../../Ntuple/signal/mc16a/signal_reco/signal_reco_{}/signal_{}_mc16a_{}up_reco_output.root ../../../Ntuple/signal/mc16d/signal_reco/signal_reco_{}/signal_{}_mc16d_{}up_reco_output.root  ../../../Ntuple/signal/mc16e/signal_reco/signal_reco_{}/signal_{}_mc16e_{}up_reco_output.root".format(k_Syst, k_mZd_mS,k_Syst, k_Syst, k_mZd_mS, k_Syst,k_Syst, k_mZd_mS, k_Syst, k_Syst, k_mZd_mS, k_Syst))
#        os.system("hadd -f ../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_{}/signal_{}_{}down_reco_output_combined.root ../../../Ntuple/signal/mc16a/signal_reco/signal_reco_{}/signal_{}_mc16a_{}down_reco_output.root ../../../Ntuple/signal/mc16d/signal_reco/signal_reco_{}/signal_{}_mc16d_{}down_reco_output.root  ../../../Ntuple/signal/mc16e/signal_reco/signal_reco_{}/signal_{}_mc16e_{}down_reco_output.root".format(k_Syst, k_mZd_mS,k_Syst, k_Syst, k_mZd_mS, k_Syst,k_Syst, k_mZd_mS, k_Syst, k_Syst, k_mZd_mS, k_Syst))
       
