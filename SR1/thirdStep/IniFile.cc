/**
 * File: IniFile.cc
 *
 *  Created on: Feb 11, 2009
 *      Author: sash
 */

#include "IniFile.hh"
#include "profile.h"
#include <cassert>
#include <cstring>
#include <cstdio>
#include <cstdlib>

IniFile::IniFile(const char *_filename) {
	filename=strdup(_filename);
	section=NULL;
}

IniFile::IniFile(const char *_filename, const char *_section) {
	filename=strdup(_filename);
	section=strdup(_section);
}

IniFile::~IniFile() {
	delete filename;
	if  (section) delete section;
}

void IniFile::cacheClear() {
	free_profile(filename);
}

void IniFile::select(const char *_section) {
	if  (section) delete section;
	section=strdup(_section);
}

short IniFile::GetSetProfile (int set, const char *AppName, const char *KeyName, const char *Default,
	       char *ReturnedString, const short Size, const char *FileName)
{
   const char   *s;

   s = GetSetProfileChar (set, AppName, KeyName, Default, FileName);
   if (!set) {
      ReturnedString[Size - 1] = 0;
      strncpy (ReturnedString, s, Size - 1);
   }
   return 1;
}

const char* IniFile::getString(const char *KeyName, const char *Default) {
	assert(section);
	return GetSetProfileChar (0, section, KeyName, Default, filename);
}

int IniFile::copyString(const char *KeyName, const char *Default, char *ReturnedString, const int Size) {
	assert(section);
	return GetSetProfile (0, section, KeyName, Default, ReturnedString, Size, filename);
}

int IniFile::getInt(const char *KeyName, const int Default) {
	char buf[80];
	snprintf (buf, 80, "%d", Default);
	const char* cres=getString (KeyName, buf);
	//fprintf(stderr,"IniFile::getInt (%s,%s,%d,%s)=%s\n",section, KeyName,Default,filename,cres);
	if (!strcasecmp (cres, "true")) return 1;
	if (!strcasecmp (cres, "yes")) return 1;
	return strtol (cres,NULL,0);
}

long IniFile::getLong(const char *KeyName, const long Default) {
	char buf[80];
	snprintf (buf, 80, "%ld", Default);
	const char* cres=getString (KeyName, buf);
	//fprintf(stderr,"IniFile::getInt (%s,%s,%d,%s)=%s\n",section, KeyName,Default,filename,cres);
	return strtoll (cres,NULL,0);
}

long IniFile::getHex(const char *KeyName, const long Default) {
	char buf[80];
	snprintf (buf, 80, "0x%lx", Default);
	const char* cres=getString (KeyName, buf);
	//fprintf(stderr,"IniFile::getHex (%s,%s,0x%x,%s)='%s'\n",section, KeyName,Default,filename,cres);
        long res;
	sscanf(cres,"%lx",&res);
	return res;
}

double IniFile::getDouble(const char *KeyName, const double Default) {
	char buf[80];
	snprintf (buf, 80, "%lf", Default);
	const char* cres=getString (KeyName, buf);
	//fprintf(stderr,"IniFile::getDouble (%s,%s,%g,%s)='%s'\n",section, KeyName,Default,filename,cres);
	double res;
	sscanf(cres,"%lf",&res);
	return res;
}

int IniFile::read_array (const char *buff, const char *format, void *array, unsigned int len, int max) {
    if (strlen (buff) == 0) return 0;
    char buff2[buflen];
    static const char *delim = ",\n\0";
    int     i = 0;
    strncpy (buff2, buff,sizeof(buff2));
    char* save=0;
    char* tok = static_cast<char *>(strtok_r (buff2, delim, &save));
    while (tok != NULL && i <= max) {
    	while (*tok == ' ') tok++; // eat front spaces
    	for (char* patt = tok + strlen (tok) - 1;
			*patt == ' ' && patt > tok;
			*patt-- = '\0');
    	// ugly ugly ugly: get pointer to element i of unkown type array
    	// calculate with char* then convert to void* for sscanf
    	void* p = static_cast<void *>((len * i) + static_cast<char *>(array));
    	sscanf (tok, format, p);
    	//printf("read_array: %d) %s [%p] -> 0x%p\n",i,tok,save,p);
    	i++;
    	tok = static_cast<char *>(strtok_r (NULL, delim, &save));
    }
    return i;
}

void IniFile::getDoubles(const char *KeyName, double v[], int n, const char *defaultVal) {
	const char* cres=getString (KeyName, defaultVal);
	//printf("IniFile::getDoubles (%s,%s)='%s'\n",section,KeyName,cres);//DEBUG
	read_array(cres,"%lf",v,sizeof(double),n);
}

void IniFile::getBools(const char* KeyName, bool v[], int n, const char* defaultVal) {
	const char* cres=getString (KeyName, defaultVal);
	//printf("IniFile::getBools (%s,%s)='%s'\n",section,KeyName,cres);//DEBUG
	read_array(cres,"%d",v,sizeof(bool),n);
}

#if 0
inline unsigned int GetPrivateProfileAddr (const char *AppName, const char *KeyName,
			   const char *Default,const char* File) {
   char buffer[1024];
   unsigned int addr;
   getString(KeyName,Default,buffer,sizeof(buffer));
   sscanf(buffer,"0x%x",&addr);
   return addr;
}

inline void GetPrivateProfileInts(const char* filename,const char* sect,const char* label,
                                     int v[], int n, const char* defaultVal="") {
    char buff[1024];
    GetPrivateProfileString (sect, label, defaultVal, buff, sizeof(buff), filename);
    //printf(" doubles: %s %s '%s'\n",sect,label,buff);//DEBUG
    read_array(buff,"%ld",v,sizeof(int),n);
}

inline void GetPrivateProfileBools(const char* filename,const char* sect,const char* label,
                                     bool v[], int n, const char* defaultVal="") {
    char buff[1024];
    GetPrivateProfileString (sect, label, defaultVal, buff, sizeof(buff), filename);
    //printf(" bools: %s %s '%s'\n",sect,label,buff);//DEBUG
    read_array(buff,"%ld",v,sizeof(bool),n);
}

int WritePrivateProfileString (const char *AppName, const char *KeyName, const char *String, const char *FileName)
{
   return GetSetProfile (1, AppName, KeyName, String, "", 0, FileName);
}
#endif


/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
