/**
 * File: IniFile.hh
 *
 *  Created on: Feb 11, 2009
 *      Author: sash
 */

#ifndef INIFILE_HH_
#define INIFILE_HH_

class IniFile {
public:
	IniFile(const char* filename);
	IniFile(const char* filename,const char* section);
	~IniFile();
	void cacheClear();
	void select(const char* section);
	const char* getSection() const { return section; };
	int getInt (const char* KeyName, const int Default);
	long getLong (const char* KeyName, const long Default);
	long getHex (const char* KeyName, const long Default);
	double getDouble (const char* KeyName, const double Default);
	const char* getString (const char *KeyName, const char *Default);
	int copyString (const char *KeyName, const char *Default, char *ReturnedString, const int Size);
	void getDoubles(const char* KeyName, double v[], int n, const char* defaultVal="");
	void getBools(const char* KeyName, bool v[], int n, const char* defaultVal="");

private:
	static int read_array (const char *buff, const char *format, void *array, unsigned int len, int max);
	static short GetSetProfile (int set, const char *AppName, const char *KeyName, const char *Default,
		       char *ReturnedString, const short Size, const char *FileName);

private:
	static const int buflen=1024;
	char* section;
	char* filename;
};

#endif /* INIFILE_HH_ */

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
