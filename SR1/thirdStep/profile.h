#ifndef PROFILE_H
#define PROFILE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Prototypes for the profile management functions */
void sync_profiles (void);
void sync_profile (const char *filename);

void free_profiles (void);
void free_profile (const char *filename);
const char  *get_profile_string (const char *AppName, const char *KeyName,
			    const char *Default, const char *FileName);
const char* GetSetProfileChar (int set, const char *AppName, const char *KeyName,
			   const char *Default, const char *FileName);


/** Returns a pointer for iterating on appname section, on profile file */
void   *profile_iterator_init (const char *appname, const char *file);

/**
 * Returns both the key and the value of the current section.
 * You pass the current iterating pointer and it returns the new pointer
 */
void   *profile_iterator_next (void *s, char **key, char **value);

/** Removes all the definitions from section appname on file */
void    profile_clean_section (const char *appname, const char *file);

#ifdef __cplusplus
}
#endif

#endif
