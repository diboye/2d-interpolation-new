/*
 * Initialization-File Functions.
 *
 * From the Wine project

 Copyright (C) 1993, 1994 Miguel de Icaza.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
static char Copyright[] = "Copyright (C) 1993, 1994 Miguel de Icaza";
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* For free() and atoi() */
#include <sys/types.h>
/*
 * #include "mad.h"
 * #include "util.h"
 * #include "global.h"
 */
/*
 * this was in mad.h, Memory Allocation Debugging system
 * Hopefully, i'll have not too many problems with memory,
 * so i just drop it.         sash
 */
#define xmalloc(x, y)     malloc (x)
#include "profile.h"

#define INIFILE "win.ini"
#define STRSIZE 255
#define overflow (next == &CharBuffer [STRSIZE-1])

#undef DEBUG

enum {
   FirstBrace, OnSecHeader, IgnoreToEOL, KeyDef, KeyValue, FirstChar
};

typedef struct TKeys {
   char   *KeyName;
   char   *Value;
   struct TKeys *link;
} TKeys;

typedef struct TSecHeader {
   char   *AppName;
   TKeys  *Keys;
   struct TSecHeader *link;
} TSecHeader;

typedef struct TProfile {
   char   *FileName;
   TSecHeader *Section;
   struct TProfile *link;
} TProfile;

TProfile *Current = 0;
TProfile *Base = 0;

static int is_loaded (const char *FileName, TSecHeader ** section) {
	TProfile *p = Base;

	while (p) {
		if (!strcasecmp (FileName, p->FileName)) {
			Current = p;
			*section = p->Section;
			return 1;
		}
		p = p->link;
	}
	return 0;
}

static TSecHeader* load (const char *file) {
	FILE   *f;
	int     state;
	TSecHeader *SecHeader = 0;
	char    CharBuffer[STRSIZE];
	char   *next = (char*)"";		/* Not needed */
	int     c;

	if ((f = fopen (file, "r")) == NULL) return NULL;

	state = FirstBrace;
	while ((c = getc (f)) != EOF) {
		if (c == '\r')		/* Ignore Carriage Return */
			continue;

		switch (state) {

		case OnSecHeader:
			if (c == ']' || overflow) {
				*next = '\0';
				next = CharBuffer;
				SecHeader->AppName = strdup (CharBuffer);
				state = IgnoreToEOL;
			} else
				*next++ = c;
			break;

		case IgnoreToEOL:
			if (c == '\n') {
				state = FirstChar;
				next = CharBuffer;
			}
			break;

		case FirstBrace:
		case FirstChar:
			if (c == '\n') {
				state = FirstChar;
				next = CharBuffer;
				break;
			}
			if (c == '#' || c == ';') {
				state = IgnoreToEOL;
				break;
			}
		case KeyDef:
			if (c == '[') {
				TSecHeader *temp = SecHeader;
				SecHeader = (TSecHeader *) xmalloc (sizeof (TSecHeader),"KeyDef");
				SecHeader->link = temp;
				SecHeader->Keys = NULL;
				state = OnSecHeader;
				next = CharBuffer;
				break;
			}
			if (state == FirstBrace)	/* On first pass, don't allow dangling keys */
				break;

			if (state == FirstChar)	/* On first pass, don't allow dangling keys */
				state = KeyDef;

			if (c == ' ' || c == '\t')
				break;

			if (c == '\n' || overflow) {	/* Abort Definition */
#ifdef DEBUG
				printf ("Abort %s (%s)\n", next, CharBuffer);
#endif
				next = CharBuffer;
				break;
			}
			if (c == '=' || overflow) {
				TKeys  *temp;

				temp = SecHeader->Keys;
				*next = '\0';
				SecHeader->Keys = (TKeys *) xmalloc (sizeof (TKeys), "KD2");
				SecHeader->Keys->link = temp;
				SecHeader->Keys->KeyName = strdup (CharBuffer);
				SecHeader->Keys->Value = NULL;
				state = KeyValue;
				next = CharBuffer;
			} else
				*next++ = c;
			break;

		case KeyValue:
			if (overflow || c == '\n') {
				*next = '\0';
				SecHeader->Keys->Value = strdup (CharBuffer);
				state = c == '\n' ? FirstChar : IgnoreToEOL;
				next = CharBuffer;
#ifdef DEBUG
				printf ("[%s] (%s)=%s\n", SecHeader->AppName,
						SecHeader->Keys->KeyName, SecHeader->Keys->Value);
#endif
			} else
				*next++ = c;
			break;

		}				/* switch */
#ifdef DEBUG
		printf ("%i Step %c [%s] (%s)\n", state, c, next, CharBuffer);
#endif
	}   			/* while ((c = getc (f)) != EOF) */
	if (state==KeyValue && SecHeader->Keys->Value==NULL ) {
		SecHeader->Keys->Value = strdup (CharBuffer);
	}
	return SecHeader;
}

static void new_key (TSecHeader * section, const char *KeyName, const char *Value)
{
	TKeys  *key;

	key = (TKeys *) xmalloc (sizeof (TKeys), "new_key");
	key->KeyName = strdup (KeyName);
	key->Value = strdup (Value);
	key->link = section->Keys;
	section->Keys = key;
}

const char* GetSetProfileChar (int set, const char *AppName, const char *KeyName,
		const char *Default, const char *FileName)
{

	TProfile *New;
	TSecHeader *section;
	TKeys  *key;

	if (!is_loaded (FileName, &section)) {
		New = (TProfile *) xmalloc (sizeof (TProfile), "GetSetProfile");
		New->link = Base;
		New->FileName = strdup (FileName);
		New->Section = load (FileName);
		Base = New;
		section = New->Section;
		Current = New;
	}
	/* Start search */
	for (; section; section = section->link) {
		if (strcasecmp (section->AppName, AppName))
			continue;
		for (key = section->Keys; key; key = key->link) {
			if (strcasecmp (key->KeyName, KeyName))
				continue;
			if (set) {
				free (key->Value);
				key->Value = strdup (Default);
			}
			if (key->Value==NULL) {
				return 0;
			};
			return key->Value;
		}
		/* If getting the information, then don't write the information
		 * to the INI file, need to run a couple of tests with windog */
		/* No key found */
		if (set) {
			new_key (section, KeyName, Default);
			return 0;
		}
	}

	/* Non existent section */
	if (set) {
		section = (TSecHeader *) xmalloc (sizeof (TSecHeader), "GSP3");
		section->AppName = strdup (AppName);
		section->Keys = 0;
		new_key (section, KeyName, Default);
		section->link = Current->Section;
		Current->Section = section;
	}
	return Default;
}

static void dump_keys (FILE * profile, TKeys * p) {
   if (!p) return;
   dump_keys (profile, p->link);
   fprintf (profile, "%s=%s\n", p->KeyName, p->Value);
}

static void dump_sections (FILE * profile, TSecHeader * p) {
   if (!p) return;
   dump_sections (profile, p->link);
   if (p->AppName[0]) {
      fprintf (profile, "\n[%s]\n", p->AppName);
      dump_keys (profile, p->Keys);
   }
}

static void dump_profile (TProfile * p) {
   FILE   *profile;

   if (!p) return;
   dump_profile (p->link);
   if ((profile = fopen (p->FileName, "w")) != NULL) {
      dump_sections (profile, p->Section);
      fclose (profile);
   }
}

static void dump_oneprofile (TProfile * p) {
   FILE   *profile;

   if (!p) return;
   if ((profile = fopen (p->FileName, "w")) != NULL) {
      dump_sections (profile, p->Section);
      fclose (profile);
   }
}
/*
 * Must be called at the end of wine run
 */

void sync_profiles (void) {
   dump_profile (Base);
}

void sync_profile (const char * filename) {
	TProfile * p=Base;
	while (p) {
		if (strcmp(filename,p->FileName)==0) {
			dump_oneprofile (p);
			break;
		}
		p=p->link;
	}
}

static void free_keys (TKeys * p) {
   if (!p) return;
   free_keys (p->link);
   free (p->KeyName);
   free (p->Value);
   free (p);
}

static void free_sections (TSecHeader * p) {
   if (!p) return;
   free_sections (p->link);
   free_keys (p->Keys);
   free (p->AppName);
   p->link = 0;
   p->Keys = 0;
   free (p);
}

static void free_1profile (TProfile * p) {
	free_sections (p->Section);
	free (p->FileName);
	free (p);
}
static void free_profile (TProfile * p) {
   if (!p) return;
   free_profile (p->link);//recurse
   free_1profile(p);
}

void free_profiles (void) {
   free_profile (Base);
}

void free_profile(const char *filename) {
	TProfile * p=Base;
	TProfile* op=NULL;
	while (p) {
		if (strcmp(filename,p->FileName)==0) {
			if (op==NULL)
				Base=p->link;
			else
				op->link=p->link;
			free_1profile (p);
			break;
		}
		op=p;
		p=p->link;
	}
}

void* profile_iterator_init (const char *appname, const char *file) {
   TProfile *New;
   TSecHeader *section;

   if (!is_loaded (file, &section)) {
      New = (TProfile *) xmalloc (sizeof (TProfile), "GetSetProfile");
      New->link = Base;
      New->FileName = strdup (file);
      New->Section = load (file);
      Base = New;
      section = New->Section;
      Current = New;
   }
   for (; section; section = section->link) {
      if (strcasecmp (section->AppName, appname))
	 continue;
      return section->Keys;
   }
   return 0;
}

void* profile_iterator_next (void *s, char **key, char **value) {
   TKeys  *keys = (TKeys *) s;

   if (keys) {
      *key = keys->KeyName;
      *value = keys->Value;
      keys = keys->link;
   }
   return keys;
}

void profile_clean_section (const char *appname, const char *file) {
   TSecHeader *section;

   /* We assume the user has called one of the other initialization funcs */
   if (!is_loaded (file, &section)) {
      fprintf (stderr, "Warning: profile_clean_section called before init\n");
      return;
   }
   /* We only disable the section, so it will still be freed, but it */
   /* won't be find by further walks of the structure */

   for (; section; section = section->link) {
	   if (strcasecmp (section->AppName, appname))
		   continue;
	   section->AppName[0] = 0;
   }
}

