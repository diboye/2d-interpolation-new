#include "2D_Smooth.h"
#include "tps_calculator.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"


void Animate()
{
   theta += 1;
   gPad->GetView()->RotateView(theta,phi);
}


double tps_base_func(double r)
{
  if ( r == 0.0 )
    return 0.0;
  else
    return r*r * log(r);
}

void clear_grid()
{
  for (int x=0; x<GRID_W; ++x)
    for (int z=0; z<GRID_H; ++z)
      grid[x][z] = 0;
}

/*
 *  Calculate Thin Plate Spline (TPS) weights from
 *  control points and build a new height grid by
 *  interpolating with them.
 */
int calc_tps()
{
  // You We need at least 3 points to define a plane
  if ( control_points.size() < 3 )
    return 1;

  unsigned p = control_points.size();

  // Allocate the matrix and vector
  matrix<double> mtx_l(p+3, p+3);
  matrix<double> mtx_v(p+3, 1);
  matrix<double> mtx_orig_k(p, p);

  // Fill K (p x p, upper left of L) and calculate
  // mean edge length from control points
  //
  // K is symmetrical so we really have to
  // calculate only about half of the coefficients.
  double a = 0.0;
  for ( unsigned i=0; i<p; ++i )
  {
    for ( unsigned j=i+1; j<p; ++j )
    {
      Vec pt_i = control_points[i];
      Vec pt_j = control_points[j];
      pt_i.y = pt_j.y = 0;
      double elen = (pt_i - pt_j).len();
      mtx_l(i,j) = mtx_l(j,i) =
        mtx_orig_k(i,j) = mtx_orig_k(j,i) =
          tps_base_func(elen);
      a += elen * 2; // same for upper & lower tri
    }
  }
  a /= (double)(p*p);

  // Fill the rest of L
  for ( unsigned i=0; i<p; ++i )
  {
    // diagonal: reqularization parameters (lambda * a^2)
    mtx_l(i,i) = mtx_orig_k(i,i) =
      regularization * (a*a);

    // P (p x 3, upper right)
    mtx_l(i, p+0) = 1.0;
    mtx_l(i, p+1) = control_points[i].x;
    mtx_l(i, p+2) = control_points[i].z;

    // P transposed (3 x p, bottom left)
    mtx_l(p+0, i) = 1.0;
    mtx_l(p+1, i) = control_points[i].x;
    mtx_l(p+2, i) = control_points[i].z;
  }
  // O (3 x 3, lower right)
  for ( unsigned i=p; i<p+3; ++i )
    for ( unsigned j=p; j<p+3; ++j )
      mtx_l(i,j) = 0.0;


  // Fill the right hand vector V
  for ( unsigned i=0; i<p; ++i )
    mtx_v(i,0) = control_points[i].y;
  mtx_v(p+0, 0) = mtx_v(p+1, 0) = mtx_v(p+2, 0) = 0.0;

  // Solve the linear system "inplace"
  if (0 != LU_Solve(mtx_l, mtx_v))
  {
    puts( "Singular matrix! Aborting." );
    return(1);
  }

  // Interpolate grid heights
  for ( int x=-GRID_W/2; x<GRID_W/2; ++x )
  {
    for ( int z=-GRID_H/2; z<GRID_H/2; ++z )
    {
      double h = mtx_v(p+0, 0) + mtx_v(p+1, 0)*x + mtx_v(p+2, 0)*z;
      Vec pt_i, pt_cur(x,0,z);
      for ( unsigned i=0; i<p; ++i )
      {
        pt_i = control_points[i];
        pt_i.y = 0;
        h += mtx_v(i,0) * tps_base_func( ( pt_i - pt_cur ).len());
      }
      grid[x+GRID_W/2][z+GRID_H/2] = h;
    }
  }

  // Calc bending energy
  matrix<double> w( p, 1 );
  for ( int i=0; i<p; ++i )
    w(i,0) = mtx_v(i,0);
  matrix<double> be = prod( prod<matrix<double> >( trans(w), mtx_orig_k ), w );
  bending_energy = be(0,0);

  return 0;
}

void Do_interpolation(int j)
{
  Vec one_point;  // 3D vector
  int binx, biny;
  control_points.clear();
  for (int i = 0; i < v.size(); i++){
    binx = hPlane[0]->GetXaxis()->FindBin(v[i].mS);
    biny = hPlane[0]->GetYaxis()->FindBin(v[i].mZd);
    one_point.x = ((TAxis*)hContPt->GetXaxis())->GetBinCenter(binx);
    one_point.z = ((TAxis*)hContPt->GetYaxis())->GetBinCenter(biny);
    if(j == 0)   one_point.y = v[i].mean;
    if(j == 1)   one_point.y = v[i].sigma;
    if(j == 2)   one_point.y = v[i].norm;
    hContPt->SetBinContent(binx,biny,one_point.y);
    control_points.push_back(one_point);
    //std::cout << "orig - " << j << ":  " << v[i].mS << "  " << v[i].mZd <<  std::endl;
    //std::cout << "bin  - " << j << ":  " << ((TAxis*)hPlane[0]->GetXaxis())->GetBinCenter(binx) << "  " << ((TAxis*)hPlane[0]->GetYaxis())->GetBinCenter(biny) <<  std::endl;
    //std::cout << "grid - " << j << ":  " << control_points[i].x << "  " << control_points[i].z << "  " << control_points[i].y << std::endl;
  //dt->SetPoint(i,one_point.x,one_point.z,one_point.y);
    dt[j]->SetPoint(i,v[i].mS,v[i].mZd,one_point.y);
  }
  clear_grid();
  if(!calc_tps()) {
    for (int x=0; x<GRID_W; ++x) {
      for (int z=0; z<GRID_H; ++z){
        one_point.x = ((TAxis*)hContPt->GetXaxis())->GetBinCenter(x+1);
        one_point.z = ((TAxis*)hContPt->GetYaxis())->GetBinCenter(z+1);
        if (grid[x][z] > 0){
          hGrid->SetBinContent(x+1,z+1,grid[x][z]);
          hInterp[j]->SetBinContent(x+1,z+1,grid[x][z]);
          if(((TAxis*)hInterp[j]->GetXaxis())->GetBinCenter(x+1) < (((TAxis*)hInterp[j]->GetYaxis())->GetBinCenter(z+1))/0.5) hInterp[j]->SetBinContent(x+1,z+1,0);
          //std::cout << ((TAxis*)hInterp->GetXaxis())->GetBinCenter(x+1) << "  " << ((TAxis*)hInterp->GetYaxis())->GetBinCenter(z+1) << "  " << grid[x][z] << std::endl;
        }
        else {
          hGrid->SetBinContent(x+1,z+1,0);
          hInterp[j]->SetBinContent(x+1,z+1,0);
        }
      }
    }
    std::cout << j << ":  " << "**** Worked ****\n" << std::endl;
  }
  else
  {
    hGrid->Reset();
    hContPt->Reset();
    std::cout << j << ":  " << "**** Failed ****\n" << std::endl;
  }
}


int main(int argc, char *argv[])
{

	if (argc < 3) {
			std::cout << "Usage: 2D_Smooth datafile_txt inifile root_out_file" << std::endl;
			exit(-1);
	}

// Extract info from commabine arguments
  std::string filename = argv[1];
	std::string iniFileName = argv[2];

// Read in the settings file Information
	IniFile iniFile(iniFileName.c_str(), "Initial_Values");
	regularization = iniFile.getDouble("regularization", 0.0);

  // Extract ROOT output file name from the argument list
  char root_file[64];   // root file
  strcpy(root_file,argv[3]);


// Initiate ROOT Graphics session
	TApplication theApp("hello", &argc, argv);
	c = new TCanvas("canv", "Yankee-doodle",1000, 500);
  TString filename_ROOT = root_file;
  TFile *f = new TFile(filename_ROOT,"RECREATE");


// Read in Data
	PeakData peak_data;
	std::string line;
//	std::vector<PeakData> v; .... declare in header
	std::ifstream ifs(filename);
	std::cout  << "READING ..... " << filename << "\n";
	if (ifs) {
		std::getline(ifs, line); // read past the first line as it has the header ... dummy read
		std::cout  << "READING ..... " << line << "\n";
		while (std::getline(ifs, line)) {
			std::istringstream iss(line);
			iss >> peak_data.mS	 >> peak_data.mZd	 >> peak_data.mean	 >> peak_data.erMean	 >> peak_data.sigma	 >> peak_data.erSigma	 >> peak_data.norm	 >> peak_data.erNorm;
			v.push_back(peak_data);
		}
	}
	else std::cerr << "Couldn't open " << filename << " for reading\n";
	std::cout  << "READ ..... " << v.size() << " lines" << "\n";


	gStyle->SetOptStat(0);
	c->Divide(2,2);
	int binx, biny;
	for (int j = 0; j < 3; j++) {
		hPlane[j] = new TH2D(TString::Format("hPlane_%s", c_name[j]), TString::Format("hPlane_%s", c_name[j]), NBin, mS_low_h, mS_high_h, NBin, mZd_low_h, mZd_high_h);
		for (int i = 0; i < v.size(); i++){
			binx = hPlane[0]->GetXaxis()->FindBin(v[i].mS);
			biny = hPlane[0]->GetYaxis()->FindBin(v[i].mZd);
			if(j == 0)   hPlane[0]->SetBinContent(binx,biny,v[i].mean);
			if(j == 1)   hPlane[1]->SetBinContent(binx,biny,v[i].sigma);
			if(j == 2)   hPlane[2]->SetBinContent(binx,biny,v[i].norm);
		}
		  c->cd(j+1);
			hPlane[j]->Draw("lego2");
	}
	c->Update();
	gPad->Modified();		// make sure
	gPad->Update();		  // hist is drawn
	gSystem->ProcessEvents();


  // Initial plot to check the data read in
  std:: cout << " Histo edges   : " <<  mS_low_h    << "  " <<  mS_high_h   << " " <<  mZd_low_h   << " " <<  mZd_high_h << " \n";
  std:: cout << " Histo centres : " << ((TAxis*)hPlane[0]->GetXaxis())->GetBinCenter(1)    << " "
             << ((TAxis*)hPlane[0]->GetXaxis())->GetBinCenter(NBin)    << " "
             << ((TAxis*)hPlane[0]->GetYaxis())->GetBinCenter(1)    << " "
             << ((TAxis*)hPlane[0]->GetYaxis())->GetBinCenter(NBin)    << " \n";


// Now remove the Closure test point
// H_s = 375  and Z_d = 140   -->  Point 15
// H_s = 400  and Z_d = 50    -->  Point  2
// H_s = 320  and Z_d = 110   -->  Point 13
// H_s = 200  and Z_d = 30    -->  Point 10
// H_s = 760  and Z_d = 190   -->  Point 33

// H_s = 80  and Z_d = 27   -->  Point 15

	PeakData test_point = v[15];
  std::cout << "Test point is H_s = " << test_point.mS  << " and Z_d = " << test_point.mZd << "\n";
  //v.erase (v.begin()+15);
  for (size_t i = 0; i<v.size(); i++) std::cout << "H_s = " << v[i].mS  << " Z_d = " << v[i].mZd << "\n";

// Now prepare to Interpolate

	hGrid = new TH2D("Grid", "Grid", NBin, mS_low_g, mS_high_g, NBin, mZd_low_g, mZd_high_g);   // As TPS needs it
	hContPt = new TH2D("ContPt", "ContPt", NBin, mS_low_g, mS_high_g, NBin, mZd_low_g, mZd_high_g); // As TPS needs it
  c3 = new TCanvas("Canv3", "Canv3",25,25,1000, 500);
  c3->Divide(2,2);

  // Initial plot to check the data read in
  std:: cout << " Histo edges   : " <<  mS_low_g    << "  " <<  mS_high_g   << " " <<  mZd_low_g   << " " <<  mZd_high_g << " \n";
  std:: cout << " Histo centres : " << ((TAxis*)hGrid->GetXaxis())->GetBinCenter(1)    << " "
             << ((TAxis*)hGrid->GetXaxis())->GetBinCenter(NBin)    << " "
             << ((TAxis*)hGrid->GetYaxis())->GetBinCenter(1)    << " "
             << ((TAxis*)hGrid->GetYaxis())->GetBinCenter(NBin)    << " \n";


  for (int j = 0; j < 3; j++) {
    hMax1 = 0; hMax2=0;
    c2[j] = new TCanvas(TString::Format("Canv_%s", c_name[j]), TString::Format("Canv_%s", c_name[j]),(j+1)*50,(j+1)*50,1000, 500);
    hInterp[j] = new TH2D(TString::Format("Interp_%s", c_name[j]), TString::Format("Interp_%s", c_name[j]), NBin, mS_low_h, mS_high_h, NBin, mZd_low_h, mZd_high_h);   // Mapped back to hPlane
    hCheck[j] = new TH2D(TString::Format("Check_%s", c_name[j]), TString::Format("Check_%s", c_name[j]), NBin, mS_low_h, mS_high_h, NBin, mZd_low_h, mZd_high_h);   // Mapped back to hPlane
    dt[j] = new TGraph2D();
    Do_interpolation(j);
		c2[j]->cd();
    if (hInterp[j]->GetMaximum()==0) {
      hMax1 = hPlane[j]->GetMaximum()*1.1;
    }
    else
    {
      hMax2 = hInterp[j]->GetMaximum()*1.1; // failed  - use data hsito for max bin value
    }
    hMax[j]=std::max(hMax1,hMax2);
    dt[j]->SetMaximum(hMax[j]); // this is todraw dots on top of the contril points
    dt[j]->SetMinimum(0);

    hPlane[j]->SetMaximum(hMax[j]);
    hPlane[j]->SetMinimum(0);
    hPlane[j]->Draw("lego1 0 ");
    hInterp[j]->SetMaximum(hMax[j]);  //  hInterp remapped to be same space as hPlane
    hInterp[j]->SetMinimum(0);
    hInterp[j]->Draw("same lego");
    dt[j]->Draw("same p0");
    c2[j]->Update();

    //For_Publishing();
    //SetAtlasStyle();

    c3->cd(j+1);
    hPlane[j]->Draw("lego1 0 ");
    hPlane[j]->GetXaxis()->SetTitle("m_S");
    hPlane[j]->GetYaxis()->SetTitle("m_Zd");
    hPlane[j]->GetXaxis()->CenterTitle();
    hPlane[j]->GetYaxis()->CenterTitle();
    hPlane[j]->GetXaxis()->SetTitleOffset(2.0);
    hPlane[j]->GetYaxis()->SetTitleOffset(2.0);
    hInterp[j]->Draw("same lego");
    dt[j]->Draw("same p0");
    c3->Update();
	}

  // Write out the test point and the interpolated point
    std::cout << "Test point is :      " << test_point.mS	 << ", "
              << test_point.mZd << ", "
              << test_point.mean << ", "
              << test_point.sigma << ", "
              << test_point.norm << std::endl;
    binx = hInterp[0]->GetXaxis()->FindBin(test_point.mS);
    biny = hInterp[0]->GetYaxis()->FindBin(test_point.mZd);
    std::cout << "Interpolated point : " <<  ((TAxis*)hInterp[0]->GetXaxis())->GetBinCenter(binx) <<  ", "
              <<((TAxis*)hInterp[0]->GetYaxis())->GetBinCenter(biny)  << " "
              << hInterp[0]->GetBinContent(binx,biny)  << ", "
              << hInterp[1]->GetBinContent(binx,biny)  << ", "
              << hInterp[2]->GetBinContent(binx,biny) << std::endl;

  for (int j = 0; j < 3; j++) {
    if(j == 0)   hCheck[0]->SetBinContent(binx,biny,test_point.mean);
    if(j == 1)   hCheck[1]->SetBinContent(binx,biny,test_point.sigma);
    if(j == 2)   hCheck[2]->SetBinContent(binx,biny,test_point.norm);


    hCheck[j]->SetMaximum(hMax[j]);
    hCheck[j]->SetMinimum(0);
    c3->cd(j+1);
    c3->Update();
    hCheck[j]->SetFillColor(kRed);
    hCheck[j]->Draw("same lego1 0");
    c2[j]->cd();
    c2[j]->Update();
    hCheck[j]->Draw("same lego1 0");
  }


// Extract 1D m_Zd Histo for Panel in m_S
  float sigma = 10;   // for m_S = 80 GeV, sigma here is 10 GeV ????
  float panel_cen = 80, panel_low = panel_cen - sigma, panel_high = panel_cen + sigma;

  int panel_cen_b = hInterp[0]->GetXaxis()->FindBin(panel_cen);
  int panel_low_b = hInterp[0]->GetXaxis()->FindBin(panel_low);
  int panel_high_b = hInterp[0]->GetXaxis()->FindBin(panel_high);
  std:: cout << " panel :  low " << panel_low   << " cen " << panel_cen   << " high " <<  panel_high   <<  " \n";
  std:: cout << "panelb :  low " << panel_low_b << " cen " << panel_cen_b << " high " << panel_high_b   <<  " \n";
  float av;
  int n_panel_width;
  for (int j = 0; j < 3; j++) {
    hInterp1D[j] = new TH1D(TString::Format("Interp1D_%s", c_name[j]), TString::Format("Interp1D_%s", c_name[j]), NBin, mZd_low_h, mZd_high_h);   // This will be the 1D histo for 1 panel.
    hInterpAdd[j] = new TH2D(TString::Format("InterpAdd_%s", c_name[j]), TString::Format("InterpAdd_%s", c_name[j]), NBin, mS_low_h, mS_high_h, NBin, mZd_low_h, mZd_high_h);   // To draw on top
    for (size_t i = 0; i < NBin; i++) {
      av = 0.0;
      n_panel_width = 0;
      for (size_t k = panel_low_b; k <= panel_high_b; k++) {
        if (hInterp[j]->GetBinContent(k,i) > 0.0 ) {
        av = av + hInterp[j]->GetBinContent(k,i+1);
        n_panel_width = n_panel_width + 1;
        }
      }
      if (av > 0.0) av = av/n_panel_width;
      hInterp1D[j]->SetBinContent(            i+1,av);
      hInterpAdd[j]->SetBinContent(panel_cen_b,i+1,av); // To plot on top
    }
  }
  c4 = new TCanvas("canv4", "Interpolated",700, 200, 1000, 500);
  c4->Divide(2,2);
  c5 = new TCanvas("Canv5", "Canv5",20,200,1000, 500);
  c5->Divide(2,2);

  for (int j = 0; j < 3; j++) {
    c4->cd(j+1);
    hInterp1D[j]->Draw();
    c4->Update();
    c2[j]->cd();
    hInterpAdd[j]->SetMaximum(hMax[j]);
    hInterpAdd[j]->SetMinimum(0);
    hInterpAdd[j]->SetLineColor(kRed);
    //hInterpAdd[j]->SetLineWidth(9);
    hInterpAdd[j]->SetMarkerColor(kMagenta);
    hInterpAdd[j]->Draw("same lego 0");
    c2[j]->Update();
    if (j>0) {
      c5->cd(j+1);
      hPlane[j]->Draw("lego1 0 ");
      hInterp[j]->Draw("same lego");
      dt[j]->Draw("same p0");
      hInterpAdd[j]->Draw("same lego 0");
      hCheck[j]->Draw("same lego1 0");
      c5->Update();
    }


    std::cout << c_name[j] << " at " << panel_cen << " GeV and 27 GeV is " <<
    hInterp1D[j]->GetBinContent(hInterp1D[j]->GetXaxis()->FindBin(27)) << std::endl;
  }


  //  Extract 1D m_Zd Histo for Panels in m_S and pack in 2D histo
  // Note ... bin centres are on integers in GeV
  // Slice or panel boundaries must also always be on an integers
  // The bin boundaries in m_S are not necesarily on integers
  // At the moment, half the M_S bin goes in a panel, on either side.
      double fraction, panel_wid, hInterp_low_b, hInterp_high_b;
      int NBins_m_S = 18;  //want width of 5, centres on multiples of 5, from 30 - 130
      Double_t edges[NBins_m_S + 1];
      for (size_t i = 0; i < NBins_m_S; i++) edges[i] = 30+i*5;
      //edges[0] = mS_low_h;
      edges[NBins_m_S] = mS_high_h;
      for (int j = 0; j < 3; j++) {                                 // Loop over DSCB parameters
        hPanel2D[j] = new TH2D(TString::Format("Panel_%s", c_name[j]), TString::Format("Panel_%s", c_name[j]), NBins_m_S, edges, NBin, mZd_low_h, mZd_high_h);
        for (size_t i = 0; i < NBin; i++) {                         // Loop over <m_ll>
          //for (size_t l = 0; l < NBins_m_S; l++) {                          // Loop over m_S slice number
          for (size_t l = 0; l < NBins_m_S-1; l++) {                          // Loop over m_S slice number
            panel_cen = hPanel2D[0]->GetXaxis()->GetBinCenter(l+1);          // must be an integer
            panel_low = hPanel2D[0]->GetXaxis()->GetBinLowEdge(l+1);         // must be an integer
            panel_wid = hPanel2D[0]->GetXaxis()->GetBinWidth(l+1);           // must be an integer
            hInterp_low_b =  hInterp[0]->GetXaxis()->FindBin(panel_low);     // must be an integer
            hInterp_high_b = hInterp[0]->GetXaxis()->FindBin(panel_low+panel_wid-.001);
            if (i==0 && j==0) {
              std:: cout << " panel   :  (low,cen,high) = ( " << panel_low   << " , " << panel_cen   << " , " <<  panel_low + panel_wid   <<  ") \n";
              std:: cout << " panel-b :  (low  ,  high) = ( " << hInterp_low_b << "   ,    " <<  hInterp_high_b   <<  ") \n";
            }
            av = 0.0;
            n_panel_width = 0;
            for (size_t k = hInterp_low_b; k <= hInterp_high_b; k++) {    // Loop within m_S slice
              fraction = 1.0;
              if(k == hInterp_low_b)  fraction = 1.0;
              if(k == hInterp_high_b) fraction = 1.0;
              if (hInterp[j]->GetBinContent(k,i+1) > 0.0 ) {
                av = av + fraction * hInterp[j]->GetBinContent(k,i+1);
                n_panel_width = n_panel_width + 1;
              } //endif
            } // Loop within slice
            if (av > 0.0) av = av/n_panel_width;
            hPanel2D[j]->SetBinContent(l+1,i+1,av);
          }  // Loop over slice number
          //hPanel2D[j]->SetBinContent(1,i+1,0);
          hPanel2D[j]->SetBinContent(NBins_m_S,i+1,0);
        }  // Loop over <m_ll>
      }    // Loop over DSCB
      c6 = new TCanvas("Canv6", "Canv6",40,250,1000, 500);
      c6->Divide(2,2);
      c7 = new TCanvas("Canv7", "Canv7",60,300,1000, 500);





      for (int j = 0; j < 3; j++) {
        c6->cd(j+1);
        hPanel2D[j]->SetMaximum(hMax[j]);
        hPanel2D[j]->SetMinimum(0);
        hPanel2D[j]->SetLineColor(kRed);
        hPanel2D[j]->Draw("lego 0");
        hInterp[j]->Draw("same lego 0");
        c6->Update();
      }


      c7->cd();
      gPad->SetTheta(30); // default is 30
      gPad->SetPhi(120); // default is 30
      gPad->Update();
      hInterp[2]->GetXaxis()->SetTitle("m_S (GeV)");
      hInterp[2]->GetYaxis()->SetTitle("m_Zd (GeV)");
      hInterp[2]->GetXaxis()->CenterTitle();
      hInterp[2]->GetYaxis()->CenterTitle();
      hInterp[2]->GetXaxis()->SetTitleOffset(2.0);
      hInterp[2]->GetYaxis()->SetTitleOffset(2.0);
      hPanel2D[2]->GetXaxis()->SetTitle("m_S (GeV)");
      hPanel2D[2]->GetYaxis()->SetTitle("m_Zd (GeV)");
      hPanel2D[2]->GetXaxis()->CenterTitle();
      hPanel2D[2]->GetYaxis()->CenterTitle();
      hPanel2D[2]->GetXaxis()->SetTitleOffset(2.0);
      hPanel2D[2]->GetYaxis()->SetTitleOffset(2.0);
      hPanel2D[2]->SetMaximum(hMax[2]);
      hPanel2D[2]->SetMinimum(0);
      hPanel2D[2]->SetLineColor(kRed);
      hPanel2D[2]->Draw("lego 0");
      hInterp[2]->Draw("same lego 0");
      c7->Update();

      c8 = new TCanvas("Canv8", "Canv8",60,300,1000, 500);
      c8->cd();
      hInterp[2]->GetXaxis()->SetTitleOffset(1.0);
      hInterp[2]->GetYaxis()->SetTitleOffset(1.0);
      hInterp[2]->Draw("contz");
      c8->Update();


    gPad->Modified();		// make sure
    gPad->Update();		  // hist is drawn
    gSystem->ProcessEvents();


  // write out the output ROOT fle
    f->Write();

/*
    // Make an animated plot
      while (1) {
        for (size_t j = 1; j < 7; j++) {
          c5->cd(j);
          Animate();
        }
        gSystem->ProcessEvents();
        usleep(100);
      }
*/










		// Can use this instead of TheApp->Run();
		   std::cout << "\n Done  ...  Now waiting...";
		   std::cout << "\n ==> Double click mouse button in graphics window to end program.\n\n\n";
		   int n=1;
		   while (n>0) {
		     MyObj = gPad->WaitPrimitive();
		      if (!MyObj) break;
		      printf("Loop i=%d, found objIsA=%s, name=%s\n",n,MyObj->ClassName(),MyObj->GetName());
		   }



delete hPlane[3];
delete hInterp[3];
delete hCheck[3];
delete hGrid;
delete hContPt;
delete dt[3];
delete c;
delete c2[3];
delete c3;
delete MyObj;
}
