/*
 *  Thin Plate Spline demo/example in C++
 *
 *  - a simple TPS editor, using the Boost uBlas library for large
 *    matrix operations and OpenGL + GLUT for 2D function visualization
 *    (curved plane) and user interface
 *
 *  Copyright (C) 2003,2005 by Jarno Elonen
 *
 *  TPSDemo is Free Software / Open Source with a very permissive
 *  license:
 *
 *  Permission to use, copy, modify, distribute and sell this software
 *  and its documentation for any purpose is hereby granted without fee,
 *  provided that the above copyright notice appear in all copies and
 *  that both that copyright notice and this permission notice appear
 *  in supporting documentation.  The authors make no representations
 *  about the suitability of this software for any purpose.
 *  It is provided "as is" without express or implied warranty.
 *
 *  TODO:
 *    - implement TPS approximation 3 as suggested in paper
 *      Gianluca Donato and Serge Belongie, 2002: "Approximation
 *      Methods for Thin Plate Spline Mappings and Principal Warps"
 */

//#define GL_SILENCE_DEPRECATION  // Kill warnings about OPenGL being deprecated on a Mac
//#include <GL/glut.h>          // in Mac we dont use this
//#include <OpenGL/glut.h>      // We use GLUT here it seems not OpenGL
//#include <GLUT/glut.h>          // Replace with this
#include <boost/numeric/ublas/matrix.hpp>

#include "linalg3d.h"
#include "ludecomposition.h"

#include <vector>
#include <cmath>
#include <cstdio>
#include <cstring>

using namespace boost::numeric::ublas;

// ========= BEGIN INTERESTING STUFF  =========

#define GRID_W 100
#define GRID_H 100
float grid[GRID_W][GRID_H];

std::vector< Vec > control_points;
int selected_cp = -1;


double regularization;  // value read from settings.ini file
double bending_energy = 0.0;
