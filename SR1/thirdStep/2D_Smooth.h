#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <TCanvas.h>
#include <TView.h>
#include <TApplication.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TGraph2D.h>
#include <TGraphErrors.h>
#include <TStyle.h>
#include <TMath.h>
#include <TROOT.h>
#include <TLine.h>
#include <TSystem.h>
#include <TFitResult.h>
#include <TFitter.h>
#include <TSpline.h>
#include <TSpectrum.h>
#include <TMinuit.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TFile.h>

#include <vector>
#include <boost/algorithm/string.hpp>
#include "IniFile.hh"


//using namespace std;

// ROOT Globals
TObject  *MyObj = 0;
TCanvas  *c= NULL;       // create a canvass
TCanvas  *c2[3];       // create multiple canvasses
TCanvas  *c3= NULL;       // create multiple canvasses
TCanvas  *c4= NULL;       // create multiple canvasses
TCanvas  *c5= NULL;       // create multiple canvasses
TCanvas  *c6= NULL;       // create multiple canvasses
TCanvas  *c7= NULL;       // create multiple canvasses
TCanvas  *c8= NULL;       // create multiple canvasses
TTimer *timer = new TTimer( kFALSE, 20);

//void Animate();
Double_t pi;
Float_t theta = 30;
Float_t phi   = 30;


// global variables


// range is m_Zd  15 - 46
// range is m_S   30 - 115

int mS_low = 30;
int mS_high = 130;
int mZd_low = 10;
int mZd_high = 60;



int NBin = 100;
// Arrange bin centres on integers !
float mS_low_h = mS_low - (float)( (mS_high - mS_low) / (float)(NBin) ) /2.0;   // Makes 1.5 to be centre of low bin
float mS_high_h = mS_high - (float)( (mS_high - mS_low) / (float)(NBin) ) /2.0; // Makes 901.5 to be centre of high bin
float mZd_low_h = mZd_low - (float)( (mZd_high - mZd_low) / (float)(NBin) ) /2.0;// Makes 1 to be centre of low bin
float mZd_high_h = mZd_high - (float)( (mZd_high - mZd_low) / (float)(NBin) ) /2.0;// Makes 300 to be centre of high bin

float mS_low_g = -NBin/2.0 - 0.5;   // Makes 1 to be centre of low bin
float mS_high_g = NBin/2.0-1 + 0.5;// Makes 900 to be centre of high bin
float mZd_low_g = -NBin/2.0 - 0.5;
float mZd_high_g = NBin/2.0-1 + 0.5;



TH2D*hPlane[3];
TH2D*hInterp[3];
TH2D*hInterpAdd[3];
TH2D*hCheck[3];
TH1D*hInterp1D[3];
TH2D*hGrid;
TH2D*hContPt;
TGraph2D *dt[3];
TH2D*hPanel2D[3];
int hMax1, hMax2, hMax[3];

struct PeakData
{
		int mS;
		int mZd;
		float mean;
		float erMean;
		float sigma;
		float erSigma;
		float norm;
		float erNorm;
};
std::vector<PeakData> v;



const char *c_name[] =
      {
				"mean",
//				"erMean",
				"sigma",
//				"erSigma",
				"norm"
//				"erNorm"
      };
