using namespace std;

 double myGaus(double *x, double *par)
 { 
   double mean	= par[0]; 
   double sigma	= par[1];
   double N	= par[2];
   double t = (x[0]-mean)/sigma;
   double result = -11111;
 
   
  
       result = exp(-0.5*t*t);
    
   return N*result;
 }
TH1D* hInterpol[35][4];
//TFile* file[35];
const char *i_name[] = {"4e", "2e2m", "4m", "all"};

//TH1D* hInterpol = new TH1D("hInterpol" ,";m_{Zd} GeV;events/[1 GeV]", 300,0,300);

TH1D* hist_avgM_nom[36][3];
TH1D* hist_avgM_nomLOEvt[36][3];
TH1D* hist_avgM_kSyst[36][3];
TH2D *hist[3][3];
// Globals
TObject *MyObj = 0;

TH1D* hist_m4l_nom[36][4];
TFile* file[36];
TFile* fileLOEvt[36];
TFile* file_kSyst[36][2];
//const char *i_name[] = {"4e", "2e2m", "4m", "all"};
const char *i_namePrime[] = {"4e", "2e2mu", "4mu", "all"};
 string line;
//ifstream myfile ("inputsSignalNominal.dat");//for reco
//ifstream myfileKinSyst ("inputsSignalkinSys.dat");//for reco
//ifstream myfileWeightSyst ("inputsSignalWeightSys.dat");//for reco
vector<TString> lineCointainerAvgM;
vector<TString> lineCointainerLumOEvt;
//vector<TString> lineCointainerWeightSys;
vector<TString> lineCointainerAvgMkSyst[36][2];
int NFileToProcess;
TFile* file_saveFitHist;
TObjArray *MyHistArrayGen = new TObjArray(0);
TObjArray *MyHistArrayHistAll = new TObjArray(0);
TFile *file_saveGenHist;
TH1F *sig;
TH1F *sig_up;
TH1F *sig_down;
TDirectory *myDirect;
int channel;
TCanvas *c1 = new TCanvas("c1","c1",700,500);
TPad* thePad = (TPad*)c1->cd();
//const int NumberOfFileToProcess;
int i_Nwsyst = -1;
int i_var = -1;
int i_kSyst;
int k_camp;

double PmS[86];
double PmZd[86];

//double mS[36];
//double mZd[36];
double QCD_scale_up[86];
double QCD_scale_down[86];
double PDF_scale_up[86];
double PDF_scale_down[86];

double interpSyst_scale_up[86];
double interpSyst_scale_down[86];

double QCD_uncert_up[86];
double QCD_uncert_down[86];
double PDF_uncert_up[86];
double PDF_uncert_down[86];



 const char *c_mZdmS[] =
      {
	"mZd15_mH30GeV",
	"mZd15_mH40GeV",
	"mZd20_mH50GeV",
	"mZd25_mH60GeV",
	"mZd30_mH70GeV",
	"mZd35_mH80GeV",
	"mZd40_mH90GeV",
	"mZd45_mH100GeV",
	"mZd18_mH56GeV",
	"mZd22_mH60GeV",
	"mZd16_mH64GeV",
	"mZd29_mH66GeV",
	"mZd32_mH68GeV",
	"mZd20_mH73GeV",
	"mZd34_mH76GeV",
	"mZd27_mH80GeV",
	"mZd36_mH82GeV",
	"mZd19_mH84GeV",
	"mZd33_mH85GeV",
	"mZd24_mH86GeV",
	"mZd37_mH92GeV",
	"mZd28_mH93GeV",
	"mZd17_mH95GeV",
	"mZd44_mH96GeV",
	"mZd42_mH98GeV",
	"mZd23_mH102GeV",
	"mZd48_mH104GeV",
	"mZd35_mH105GeV",
	"mZd38_mH105GeV",
	"mZd46_mH108GeV",
	"mZd26_mH109GeV",
	"mZd30_mH110GeV",
	"mZd50_mH111GeV",
	"mZd52_mH112GeV"
      };
    
const char *c_wSyst[] =
      {
	"Nominal",
	"EL_EFF_ID_TOTAL",
	"EL_EFF_ISO_TOTAL",
	"EL_EFF_RECO_TOTAL",
	"MUON_EFF_ISO_STAT",
	"MUON_EFF_ISO_SYS",
	"MUON_EFF_RECO_STAT",
	"MUON_EFF_RECO_STAT_LOWPT",
	"MUON_EFF_RECO_SYS",
	"MUON_EFF_RECO_SYS_LOWPT",
	"MUON_EFF_TTVA_STAT",
	"MUON_EFF_TTVA_SYS",
	"Pileup_weight"
      };

const char *c_kSyst[] =
      {
	"EG_RESOLUTION_ALL1",
	"EG_SCALE_ALL1",
	"MUONS_ID1",
	"MUONS_MS1",
	"MUONS_SAGITTA_RESBIAS1",
	"MUONS_SAGITTA_RHO1",
	"MUONS_SCALE1"

      };
    
    const char *c_vSyst[] =
      {
	"up",
	"down"
      };

  
  const char *c_mS[] =
    {
      "30",
      "40",
      "50",
      "60",
      "70",
      "80",
      "90",
      "100",
      "56",
      "60",
      "64",
      "66",
      "68",
      "73",
      "76",
      "80",
      "82",
      "84",
      "85",
      "86",
      "92",
      "93",
      "95",
      "96",
      "98",
      "102",
      "104",
      "105",
      "105",
      "108",
      "109",
      "110",
      "111",
      "112"
    };
  
  const char *c_mZd[] =
      {
	"15",
	"15",
	"20",
	"25",
	"30",
	"35",
	"40",
	"45",
	"18",
	"22",
	"16",
	"29",
	"32",
	"20",
	"34",
	"27",
	"36",
	"19",
	"33",
	"24",
	"37",
	"28",
	"17",
	"44",
	"42",
	"23",
	"48",
	"35",
	"38",
	"46",
	"26",
	"30",
	"50",
	"52"
      };

const char *c_mSPrime[] =
    {
      "30",
      "40",
      "50",
      "60",
      "70",
      "80",
      "90",
      "100",
      "56",
      "60",
      "64",
      "66",
      "68",
      "73",
      "76",
      "80",
      "82",
      "84",
      "85",
      "86",
      "92",
      "93",
      "95",
      "96",
      "98",
      "102",
      "104",
      "105",
      "105",
      "108",
      "109",
      "110",
      "111",
      "112"
    };
  
  const char *c_mZdPrime[] =
      {
	"15",
	"15",
	"20",
	"25",
	"30",
	"35",
	"40",
	"45",
	"18",
	"22",
	"16",
	"29",
	"32",
	"20",
	"34",
	"27",
	"36",
	"19",
	"33",
	"24",
	"37",
	"28",
	"17",
	"44",
	"42",
	"23",
	"48",
	"35",
	"38",
	"46",
	"26",
	"30",
	"50",
	"52"
      };

   
const char *c_camp[] =
      {
	"mc16a",
	"mc16d",
	"mc16e"
      };


double mZd[34] =
      {
	15,
	15,
	20,
	25,
	30,
	35,
	40,
	45,
	18,
	22,
	16,
	29,
	32,
	20,
	34,
	27,
	36,
	19,
	33,
	24,
	37,
	28,
	17,
	44,
	42,
	23,
	48,
	35,
	38,
	46,
	26,
	30,
	50,
	52 
      };

 double mS[34] =
      {
	30,
	40,
	50,
	60,
	70,
	80,
	90,
	100,
	56,
	60,
	64,
	66,
	68,
	73,
	76,
	80,
	82,
	84,
	85,
	86,
	92,
	93,
	95,
	96,
	98,
	102,
	104,
	105,
	105,
	108,
	109,
	110,
	111,
	112
      };
