double cross_section(double mS)
{
TString files[]={"./YR_mass_xs.dat"};
//TString files_mS[]={"./mS.dat"};
//TString files_br[]={"./brSZZ.dat"};
  ifstream fin(files[0]);
  TString tmps;
  std::vector<unsigned int> Npoints {70};
  const unsigned int  Npoints_max = 100;

 double mass_mS[Npoints_max];
 double XS[Npoints_max];

 for (Int_t j=0; j<Npoints[0]; j++)
	{
	  
	  fin>>tmps;mass_mS[j]=tmps.IsFloat()?tmps.Atof():0;
	  fin>>tmps; XS[j]=tmps.IsFloat()?tmps.Atof():0;
	}

 //cout << "ms = " << mass_mS[0] << "XS = " << XS[0] << endl;
 TGraph* gr_XS = new TGraph(Npoints[0]);
 gr_XS->SetTitle("");
 gr_XS->GetXaxis()->SetTitle("m_{S} [GeV]");
 gr_XS->GetYaxis()->SetTitle("XS[pb]");


 for ( Int_t j=0; j<Npoints[0]; j++ ) {
      
   gr_XS->SetPoint(j,mass_mS[j],XS[j]);
 }
 gr_XS->Draw("ac");

 return gr_XS->Eval(mS);
  
}
