//#include "declarationFile.h"




void SaveInNomDirect(int j, double mZd, double mS)

{
  
  /*for (int i_gen = 0; i_gen < NFileToProcess; i_gen++)
	{
	  QCD_scale_up[i_gen]   = (1 + QCD_uncert_up[i_gen]*0.01);
	  QCD_scale_down[i_gen] = (1 + QCD_uncert_down[i_gen]*0.01);
	  
	  PDF_scale_up[i_gen]   = (1 + PDF_uncert_up[i_gen]*0.01);
	  PDF_scale_down[i_gen] = (1 + PDF_uncert_down[i_gen]*0.01);*/
  
  interpSyst_scale_up[j]   = (1 + 0.1); //20% => -10% and + 10%
  interpSyst_scale_down[j] = (1 -0.1);
  
  file_saveFitHist =  new TFile(TString::Format("outputFile/BRscaled_gaussiansignal_%schannel_mZd%.0lf_mH%.0lfGeV.root",i_namePrime[channel],PmZd[j],PmS[j]),"recreate");
	  sig = (TH1F*)hInterpol[j][channel]->Clone();
	  sig->SetName("sig");
	  sig_up = (TH1F*)hInterpol[j][channel]->Clone();
	  sig_up->SetName("sig_up");
	  sig_down = (TH1F*)hInterpol[j][channel]->Clone();
	  sig_down->SetName("sig_down");
	  myDirect = file_saveFitHist->mkdir("Nominal");
	  file_saveFitHist->cd("Nominal");
	  sig->Write();
	  myDirect = file_saveFitHist->mkdir("STAT");
	  file_saveFitHist->cd("STAT");
	  sig->Write();
	  myDirect = file_saveFitHist->mkdir("LUMI");
	  file_saveFitHist->cd("LUMI");
	  sig_up->Write();
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("QCDSCALE");
	  file_saveFitHist->cd("QCDSCALE");
	  //sig_up->Scale(QCD_scale_up[i_gen]);
	  sig_up->Write();
	  //sig_down->Scale(QCD_scale_down[i_gen]);
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("PDF");
	  file_saveFitHist->cd("PDF");
	  //sig_up->Scale(PDF_scale_up[i_gen]/QCD_scale_up[i_gen]);
	  sig_up->Write();
	  //sig_down->Scale(PDF_scale_down[i_gen]/QCD_scale_down[i_gen]);
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("interpSyst");
	  file_saveFitHist->cd("interpSyst");
	  sig_up->Scale(interpSyst_scale_up[j]);
	  sig_up->Write();
	  sig_down->Scale(interpSyst_scale_down[j]);
	  sig_down->Write();
	  sig->Reset();
	  sig_up->Reset();
	  sig_down->Reset();
	  file_saveFitHist->Close();
	  
	  //	}
  
 
}

void SaveOtherSystDirect(int j, double mZd, double mS)
{
  //for (int i_gen = 0; i_gen < NFileToProcess; i_gen++)
  //	{
for (i_Nwsyst = 1; i_Nwsyst < 13; i_Nwsyst++)
	    {
	      for (i_var = 0; i_var < 2; i_var++)
		{
		  file_saveFitHist =  new TFile(TString::Format("outputFile/BRscaled_gaussiansignal_%schannel_mZd%.0lf_mH%.0lfGeV.root",i_namePrime[channel],PmZd[j],PmS[j]),"update");
		  if (i_var ==0)  myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_wSyst[i_Nwsyst]));
		  else file_saveFitHist->cd(TString::Format("%s",c_wSyst[i_Nwsyst])); 
		  // for (int i_var = 0; i_var < 2; i_var++)
		  // {
		  sig = (TH1F*)hInterpol[j][channel]->Clone();
		  sig->SetName(TString::Format("sig_%s",c_vSyst[i_var]));
		  file_saveFitHist->cd(TString::Format("%s",c_wSyst[i_Nwsyst]));
		  sig->Write();
		  sig->Reset();
		  // }
		  file_saveFitHist->Close();
	  	}
	    }

}



void SaveKinSystDirect(int j, double mZd, double mS)
{

  //for (int i_gen = 0; i_gen < NFileToProcess; i_gen++)
  // {

  for (int k = 0; k < 7; k++)
  {
    for (i_var = 0; i_var < 2; i_var++)
		{
     file_saveFitHist =  new TFile(TString::Format("outputFile/BRscaled_gaussiansignal_%schannel_mZd%.0lf_mH%.0lfGeV.root",i_namePrime[channel],PmZd[j],PmS[j]),"update");
     if (i_var ==0)  myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_kSyst[k]));
     else file_saveFitHist->cd(TString::Format("%s",c_kSyst[k])); 
     sig = (TH1F*)hInterpol[j][channel]->Clone();
     sig->SetName(TString::Format("sig_%s",c_vSyst[i_var]));
     file_saveFitHist->cd(TString::Format("%s",c_kSyst[k]));
     sig->Write();
     sig->Reset();
     file_saveFitHist->Close();
     }
  }
}
