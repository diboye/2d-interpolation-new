#include <TStyle.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TSystem.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TFitResult.h>
#include <TObject.h>
#include <TTree.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TBox.h>
#include <TSpectrum.h>
#include <TLatex.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string.h>
#include <TGraph2DErrors.h>
#include <cmath>
#include <iomanip>
#include "RooWorkspace.h"
#include <RooRealVar.h>
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooAbsArg.h"
#include "RooMomentMorph.h"
#include "RooFit.h"
#include "TRandom3.h"
#include "TGraphErrors.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TSpline.h"
#include "TPaveStats.h"
#include "TExec.h"
#include "TPaletteAxis.h"
#include "declarationFile.h"
#include "functionForMain.h"
#include "cross_section.h"
#include <TString.h>
using namespace std;



int main(int argc, char *argv[])
  
{
    TCanvas *c1 = new TCanvas("c1","c1",700,500);
    TPad* thePad = (TPad*)c1->cd();
    thePad->SetTicks();
    //.SetTicks();
    
    double NormLOEvt[34];
    double intNorm[34];
    double NormLOEvtError[34];
    double intNormError[34];

    /*std::ofstream outfile;
      outfile.open("EffTimesAcc.txt");
      outfile << "mS" << '\t' << "mZd" << '\t' << "reco EffxAccp"  << '\t' << "erro reco EffxAccp" << '\t' << "interp EffxAccp" <<'\t' << "error interp EffxAccp" << endl;*/
    
    const int Npoints =31;
    /*for (int i = 0; i < Npoints; i++)
      {

	
	    
	    lineCointainerLumOEvt.push_back(TString::Format("../../../Ntuple/signal/combined_output_SR1/signal_reco/signal_reco_Nominal/signal_%s_%s_Nominal_reco_5GeVSliceCutNoXS_output_combined.root",c_mSPrime[i], c_mZdPrime[i]));
	  

	 }
    
    for (int i = 0; i < 3; i++)
      {
	for (int j = 0; j < Npoints; j++)
	  {
	   
	   
	    fileLOEvt[j] = TFile::Open(lineCointainerLumOEvt[j]);
	    hist_avgM_nomLOEvt[j][i]  = (TH1D*)fileLOEvt[j]->Get(TString::Format("hCutflow_%s",i_name[i]));
	    
	  }
      }*/
    TFile *f_4e   = new TFile("FitParameterAvgMll_sr1_4e.root");
    TFile *f_2e2m = new TFile("FitParameterAvgMll_sr1_2e2m.root");
    TFile *f_4m   = new TFile("FitParameterAvgMll_sr1_2e2m.root");
   

    hist[0][0] = (TH2D*)f_4e->Get("Panel_mean");
    hist[1][0] = (TH2D*)f_4e->Get("Panel_sigma");
    hist[2][0] = (TH2D*)f_4e->Get("Panel_norm");


    hist[0][1] = (TH2D*)f_2e2m->Get("Panel_mean");
    hist[1][1] = (TH2D*)f_2e2m->Get("Panel_sigma");
    hist[2][1] = (TH2D*)f_2e2m->Get("Panel_norm");


    hist[0][2] = (TH2D*)f_4m->Get("Panel_mean");
    hist[1][2] = (TH2D*)f_4m->Get("Panel_sigma");
    hist[2][2] = (TH2D*)f_4m->Get("Panel_norm");
    
    
    for (channel = 0; channel < 3; channel++)
      {
	TString file_input[] = {"./input_mS_mZd.dat"};
	TString tmps;
	ifstream fin_intp(file_input[0]);
	//double Param_intp[6][Npoints];

	double XS[66];
	fin_intp >> tmps;
	fin_intp >> tmps; // read first line.
	
	for (Int_t i = 0; i < 66; i++) {
	  
	  fin_intp >> tmps;
	  PmS[i] = tmps.IsFloat() ? tmps.Atof() : 0;
	  fin_intp >> tmps;
	  PmZd[i] = tmps.IsFloat() ? tmps.Atof() : 0;
	  
	  
	}
	for (int i=0; i< 66; i++)
	  {
	    XS[i] = cross_section(PmS[i]);
	    //cout << PmS[i] << " " << PmZd[i] << " " << XS[i] << endl;
	    //cout << cross_section(mS[20]) << endl;
	  }
	//double XS[24];



	double Mean_binX[66];
	double Mean_binY[66];

	double Sigma_binX[66];
	double Sigma_binY[66];

	double Norm_binX[66];
	double Norm_binY[66];

	

	double Mean[66];
	double Sigma[66];
	double Norm[66];

	
	//TF1 *rand_gauss_mZd = new TF1("rand_gauss_mZd",myGaus, 0, 60, 3);
  
    for (int f =0; f<66; f++)
      {
	Mean_binX[f]     =  hist[0][channel]->GetXaxis()->FindBin(PmS[f]);
	Mean_binY[f]     =  hist[0][channel]->GetYaxis()->FindBin(PmZd[f]);

	Sigma_binX[f]	 =  hist[1][channel]->GetXaxis()->FindBin(PmS[f]);
	Sigma_binY[f]	 =  hist[1][channel]->GetYaxis()->FindBin(PmZd[f]);
			  
	Norm_binX[f]	 =  hist[2][channel]->GetXaxis()->FindBin(PmS[f]);
	Norm_binY[f]	 =  hist[2][channel]->GetYaxis()->FindBin(PmZd[f]);

	Mean[f]	  = hist[0][channel]->GetBinContent(hist[0][channel]->GetBin(Mean_binX[f],Mean_binY[f]));
	Sigma[f]  = hist[1][channel]->GetBinContent(hist[1][channel]->GetBin(Sigma_binX[f],Sigma_binY[f]));
	Norm[f]	  = hist[2][channel]->GetBinContent(hist[2][channel]->GetBin(Norm_binX[f],Norm_binY[f]));
	
	hInterpol[f][channel] = new TH1D(TString::Format("hInterpol_%.0lf_%.0lf_%s", PmS[f], PmZd[f], i_name[channel]), TString::Format("hist_%.0lf_%.0lf;m_{Zd} GeV;events/[1.0 GeV]",PmS[f],PmZd[f]), 60, 0, 60);

	 //intNorm[f] = XS[f]*(gr_integral_reco_mZd_mS->Interpolate(PmZd[f],PmS[f]));
	double mZd_rd;
	TRandom3 *rand_gauss = new TRandom3();
	cout << "mean " << Mean[f] << " sigma " << Sigma[f] << " norm  " << Norm[f] << endl;
	 for (int j = 0; j < 50000; j++)
	  
	  {
	    //rand_gauss_mZd->SetParameters(Mean[f], Sigma[f], Norm[f]);
	    //hInterpol[f][channel]->SetBinContent(j, rand_gauss_mZd->Eval(j));
	    //cout << "mean " << rand_gauss_mZd->Eval(j) << endl;
	    mZd_rd = rand_gauss->Gaus(Mean[f], Sigma[f]);
	    hInterpol[f][channel]->Fill(mZd_rd);
	  }
	 double integral_interp =  hInterpol[f][channel]->Integral();
	hInterpol[f][channel]->Scale(Norm[f]/integral_interp);
	hInterpol[f][channel]->Scale(XS[f]);
	
	hInterpol[f][channel]->Draw("hist");
	thePad->Print(TString::Format("hist_reco_interpol_%.0lf_%.0lf_%s.pdf",PmS[f],PmZd[f],i_name[channel]));
	SaveInNomDirect(f, PmS[f], PmZd[f]);
	SaveOtherSystDirect(f, PmS[f], PmZd[f]);
	SaveKinSystDirect(f, PmS[f], PmZd[f]);
	
      }
    //outfile.close();
    
    
    
      }
    
    return 0;
}
